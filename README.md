# MS3Align #

Welcome to Source code repo of MS3Align 


## Introduction 
This is essentially a command line tool to generate alignments between 2 or more molecular surfaces.

See [the project website ](http://vgl.serc.iisc.ernet.in/ms3align/) for more details