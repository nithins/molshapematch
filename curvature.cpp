#include <string>
#include <iostream>
#include <stack>
#include <set>
#include <stdexcept>

#include "utl.h"
#include "curvature.hpp"

#define HALF_PI 1.57079632679489661;

using namespace std;

using namespace std;

namespace eigen {

void semi_definite_symmetric(const double *mat,
                             int n,
                             double *eigen_vec,
                             double *eigen_val)
{
  static const double EPS = 0.00001;
  static int MAX_ITER = 100;
  //static const double M_PI = 3.14159265358979323846;
  double *a,*v;
  double a_norm,a_normEPS,thr,thr_nn;
  int nb_iter = 0;
  int jj;
  int i,j,k,ij,ik,l,m,lm,mq,lq,ll,mm,imv,im,iq,ilv,il,nn;
  int *index;
  double a_ij,a_lm,a_ll,a_mm,a_im,a_il;
  double a_lm_2;
  double v_ilv,v_imv;
  double x;
  double sinx,sinx_2,cosx,cosx_2,sincos;
  double delta;

  // Number of entries in mat

  nn = (n*(n+1))/2;

  // Step 1: Copy mat to a

  a = new double[nn];

  for( ij=0; ij<nn; ij++ ) {
    a[ij] = mat[ij];
  }

  // Ugly Fortran-porting trick: indices for a are between 1 and n
  a--;

  // Step 2 : Init diagonalization matrix as the unit matrix
  v = new double[n*n];

  ij = 0;
  for( i=0; i<n; i++ ) {
    for( j=0; j<n; j++ ) {
      if( i==j ) {
        v[ij++] = 1.0;
      } else {
        v[ij++] = 0.0;
      }
    }
  }

  // Ugly Fortran-porting trick: indices for v are between 1 and n
  v--;

  // Step 3 : compute the weight of the non diagonal terms
  ij     = 1  ;
  a_norm = 0.0;
  for( i=1; i<=n; i++ ) {
    for( j=1; j<=i; j++ ) {
      if( i!=j ) {
        a_ij    = a[ij];
        a_norm += a_ij*a_ij;
      }
      ij++;
    }
  }

  if( a_norm != 0.0 ) {

    a_normEPS = a_norm*EPS;
    thr       = a_norm    ;

    // Step 4 : rotations
    while( thr > a_normEPS   &&   nb_iter < MAX_ITER ) {

      nb_iter++;
      thr_nn = thr / nn;

      for( l=1  ; l< n; l++ ) {
        for( m=l+1; m<=n; m++ ) {

          // compute sinx and cosx

          lq   = (l*l-l)/2;
          mq   = (m*m-m)/2;

          lm     = l+mq;
          a_lm   = a[lm];
          a_lm_2 = a_lm*a_lm;

          if( a_lm_2 < thr_nn ) {
            continue ;
          }

          ll   = l+lq;
          mm   = m+mq;
          a_ll = a[ll];
          a_mm = a[mm];

          delta = a_ll - a_mm;

          if( delta == 0.0 ) {
            //x = - M_PI/4 ;
            x = -3.14159265358979323846/4;
          } else {
            x = - atan( (a_lm+a_lm) / delta ) / 2.0 ;
          }

          sinx    = sin(x)   ;
          cosx    = cos(x)   ;
          sinx_2  = sinx*sinx;
          cosx_2  = cosx*cosx;
          sincos  = sinx*cosx;

          // rotate L and M columns

          ilv = n*(l-1);
          imv = n*(m-1);

          for( i=1; i<=n;i++ ) {
            if( (i!=l) && (i!=m) ) {
              iq = (i*i-i)/2;

              if( i<m )  {
                im = i + mq;
              } else {
                im = m + iq;
              }
              a_im = a[im];

              if( i<l ) {
                il = i + lq;
              } else {
                il = l + iq;
              }
              a_il = a[il];

              a[il] =  a_il*cosx - a_im*sinx;
              a[im] =  a_il*sinx + a_im*cosx;
            }

            ilv++;
            imv++;

            v_ilv = v[ilv];
            v_imv = v[imv];

            v[ilv] = cosx*v_ilv - sinx*v_imv;
            v[imv] = sinx*v_ilv + cosx*v_imv;
          }

          x = a_lm*sincos; x+=x;

          a[ll] =  a_ll*cosx_2 + a_mm*sinx_2 - x;
          a[mm] =  a_ll*sinx_2 + a_mm*cosx_2 + x;
          a[lm] =  0.0;

          thr = fabs( thr - a_lm_2 );
        }
      }
    }
  }

  // Step 5: index conversion and copy eigen values

  // back from Fortran to C++
  a++;

  for( i=0; i<n; i++ ) {
    k = i + (i*(i+1))/2;
    eigen_val[i] = a[k];
  }

  delete[] a;

  // Step 6: sort the eigen values and eigen vectors

  index = new int[n];
  for( i=0; i<n; i++ ) {
    index[i] = i;
  }

  for( i=0; i<(n-1); i++ ) {
    x = eigen_val[i];
    k = i;

    for( j=i+1; j<n; j++ ) {
      if( x < eigen_val[j] ) {
        k = j;
        x = eigen_val[j];
      }
    }

    eigen_val[k] = eigen_val[i];
    eigen_val[i] = x;

    jj       = index[k];
    index[k] = index[i];
    index[i] = jj;
  }


  // Step 7: save the eigen vectors

  v++; // back from Fortran to to C++

  ij = 0;
  for( k=0; k<n; k++ ) {
    ik = index[k]*n;
    for( i=0; i<n; i++ ) {
      eigen_vec[ij++] = v[ik++];
    }
  }

  delete[] v    ;
  delete[] index;
  return;
}

}

/*****************************************************************************/
/* Cut the line from p1 to p2 by sphere centered at sc with radius r         */
/* This function assumes p1 is in the sphere (sc,r)                          */
/* and p2 is outside and you are interested in the intersection point        */

typedef tri_cc_geom_t::vertex_t vertex_t;
typedef tri_cc_geom_t::normal_t normal_t;
typedef tri_cc_geom_t::cellid_t cellid_t;



vertex_t RaySphere(const vertex_t& p1,const vertex_t& p2,const vertex_t& sc,const double& r)
{


  double d1 = (sc-p1).squaredNorm();
  double d2 = (sc-p2).squaredNorm();

  double r2 = r*r;

  assert ( d1 < r2 || d2 < r2);

  if(d1 == r2)
    return p1;
  else if(d2 == r2)
    return p2;

  normal_t dp = p2 - p1;

  double a = dp.dot(dp);
  double b = 2*dp.dot(p1-sc);
  double c = sc.dot(sc) + p1.dot(p1) - 2*sc.dot(p1) - r*r;
  double d = b * b - 4 * a * c;

  // avoid floating point errors
  if(d < 0 &&  d > -10e-12)
    d = 0;

  assert(d >= 0);

  d = sqrt(d);

  double u = (-b + d) / (2 * a);

  return  (p1 + u*dp);
}

/*****************************************************************************/
/* Cut a triangle to a polygon with the sphere (sc,r)                        */
int TriSphere(tri_cc_geom_ptr_t tcc, cellid_t c,vertex_t sc,double r,vertex_t * polygon)
{
  cellid_t pt_cells[3];


  int np = tcc->get_cell_points(c,pt_cells);
  ensure(np == 3,"incorrect cellid");
  vertex_t pts[3];

  pts[0] = tcc->get_cell_position(pt_cells[0]);
  pts[1] = tcc->get_cell_position(pt_cells[1]);
  pts[2] = tcc->get_cell_position(pt_cells[2]);

  enum {TRISPHIXN_IN_IN_IN = 0,
        TRISPHIXN_IN_IN_OUT,
        TRISPHIXN_IN_OUT_OUT,
        TRISPHIXN_OUT_OUT_OUT};

  int trisphixn = TRISPHIXN_IN_IN_IN;

  bool isin[3] = {true,true,true};

  if((pts[0] - sc).squaredNorm() >= r*r )
    ++trisphixn,isin[0] = false;

  if((pts[1] - sc).squaredNorm() >= r*r )
    ++trisphixn,isin[1] = false;

  if((pts[2] - sc).squaredNorm() >= r*r )
    ++trisphixn,isin[2] = false;

  if(!isin[0])
  {
    if(isin[2])
      swap(pts[2],pts[0]);
    else
      swap(pts[1],pts[0]);
  }
  else if(!isin[1])
    swap(pts[1],pts[2]);

  switch(trisphixn)
  {
  case TRISPHIXN_IN_IN_IN:
    polygon[0] = (pts[0]);
    polygon[1] = (pts[1]);
    polygon[2] = (pts[2]);
    return 3;
  case TRISPHIXN_IN_IN_OUT:
    polygon[0] = (pts[0]);
    polygon[1] = (RaySphere(pts[0],pts[2],sc,r));
    polygon[2] = (RaySphere(pts[1],pts[2],sc,r));
    polygon[3] = (pts[1]);
    return 4;
  case TRISPHIXN_IN_OUT_OUT:
    polygon[0] = (pts[0]);
    polygon[1] = (RaySphere(pts[0],pts[1],sc,r));
    polygon[2] = (RaySphere(pts[0],pts[2],sc,r));
    return 3;
  default:
    assert(false && "This should never happen");
    return -1;
  }
}

/*****************************************************************************/
/* Area of a 3-gon and 4-gon                                                 */
double area_Polygon( vertex_t * V, int n )
{
  double area = 0;

  switch (n)
  {
  case 4:
    area +=std::sqrt((V[3]-V[0]).cross(V[2]-V[0]).squaredNorm());
  case 3:
    area +=std::sqrt((V[2]-V[0]).cross(V[1]-V[0]).squaredNorm());
  }

  return area/2;
}

#include <map>

/*****************************************************************************/
/* Subroutine to get edges within an r neighborhood of a vertex              */

void get_rrad_edges(tri_cc_geom_ptr_t tcc, cellid_t  start,double radius,
                       std::vector<cellid_t> &hedges)
{
  typedef std::map<std::pair<int,int>,cellid_t> seen_edges_t;

  std::stack<cellid_t>          front_verts;
  std::set<int>                 seen_verts;
  seen_edges_t                  seen_edges;

  vertex_t st_pt = tcc->get_cell_position(start);

  front_verts.push(start);

  double r2 = radius*radius;

  while(front_verts.size() != 0)
  {
    cellid_t hVertex = front_verts.top();
    front_verts.pop();

    if(seen_verts.count(hVertex) != 0)
      continue;

    seen_verts.insert(hVertex);

    vertex_t pt = tcc->get_cell_position(hVertex);

    if((st_pt - pt).squaredNorm() >= r2)
      continue;

    cellid_t hVertex_edges[80];

    int ct = tcc->get_cell_co_facets(hVertex,hVertex_edges);

    for(int i = 0 ; i < ct; ++ i)
    {
      cellid_t epts[2];

      tcc->get_cell_points(hVertex_edges[i],epts);

      std::pair<int,int> edge(epts[0],epts[1]);

      if(edge.first < edge.second)
        std::swap(edge.first,edge.second);

      if(seen_edges.count(edge) != 0)
        continue;

      seen_edges[edge] = hVertex_edges[i];

      cellid_t opp_pt = (epts[0] == hVertex)?(epts[1]):(epts[0]);

      front_verts.push(opp_pt);
    }
  }

  for(typename seen_edges_t::iterator it = seen_edges.begin();
      it != seen_edges.end(); ++it)
    hedges.push_back(it->second);
}

/*****************************************************************************/
/* Structure to represent the curvature tensor                               */

struct Curvature
{
  typedef double FT;

  // compact storage for 3x3 symmetric tensor
  FT m_pTensor3[6];
  normal_t m_direction_kmin;
  normal_t m_direction_kmax;
  normal_t m_normal; // should be kmin x kmax but is more traditionally

  FT m_kmin;
  FT m_kmax;
  FT m_scale;
  FT m_mean;

  /*-------------------------------------------------------------------------*/
  /*tensor = 0                                                               */
  void init()
  {
    m_pTensor3[0] = m_pTensor3[1] =
    m_pTensor3[2] = m_pTensor3[3] =
    m_pTensor3[4] = m_pTensor3[5] = (double)0.0;

    m_normal = normal_t(0,0,0);
  }

  /*-------------------------------------------------------------------------*/
  /* tensor += beta * len(edge) * edge * edge_transpose                      */
  void add(const FT& coeff, const normal_t& e)
  {
      m_pTensor3[0] += coeff * e.x()*e.x();
      m_pTensor3[1] += coeff * e.y()*e.x();
      m_pTensor3[2] += coeff * e.y()*e.y();
      m_pTensor3[3] += coeff * e.z()*e.x();
      m_pTensor3[4] += coeff * e.z()*e.y();
      m_pTensor3[5] += coeff * e.z()*e.z();
  }

  /*-------------------------------------------------------------------------*/
  /* tensor *= coeff                                                         */
  void multiply (const FT& coeff)
  {
      m_pTensor3[0] *= coeff ;
      m_pTensor3[1] *= coeff ;
      m_pTensor3[2] *= coeff ;
      m_pTensor3[3] *= coeff ;
      m_pTensor3[4] *= coeff ;
      m_pTensor3[5] *= coeff ;
  }

  /*-------------------------------------------------------------------------*/
  void diagonalize()
  {
    // extract eigenvalues and eigenvectors
    double eigen_values[3];  // will be given in decreasing order
    double eigen_vectors[9]; // the same
    eigen::semi_definite_symmetric((double *)m_pTensor3,3,
                                   eigen_vectors,eigen_values);

    // reorder them
    int indices[3] = {0,1,2};
    reorder(eigen_values,indices);

    // set member data
    m_kmin = (FT)eigen_values[indices[2]];

    m_kmax = (FT)eigen_values[indices[1]];

    m_direction_kmin = normal_t((FT)eigen_vectors[3*indices[2]],
                              (FT)eigen_vectors[3*indices[2]+1],
                              (FT)eigen_vectors[3*indices[2]+2]);
    m_direction_kmax = normal_t((FT)eigen_vectors[3*indices[1]],
                              (FT)eigen_vectors[3*indices[1]+1],
                              (FT)eigen_vectors[3*indices[1]+2]);

    m_mean = (m_kmin+m_kmax)/2.0;
  }

  /*-------------------------------------------------------------------------*/
  /* reorder s.t 0-> min abs eigen val 1-> min eigen 2-> max eigen           */
  static void reorder(double eigen_values[3],int indx[3])
  {
    double abs_eigen[3];
    abs_eigen[0] = std::fabs(eigen_values[0]); indx[0] = 0;
    abs_eigen[1] = std::fabs(eigen_values[1]); indx[1] = 1;
    abs_eigen[2] = std::fabs(eigen_values[2]); indx[2] = 2;

    if(abs_eigen[indx[1]]   <abs_eigen[indx[0]])    std::swap(indx[1],indx[0]);
    if(abs_eigen[indx[2]]   <abs_eigen[indx[0]])    std::swap(indx[2],indx[0]);
    if(eigen_values[indx[2]]<eigen_values[indx[1]]) std::swap(indx[2],indx[1]);
  }
};

/*****************************************************************************/
/* arc sinus                                                                 */
double arc_sinus(double sinus)
{
    if(sinus >= 1.0)
        return HALF_PI;
    if(sinus <= -1.0)
        return -HALF_PI;
    return std::asin(sinus);
}

/*****************************************************************************/
/* Compute average curvature over radius sized neighborhood                  */

Curvature compute_curvature(tri_cc_geom_ptr_t tcc, cellid_t hVertex, double radius)
{
  Curvature curvature;
  curvature.init();

  std::vector<cellid_t> hedges;

  get_rrad_edges(tcc,hVertex,radius,hedges);

  std::set<cellid_t> facets;

  vertex_t hVertex_pt = tcc->get_cell_position(hVertex);

  double r2 = radius*radius;

  for(int i = 0 ; i < hedges.size(); ++i)
  {
    cellid_t hHalfedge = hedges[i];

    if(tcc->is_cell_boundry(hHalfedge))
      continue;

    cellid_t pts[2];

    tcc->get_cell_points(hHalfedge,pts);

    vertex_t p1 = tcc->get_cell_position(pts[0]);
    vertex_t p2 = tcc->get_cell_position(pts[1]);

    double d1 = (p1 - hVertex_pt).squaredNorm();
    double d2 = (p2 - hVertex_pt).squaredNorm();

    if ( d1 > r2)
      p1 = RaySphere(p2,p1,hVertex_pt,radius);

    if ( d2 > r2)
      p2 = RaySphere(p1,p2,hVertex_pt,radius);

    if( p1 == p2)
      continue;

    double length = (p2-p1).norm();

    vertex_t edge = (p2-p1)/length;

    cellid_t tris[2];

    int ct = tcc->get_cell_co_facets(hHalfedge,tris);
    ensure(ct == 2,"incorrect no of tris on edge");

    normal_t n1 = tcc->get_cell_normal(tris[0]);
    normal_t n2 = tcc->get_cell_normal(tris[1]);

    double sinus = n1.cross(n2).dot(edge);
    double beta = arc_sinus((double)sinus);

    curvature.add(beta*length,edge);

    facets.insert(tris[0]);
    facets.insert(tris[1]);
  }

  double region_area = 0;

  for(typename std::set<cellid_t>::iterator it = facets.begin();
      it != facets.end(); ++it)
  {

    cellid_t fct = *it;

    vertex_t polygon[4];

    int n = TriSphere(tcc,fct,hVertex_pt,radius,polygon);

    double area = area_Polygon(polygon,n);

    region_area += area;

    curvature.m_normal = curvature.m_normal+tcc->get_cell_normal(fct)*area;
  }

  curvature.m_normal.normalize();

  ensure(region_area !=0 , "zero region area !!!");

  curvature.multiply(1.0f/region_area);
  curvature.diagonalize();

  ensure(!std::isnan(curvature.m_mean),"NaN curvature computed");

  return curvature;
}

/*---------------------------------------------------------------------------*/
void compute_mean_curvature
(tri_cc_geom_ptr_t tcc,
 double nbd_size, std::vector<double> &curv)
{
  curv.resize(tcc->get_num_cells_dim(0));

  #pragma omp parallel for
  for(cellid_t i = 0 ; i < tcc->get_num_cells_dim(0); ++i)
  {
    try {curv[i] = (compute_curvature(tcc,i,nbd_size).m_mean);}
    catch(std::exception e)
    {cerr << "Error:: VertexIdx=" << i <<endl;throw;}
  }
}

/*---------------------------------------------------------------------------*/

void compute_multi_scale_mean_curvature
(tri_cc_geom_ptr_t tcc,
 int i,
 double b,
 double e,
 int n,
 double * curv,
 double * normal)
{
  std::vector<Curvature> curvMats(n);

  for(int j = 0 ; j < n; ++ j)
  {
    double scale = b+(e-b)*double(j)/(n-1);

    curvMats[j] = compute_curvature(tcc,i,scale);
    curv[j] = curvMats[j].m_mean;
  }

  for(int k = 0 ; k < 3; ++k)
    normal[k] = curvMats[0].m_normal[k];
}


/*****************************************************************************/
