from paraview.simple import *
import json

##-----------------------------------------------------------------------------
## Set These Values To read the Experiment Meta Data and the surface to render

jsonExpMetaDataFile = "output.json"
surfaceNumber       = 0

##-----------------------------------------------------------------------------
## This is a modulo repeating Color Pallete for the surfaces. Adjust as needed.

colorSet = [
  [1,0.8,0.8],#0 skin
  [0.5,1,0.5],  #1 gren
  [0,1,1],    #2 cyan
  [0.6,0.6,1], #3 
  [1,0,0.5],  #4 red
  [0.5,1,0],  #5 green 
  [0,0.5,1],  #6 blue
  [0.5,0,1],  #7 
  [1,0.8,0.8],  #8 
  [0,1,0.5],  #9
  [0,1,1],    #10
  [0.6,0.6,1],    #11
  ]

##-----------------------------------------------------------------------------
## Lets Render the Scene now

jmd  = json.load(open(jsonExpMetaDataFile))

GetRenderView().Background = [1.0, 1.0, 1.0]

sa = XMLPolyDataReader(FileName=jmd["Surfaces"][surfaceNumber]["mscVtkFile"])

sa = GenerateSurfaceNormals()
GetDisplayProperties(sa).DiffuseColor = colorSet[surfaceNumber%len(colorSet)]
GetDisplayProperties(sa).Opacity     = 1.0

thresh = Threshold(sa)        
thresh.Scalars = ['POINTS', 'CriticalPointTag']
thresh.ThresholdRange = [1.0, 2.0]
thresh.UpdatePipeline()

glph = Glyph( GlyphType="Sphere")
glph.GlyphType.Radius = 1.0
drep = Show(glph)

drep.ColorArrayName = ('POINT_DATA', '')
drep.DiffuseColor =  [1.0, 0.3333333333333333, 0.0]

rep = GetDisplayProperties(sa)

    
#lut =  GetLookupTableForArray( "meanCurvature", 1, 
                                #RGBPoints=rcurv_luts[params.Rcurv], 
                                #VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', 
                                #ScalarRangeInitialized=1.0, AllowDuplicateScalars=1 )
#olut = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0] )
rep.Opacity = 1.0
rep.ColorArrayName = ('POINT_DATA', 'meanCurvature')
#rep.LookupTable = lut
#lut.ScalarOpacityFunction = olut


##sbar_rep = CreateScalarBar( Title='meanCurvature', LabelFontSize=12, Enabled=1, TitleFontSize=12 )
##GetRenderView().Representations.append(sbar_rep)
##sbar_rep.LookupTable = lut

Show(sa)    
sa.UpdatePipeline()
glph.UpdatePipeline()


#Render()


#WriteImage(opImgA if (iSo == 0 ) else opImgB)



#Hide(surfObj)
#Hide(glph)
Render()
    

    
    
#Render()    



