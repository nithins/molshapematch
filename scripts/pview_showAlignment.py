from paraview.simple import *
from os.path import isfile as isfile
import json

##-----------------------------------------------------------------------------
## Set These Values To read the Experiment Meta Data to render the alignment 
## surfaceNumberA has to be lesser than surfaceNumberB
## 

jsonExpMetaDataFile = "output.json"
surfaceNumberA      = 0
surfaceNumberB      = 1
transformationNo    = 0

##-----------------------------------------------------------------------------
## This is a modulo repeating Color Pallete for the surfaces. Adjust as needed.

colorSet = [
  [1,0.8,0.8],#0 skin
  [0.5,1,0.5],  #1 gren
  [0,1,1],    #2 cyan
  [0.6,0.6,1], #3 
  [1,0,0.5],  #4 red
  [0.5,1,0],  #5 green 
  [0,0.5,1],  #6 blue
  [0.5,0,1],  #7 
  [1,0.8,0.8],  #8 
  [0,1,0.5],  #9
  [0,1,1],    #10
  [0.6,0.6,1],    #11
  ]


##-----------------------------------------------------------------------------
## Setup logistic variables

jmd    = json.load(open(jsonExpMetaDataFile))
NSurfs = len(jmd["Surfaces"])

assert(0 <= surfaceNumberA and  surfaceNumberA < NSurfs )
assert(0 <= surfaceNumberB and  surfaceNumberB < NSurfs)
assert(surfaceNumberA < surfaceNumberB)

GetRenderView().Background = [1.0, 1.0, 1.0]

def nChoose2(n):
    return n*(n-1)/2

pairNum  = nChoose2(NSurfs) - nChoose2(NSurfs-surfaceNumberA) + surfaceNumberB
pairInfo = jmd["SurfacePairs"][pairNum]

o = pairInfo["transforms"][transformationNo]["orientation"]
p = pairInfo["transforms"][transformationNo]["position"]

o = map(float,o[1:-1].split(","))
p = map(float,p[1:-1].split(","))



##-----------------------------------------------------------------------------
## Create the Vis PipeLine

sa = XMLPolyDataReader(FileName=pairInfo["opSurfAFile"])

sa = GenerateSurfaceNormals()
  
sa = Transform()
sa.Transform.SetPropertyWithName("Rotate",o)
sa.Transform.SetPropertyWithName("Translate",p)  

GetDisplayProperties(sa).DiffuseColor = colorSet[surfaceNumberA%len(colorSet)]
GetDisplayProperties(sa).Opacity      = 0.25
Show(sa)


sb = XMLPolyDataReader(FileName=pairInfo["opSurfBFile"])
sb = GenerateSurfaceNormals()

GetDisplayProperties(sb).DiffuseColor = colorSet[surfaceNumberB%len(colorSet)]
GetDisplayProperties(sb).Opacity      = 0.25
Show(sb)

for iSo,surfObj in enumerate([sa,sb]):
    Threshold3 = Threshold(surfObj)
    Threshold3.Scalars = ['POINTS', 'RegionTag%d'%transformationNo]
    Threshold3.ThresholdRange = [-0.76, float(pairInfo["transforms"][transformationNo]["correspondanceSetSize"])]
    
    drep = Show(Threshold3)    
    
    drep.ColorArrayName = ('POINT_DATA', '')
    drep.DiffuseColor =  colorSet[surfaceNumberA%len(colorSet)] if iSo == 1 else colorSet[surfaceNumberB%len(colorSet)]


    Threshold4 = Threshold()        
    Threshold4.Scalars = ['POINTS', 'CriticalPointTag']
    Threshold4.ThresholdRange = [1.0, 2.0]
    
    Glyph2 = Glyph( GlyphType="Sphere")
    Glyph2.GlyphType.Radius = 0.25
    drep = Show(Glyph2)
    
    drep.ColorArrayName = ('POINT_DATA', '')
    drep.DiffuseColor =  [1.0, 0.3333333333333333, 0.0] if iSo == 0 else [ 0.3333333333333333,1.0, 0.0]       


#XMLUnstructuredGridReader(FileName=corrsAB)
#glyph = Glyph()
#glyph.GlyphType = 'Sphere'
#glyph.GlyphType.Radius = 0.2
#gdp = GetDisplayProperties(glyph)
#gdp.LookupTable = MakeBlueToRedLT(1, 4)
#gdp.LookupTable.ColorSpace = 'Diverging'
#gdp.ColorArrayName='SurfaceNum'
#glyph.UpdatePipeline()
#Show(glyph) 

#if isfile(pdbA) and isfile(pdbB) and False:
    #pdba = PDBReader(FileName=pdbA)
    #tfm = Transform(Input=pdba)
    #tfm.Transform.SetPropertyWithName("Rotate",o)
    #tfm.Transform.SetPropertyWithName("Translate",p)  
    #tube = Tube(Input=tfm)
    #tube.Radius = 0.1
    #GetDisplayProperties(tube).ColorArrayName = 'rgb_colors'
    #GetDisplayProperties(tube).MapScalars     = False
    #tube.UpdatePipeline()
    #Show(tube)

    #pdbb = PDBReader(FileName=pdbB)
    #tube = Tube(Input=pdbb)
    #tube.Radius = 0.1
    #GetDisplayProperties(tube).ColorArrayName = 'rgb_colors'
    #GetDisplayProperties(tube).MapScalars     = False
    #tube.UpdatePipeline()
    #Show(tube)
    
ResetCamera()


Render()
