#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <cmath>
#include <set>
#include <list>
#include <iomanip>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/numeric.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>


#include <vtkPolyData.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLPolyDataWriter.h>

#include "trimesh_mscomplex.h"
#include "trimesh_dataset.h"

#include "curvature.hpp"
#include "fpalign.h"
#include "utl.h"

#if MS3ALIGN_MEMORY_LIMIT > 0
#include <sys/resource.h>
#endif


using namespace std;
using namespace trimesh;

namespace bpo   = boost::program_options ;
namespace bfs   = boost::filesystem ;
namespace br    = boost::range;
namespace badpt = boost::adaptors;
namespace bpt   = boost::property_tree;



/*****************************************************************************/

/** \brief Read from file in .off format to vtkPolyData **/
inline vtkSmartPointer<vtkPolyData> read_off_to_vtk_pd(std::string fname)
{
  using namespace utl;

  vtkSmartPointer<vtkPolyData>  pd   = vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer<vtkPoints>    pts  = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> plys = vtkSmartPointer<vtkCellArray>::New();

  file_line_iterator lgen(fname.c_str()),lend;

  ENSUREV(*lgen++ == "OFF","Doesnt appear to be an OFF file",fname);

  int i = 0, j = 0 , nv = 0 , nt = 0 , np = 0;

  from_string(*lgen++,nv,nt);

  pts->SetNumberOfPoints(nv);

  double pt[3];
  vtkIdType tri[3];

  for(i = 0 ; i < nv && lgen != lend ; ++i)
  {
    from_string(*lgen++,pt[0],pt[1],pt[2]);
    pts->SetPoint(i,pt);
  }

  ENSUREV(i == nv,"Vertices ran out prematurely",fname);

  for(j = 0 ; j < nt && lgen != lend ; ++j)
  {
    from_string(*lgen++,np,tri[0],tri[1],tri[2]);
    plys->InsertNextCell(3,tri);
    ENSUREV(np == 3,"can read triangles only",np);
  }

  ENSUREV(j == nt,"Tris ran out prematurely",fname);

  pd->SetPoints(pts);
  pd->SetPolys(plys);

  return pd;
}

/*---------------------------------------------------------------------------*/

/** \brief Write to file in .off format from vtkPolyData **/

inline void write_off_from_vtk_pd(std::string fn,
                                  vtkSmartPointer<vtkPolyData> pd)
{
  using namespace std;

  vector<double> verts(pd->GetPoints()->GetNumberOfPoints()*3);
  vector<int>    tris(pd->GetPolys()->GetNumberOfCells()*3);

  for(int i = 0 ; i < pd->GetPoints()->GetNumberOfPoints(); ++i)
    pd->GetPoints()->GetPoint(i,&verts[i*3]);

  vtkIdType * tri;
  vtkIdType tri_n;
  int       tri_i = 0;

  pd->GetPolys()->InitTraversal();

  while(pd->GetPolys()->GetNextCell(tri_n,tri))
  {
    tris[tri_i++] = tri[0];
    tris[tri_i++] = tri[1];
    tris[tri_i++] = tri[2];
  }

  vtkDoubleArray * da = dynamic_cast<vtkDoubleArray*>
      (pd->GetPointData()->GetArray(0));

  if(da)
  {
    std::vector<double> da_v(pd->GetNumberOfPoints());

    for(int i = 0 ; i < pd->GetNumberOfPoints(); ++i)
      da_v[i] = da->GetValue(i);

    utl::write_off(fn,verts,tris,da_v);
  }
  else
    utl::write_off(fn,verts,tris);
}

/*---------------------------------------------------------------------------*/

typedef correspondence_builder_t::lm_t         lm_t;
typedef correspondence_builder_t::corrs_t      corrs_t;
typedef correspondence_builder_t::corrs_set_t  corrs_set_t;
typedef correspondence_builder_t::corrs_list_t corrs_list_t;

/** \brief Sort by distance                                                 **/
void sort_corrs_by_dist
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list)
{
  std::vector<size_t> idxs;

  utl::argsort(tfm_dists.begin(),tfm_dists.end(),idxs);
  utl::rearrange(corrs_list.begin(),corrs_list.end(),idxs);
  utl::rearrange(tfm_dists.begin(),tfm_dists.end(),idxs);
  utl::rearrange(transforms.begin(),transforms.end(),idxs);
}


/*---------------------------------------------------------------------------*/

/** \brief Sort corrs by area fraction                                      **/
void sort_corrs_by_area_fraction
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 const std::vector<lm_t> & lmsA,
 const std::vector<lm_t> & lmsB
 )
{
  std::vector<double> overlap(corrs_list.size());
  for(int i = 0 ; i < corrs_list.size(); ++i)
    overlap[i] = max(get_area_fraction<0>(corrs_list[i],lmsA),
                     get_area_fraction<1>(corrs_list[i],lmsB));

  std::vector<size_t> idxs;
  utl::argsort(overlap.begin(),overlap.end(),idxs);
  std::reverse(idxs.begin(),idxs.end());

  utl::rearrange(corrs_list.begin(),corrs_list.end(),idxs);
  utl::rearrange(tfm_dists.begin(),tfm_dists.end(),idxs);
  utl::rearrange(transforms.begin(),transforms.end(),idxs);
}

/*---------------------------------------------------------------------------*/

/** \brief Sort corrs by dist over area fraction                            **/
void sort_corrs_by_dist_over_area_fraction
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 const std::vector<lm_t> & lmsA,
 const std::vector<lm_t> & lmsB
 )
{
  std::vector<double> dist_over_area(corrs_list.size());
  for(int i = 0 ; i < corrs_list.size(); ++i)
    dist_over_area[i] = tfm_dists[i]/( get_area_fraction<0>(corrs_list[i],lmsA),
                     get_area_fraction<1>(corrs_list[i],lmsB));



  std::vector<size_t> idxs;
  utl::argsort(dist_over_area.begin(),dist_over_area.end(),idxs);

  utl::rearrange(corrs_list.begin(),corrs_list.end(),idxs);
  utl::rearrange(tfm_dists.begin(),tfm_dists.end(),idxs);
  utl::rearrange(transforms.begin(),transforms.end(),idxs);
}



/*---------------------------------------------------------------------------*/

/** \brief Filter corrs by inout array                                      **/
void filter_corrs_by_inout_array
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 const std::vector<int> &need_corrs
 )
{
  int ntfms = 0;

  for(int i = 0 ; i < corrs_list.size(); ++i )
  {
    if (need_corrs[i])
    {
      corrs_list[ntfms] = corrs_list[i];
      transforms[ntfms] = transforms[i];
      tfm_dists[ntfms]  = tfm_dists[i];

      ++ntfms;
    }
  }

  corrs_list.resize(ntfms);
  transforms.resize(ntfms);
  tfm_dists.resize(ntfms);
}


/*---------------------------------------------------------------------------*/

/** \brief Retain Unique corrs set                                          **/
void filter_corrs_by_unique
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 int dupn)
{
  std::vector<int> need_corrs(corrs_list.size(),0);

  std::list<corrs_set_t> corrs_llist;

  for(int i = 0 ; i < corrs_list.size(); ++i)
    corrs_llist.push_back
        (corrs_set_t(corrs_list[i].begin(),corrs_list[i].end()));

  int i = 0;

  for(std::list<corrs_set_t>::iterator it = corrs_llist.begin();
      it != corrs_llist.end();)
  {
    bool del_corrs = false;

    for(std::list<corrs_set_t>::iterator jt = corrs_llist.begin();
        it != jt ; ++jt)
    {
      corrs_list_t ixn;

      std::set_intersection(it->begin(),it->end(),
                            jt->begin(),jt->end(),back_inserter(ixn));

      if(ixn.size() >= dupn)
      {
        del_corrs = true;
        break;
      }
    }

    if (del_corrs)
      corrs_llist.erase(it++);
    else
      ++it;

    need_corrs[i++] = !del_corrs;
  }

  filter_corrs_by_inout_array
      (tfm_dists,transforms,corrs_list,need_corrs);

}


/*---------------------------------------------------------------------------*/

/** \brief Filter by average normal angle difference after alignment        **/
void filter_corrs_by_normal_angle
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 const std::vector<lm_t> & lmsA,
 const std::vector<lm_t> & lmsB,
 double  filter_radians)
{
  std::vector<int> need_corrs(corrs_list.size(),0);

  for(int i = 0 ; i < corrs_list.size() ;++ i)
  {
    double sum_ang = 0;

    vtkSmartPointer<vtkTransform> tfm = transforms[i];


    for(int j = 0 ; j < corrs_list[i].size() ;++j)
    {
      int a = corrs_list[i][j].first;
      int b = corrs_list[i][j].second;

      double va[] = {lmsA[a].normal[0],lmsA[a].normal[1],lmsA[a].normal[2],0};
      double vb[] = {lmsB[b].normal[0],lmsB[b].normal[1],lmsB[b].normal[2],0};

      tfm->MultiplyPoint(va,va);

      sum_ang += std::acos(va[0]*vb[0] + va[1]*vb[1] + va[2]*vb[2]);
    }

    need_corrs[i] = ((sum_ang /corrs_list[i].size())< filter_radians);
  }

  filter_corrs_by_inout_array
      (tfm_dists,transforms,corrs_list,need_corrs);
}


/*---------------------------------------------------------------------------*/

/** \brief Filter corrs that are within dist_factor of best distance        **/
void filter_corrs_by_dist_factor
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 double dist_factor
 )
{
  std::vector<int> need_corrs(corrs_list.size(),0);
  double min_dist = *min_element(tfm_dists.begin(),tfm_dists.end());

  for(int i = 0 ; i < tfm_dists.size(); ++i)
    need_corrs[i] = tfm_dists[i] < dist_factor*min_dist;

  filter_corrs_by_inout_array
      (tfm_dists,transforms,corrs_list,need_corrs);
}

/*---------------------------------------------------------------------------*/

/** \brief Filter first N                                                   **/
void filter_corrs_by_firstN
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 int N)
{
  N = min((size_t)N,corrs_list.size());
  corrs_list.resize(N);
  transforms.resize(N);
  tfm_dists.resize(N);
}


/*---------------------------------------------------------------------------*/

/** \brief Filter corrs by distance                                         **/
void filter_corrs_by_distance
(std::vector<double>                         &tfm_dists,
 std::vector<vtkSmartPointer<vtkTransform> > &transforms,
 std::vector<corrs_list_t> &corrs_list,
 double filter_dist_value
 )
{
  std::vector<int> need_corrs(corrs_list.size(),0);

  for(int i = 0 ; i < tfm_dists.size(); ++i)
    need_corrs[i] = tfm_dists[i] < filter_dist_value;

  filter_corrs_by_inout_array
      (tfm_dists,transforms,corrs_list,need_corrs);
}

/*---------------------------------------------------------------------------*/

/** \brief Compute and save the function                                    **/
std::pair<double,double> compute_and_save_mean_curvature
(const std::string &curvBinFile,
 std::vector<double>& mean_curvature,
 tri_cc_geom_ptr_t tcc,
 double Rcurv)
{
  compute_mean_curvature(tcc,Rcurv,mean_curvature);

  double fmin = *br::min_element(mean_curvature);
  double fmax = *br::max_element(mean_curvature);

  std::ofstream fs(curvBinFile.c_str());
  boost::archive::binary_oarchive oa(fs);
  oa << BOOST_SERIALIZATION_NVP(fmin);
  oa << BOOST_SERIALIZATION_NVP(fmax);
  oa << BOOST_SERIALIZATION_NVP(mean_curvature);

  LOG_MACRO(info)
      << "Computed curvature f=" <<curvBinFile << endl
      << SVAR(fmin) << SVAR(fmax) << endl;

  return make_pair(fmin,fmax);
}

/*---------------------------------------------------------------------------*/

/** \brief Read the fmin and fmax                                           **/
std::pair<double,double> read_mean_curvature_min_max
(const std::string &curvBinFile )
{
  double fmin,fmax;

  std::ifstream fs(curvBinFile.c_str());
  boost::archive::binary_iarchive archive(fs);
  archive >> BOOST_SERIALIZATION_NVP(fmin);
  archive >> BOOST_SERIALIZATION_NVP(fmax);
  return make_pair(fmin,fmax);
}

/*---------------------------------------------------------------------------*/

/** \brief read back mean curvature values                                  **/
std::pair<double,double> read_mean_curvature
(std::vector<double> &mean_curvature,
 const std::string &curvBinFile)
{
  double fmin,fmax;

  std::ifstream fs(curvBinFile.c_str());
  boost::archive::binary_iarchive ia(fs);
  ia >> BOOST_SERIALIZATION_NVP(fmin);
  ia >> BOOST_SERIALIZATION_NVP(fmax);
  ia >> BOOST_SERIALIZATION_NVP(mean_curvature);

  return std::make_pair(fmin,fmax);
}

/*---------------------------------------------------------------------------*/

/** \brief read off into tri_cc_geom data structure                         **/
tri_cc_geom_ptr_t read_off(const string & fname)
{
  tri_cc_geom_t::vertex_list_t  verts;
  tri_cc_geom_t::tri_idx_list_t tris;

  utl::file_line_iterator lgen(fname.c_str()),lend;

  ENSUREV(*lgen++ == "OFF","Doesnt appear to be an OFF file",fname);

  int i = 0, j = 0 , nv = 0 , nt = 0 , np = 0;

  utl::from_string(*lgen++,nv,nt);

  verts.resize(nv);
  tris.resize(nt);

  for(i = 0 ; i < nv && lgen != lend ; ++i)
    utl::from_string(*lgen++,verts[i][0],verts[i][1],verts[i][2]);

  ENSUREV(i == nv,"Vertices ran out prematurely",fname);

  for(j = 0 ; j < nt && lgen != lend ; ++j)
  {
    utl::from_string(*lgen++,np,tris[j][0],tris[j][1],tris[j][2]);

    ENSUREV(np == 3,"can read triangles only",np);
  }



  ENSUREV(j == nt,"Tris ran out prematurely",fname);

  tri_cc_geom_ptr_t tcc(new tri_cc_geom_t());

  tcc->init(tris,verts);

  return tcc;
}

/*---------------------------------------------------------------------------*/
/** \brief compute and save mscomplex                                       **/
mscomplex_ptr_t compute_mscomplex
(const std::string &mscFile,
 tri_cc_ptr_t tcc,
 const fn_list_t &fn,
 double Ts)
{
  LOG_MACRO(info) << "Computing MsComplex " << mscFile << endl;

  dataset_ptr_t ds(new dataset_t(fn,tcc));
  mscomplex_ptr_t msc(new mscomplex_t());

  ds->work(msc);
  msc->simplify(Ts);
  msc->collect_mfolds(DES,2,ds);
  msc->save(mscFile);

  return msc;
}

/*---------------------------------------------------------------------------*/

/** \brief compute and save feature points                                  **/
void compute_feature_points
(const std::string &fpFile,
 correspondence_builder_t::lms_t &fps,
 mscomplex_ptr_t msc,
 tri_cc_geom_ptr_t tcc,
 double Ts,
 double MsCurvBeg,
 double MsCurvEnd,
 double MsCurvNum
 )
{
  LOG_MACRO(info) << "Computing Feature Points " << fpFile << endl;

  msc->set_multires_version
      (msc->get_multires_version_for_thresh(Ts));

  BOOST_AUTO(cprng,msc->cp_range()
             |badpt::filtered(bind(&mscomplex_t::is_maxima,msc,_1))
             |badpt::filtered(bind(&mscomplex_t::is_not_paired,msc,_1)));

  int ncps = boost::distance(cprng);

  fps.resize(ncps);

  int i = 0;

  BOOST_FOREACH(int cp, cprng)
  {
    lm_t &lm = fps[i++];

    lm.vertid = msc->vertid(cp);
    lm.index  = msc->index(cp);

    compute_multi_scale_mean_curvature
        (tcc,lm.vertid,MsCurvBeg,MsCurvEnd,15,lm.ms_curv,lm.normal);

    tri_cc_geom_t::vertex_t v = tcc->get_cell_position(lm.vertid);

    lm.pos[0]  = v[0];
    lm.pos[1]  = v[1];
    lm.pos[2]  = v[2];

    std::set<int> vset;

    BOOST_FOREACH(int t, msc->m_des_mfolds[cp])
    {
      int pts[3];
      int ct = tcc->get_cell_points(t,pts);
      ASSERT(ct == 3);
      vset.insert(pts[0]);
      vset.insert(pts[1]);
      vset.insert(pts[2]);
    }

    lm.mfold.resize(vset.size());
    br::copy(vset,lm.mfold.begin());

    lm.area = boost::accumulate
        (msc->m_des_mfolds[cp]|badpt::transformed
         (bind(&tri_cc_geom_t::get_tri_area,tcc,_1)),double(0));
  }

  fps.save(fpFile);
}

/*---------------------------------------------------------------------------*/

void save_feature_point_correspondence_vtk(
    const std::string & fn,
    const std::vector<lm_t> &lmsA,
    const std::vector<lm_t> &lmsB,
    const corrs_list_t & corrs,
    std::vector<corrs_list_t> corrs_sets
    )
{
  vtkSmartPointer<vtkPoints>     nodes  = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray>   arcs  = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkIntArray> nodeSurf = vtkSmartPointer<vtkIntArray>::New();

  vtkSmartPointer<vtkIntArray>   arc_nA = vtkSmartPointer<vtkIntArray>::New();
  vtkSmartPointer<vtkIntArray>   arc_nB = vtkSmartPointer<vtkIntArray>::New();


  BOOST_FOREACH(const lm_t & lm, lmsA)
  {
    nodes->InsertNextPoint(lm.pos);
    nodeSurf->InsertNextValue(0);
  }

  BOOST_FOREACH(const lm_t & lm, lmsB)
  {
    nodes->InsertNextPoint(lm.pos);
    nodeSurf->InsertNextValue(1);
  }

  vtkIdType o = lmsA.size();

  std::map<corrs_t,vtkIdType> corrs_i;

  BOOST_FOREACH(const corrs_t & cr, corrs)
  {
    vtkIdType arc[]={cr.first,o + cr.second};
    corrs_i[cr] = arcs->InsertNextCell(2,arc);
    arc_nA->InsertNextValue(cr.first);
    arc_nB->InsertNextValue(cr.second);
  }

  arc_nA->SetName("fromNode");
  arc_nB->SetName("toNode");
  nodeSurf->SetName("nodeSurf");

  vtkSmartPointer<vtkUnstructuredGrid> ug = vtkSmartPointer<vtkUnstructuredGrid>::New();
  ug->SetPoints(nodes);
  ug->SetCells(VTK_LINE,arcs);
  ug->GetPointData()->AddArray(nodeSurf);
  ug->GetCellData()->AddArray(arc_nA);
  ug->GetCellData()->AddArray(arc_nB);

  int csetNo = 0;


  BOOST_FOREACH(const corrs_list_t &cl, corrs_sets)
  {
    vtkSmartPointer<vtkIntArray> corrsSetflag =
        vtkSmartPointer<vtkIntArray>::New();

    corrsSetflag->SetNumberOfValues(corrs.size());

    for(int j = 0 ; j < corrs.size() ; ++j)
      corrsSetflag->SetValue(j,0);

    BOOST_FOREACH(const corrs_t & cr, cl)
    {
      corrsSetflag->SetValue(corrs_i[cr],1);
    }

    std::string csetName = "corrsSetNum"+utl::to_string<int>(csetNo++);

    corrsSetflag->SetName(csetName.c_str());

    ug->GetCellData()->AddArray(corrsSetflag);
  }


  vtkSmartPointer<vtkXMLUnstructuredGridWriter> w =
      vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

  w->SetFileName(fn.c_str());
  w->SetInputData(ug);
  w->SetCompressorTypeToZLib();
  w->Write();
}

//void print_corrs_list
//(const std::vector<corrs_list_t> & corrs_sets,
// std::ostream &os=std::cout)
//{
//  BOOST_FOREACH(const corrs_list_t & cl, corrs_sets)
//  {
//    BOOST_FOREACH(const corrs_t & cr, cl)
//    {
//      os << cr.first <<" <--> " << cr.second << endl;
//    }
//    os << endl;
//  }
//}

vtkSmartPointer<vtkDoubleArray > vector_to_vtk_array
(const std::vector<double> &sarr,std::string name = "")
{

  vtkSmartPointer<vtkDoubleArray > arr = vtkSmartPointer<vtkDoubleArray>::New();

  arr->SetNumberOfComponents(1);
  arr->SetNumberOfTuples(sarr.size());

  if(!name.empty()) arr->SetName(name.c_str());

  for(int i = 0 ; i < arr->GetNumberOfTuples(); ++i)
  {
    arr->SetValue(i,sarr[i]);
  }

  return arr;
}

/*---------------------------------------------------------------------------*/


/// \brief A ds to hold logistic information pertinent to a single surface file

struct surface_info_t {
  std::string surfaceFile;      // Input surface in Off Format

  std::string curvDataFile;     // Intermediate surface binary data.
  std::string fpDataFile;       // Intermediate feature point binary data.
  std::string mscDataFile;      // Intermediate mscomplex binary data.

  std::string mscVtkFile;    // Output File in vtp format . Decomposition using maxima
  std::string mscGraphFile ;    // Output File in vtu format . Mscomplex Connectivity


  /// \brief Cosntruct a ptree with relevant data for users
  bpt::ptree get_ptree()
  {
    bpt::ptree pt;
    pt.put("surfaceFile",surfaceFile);
    pt.put("mscVtkFile",mscVtkFile);

    return pt;
  }

  /*---------------------------------------------------------------------------*/
  /** \brief Save Ms Decomposition                                            **/
  void save_mscomplex_vtk(mscomplex_ptr_t msc,tri_cc_geom_ptr_t tcc)
  {
    vtkSmartPointer<vtkPolyData> pd = read_off_to_vtk_pd(this->surfaceFile);


    correspondence_builder_t::lms_t lms;
    lms.read(this->fpDataFile);
    tag_landmark_points(pd,lms);


    vtkSmartPointer<vtkIntArray> regionIdArr
        = vtkSmartPointer<vtkIntArray>::New();
    regionIdArr->SetName("RegionId");
    regionIdArr->SetNumberOfTuples(tcc->get_num_cells_dim(2)+lms.size());

    int j = 0 ;

    for(int i = 0 ; i < regionIdArr->GetNumberOfTuples(); ++i)
      regionIdArr->SetValue(i,-1);

    BOOST_FOREACH
        (int cp, msc->cp_range()|
         badpt::filtered(bind(&mscomplex_t::is_not_paired,msc,_1))|
         badpt::filtered(bind(&mscomplex_t::is_maxima,msc,_1)))
    {
      int o = tcc->get_num_cells_max_dim(1);

      BOOST_FOREACH(int t, msc->m_des_mfolds[cp])
      {
        regionIdArr->SetTuple1(t-o+lms.size(),j);
      }
      ++j;
    }

    for(int i = 0 ; i < lms.size(); ++i)
    {
      regionIdArr->SetValue(i,i);
    }

    double rng[2];
    regionIdArr->GetRange(rng);
    ensure(rng[0] == 0 && rng[1] == j-1,"some tris were not assigned maxids");

    pd->GetCellData()->AddArray(regionIdArr);

    std::vector<double> meanCurvature;
    read_mean_curvature(meanCurvature,this->curvDataFile);
    pd->GetPointData()->AddArray(vector_to_vtk_array(meanCurvature,"meanCurvature"));


    vtkSmartPointer<vtkXMLPolyDataWriter> writer =
        vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName(this->mscVtkFile.c_str());
    writer->SetInputData(pd);
    writer->SetCompressorTypeToZLib();
    writer->Write();
  }




};

/*---------------------------------------------------------------------------*/

/// \brief A ds to hold the necessary input/output filenames and paramters
///        for computing alignments between a pair of surfaces
///
/// \note  This could potentially derive from correspondence_builder_t as
///        it is essentially adding some logistic information for setup
///        before the builder can work.
struct surface_pair_info_t
{
  // Input parameters
  double Tms;
  double Tmrd;
  int    Nicp;
  bool   ormsd;
  int    nbest;

  // Input Surfaces' info
  surface_info_t *siA;
  surface_info_t *siB;

  // Intermediate Common Datastructures
  vtkSmartPointer<vtkPolyData> surfA_pd;
  vtkSmartPointer<vtkPolyData> surfB_pd;

  // Output Data
  std::vector<vtkSmartPointer<vtkTransform> >         transforms;
  std::vector<correspondence_builder_t::corrs_list_t> corrs_sets;

  // Output Data filenames
  std::string aif;
  std::string opsurfAtB_fn;
  std::string opsurfBfA_fn;
  std::string opcorrs_fn;
  std::string opFeatureCorrs_fn;


  /// \brief Cosntruct a ptree with relevant output data/metadata
  bpt::ptree get_ptree()
  {
    bpt::ptree pt;
    pt.put("alignInfoFile",aif);
    pt.put("opSurfAFile",opsurfAtB_fn);
    pt.put("opSurfBFile",opsurfBfA_fn);
    pt.put("fpCorrsFile",opFeatureCorrs_fn);

    bpt::ptree tpt_arr;

    for(int i = 0 ; i < transforms.size(); ++i)
    {
      using boost::format;
      using boost::str;

      double ori[3],pos[3];
      transforms[i]->GetPosition(pos);
      transforms[i]->GetOrientation(ori);

      bpt::ptree tpt;
      tpt.put("position"   ,str(format("[%.6f,%.6f,%.6f]")%pos[0]%pos[1]%pos[2]));
      tpt.put("orientation",str(format("[%.6f,%.6f,%.6f]")%ori[0]%ori[1]%ori[2]));
      tpt.put("correspondanceSetSize",str(format("%d")%corrs_sets[i].size()));

      tpt_arr.push_back(std::make_pair("",tpt));
    }

    pt.add_child("transforms",tpt_arr);

    return pt;
  }

  /// \brief Write the computed alignemtns information to file
  void write_alignment_info_file(const correspondence_builder_t &cb) const
  {
    if (cb.m_corrs_sets.size() == 0 )
      return;

    std::ofstream os(aif.c_str());
    ensure(os.is_open(),"Unable to open File");

    os <<"------------------------------------------------------------" <<endl;

    for ( int i = 0 ; i < cb.m_corrs_sets.size() ; ++ i)
    {
      os << " Corrs Num = " << i << endl;
      os << " Distance = " << cb.m_corrs_sets_xfms_dist[i] << endl;
      os << " Transformation: " << endl;

      vtkMatrix4x4 * mat = cb.m_corrs_sets_xfms[i]->GetMatrix();

      for(int j = 0 ; j < 4; ++ j)
      {
        for(int k = 0 ; k < 4; ++k)
          os << mat->GetElement(j,k) << "\t";

        os << endl;
      }

      os << " Orientation: "
           << cb.m_corrs_sets_xfms[i]->GetOrientation()[0] << "\t "
           << cb.m_corrs_sets_xfms[i]->GetOrientation()[1] << "\t "
           << cb.m_corrs_sets_xfms[i]->GetOrientation()[2] << "\t "
           << endl;

      os << " Position: "
           << cb.m_corrs_sets_xfms[i]->GetPosition()[0] << "\t "
           << cb.m_corrs_sets_xfms[i]->GetPosition()[1] << "\t "
           << cb.m_corrs_sets_xfms[i]->GetPosition()[2] << "\t "
           << endl;

      os << "Correspondence: " << endl;
      os << cb.m_corrs_sets[i].size() << endl;
      for(int j = 0 ; j < cb.m_corrs_sets[i].size(); ++ j)
        os << cb.m_corrs_sets[i][j].first << "\t "
             << cb.m_corrs_sets[i][j].second << "\t "
             << cb.m_lmsA[cb.m_corrs_sets[i][j].first].vertid << "\t "
             << cb.m_lmsB[cb.m_corrs_sets[i][j].second].vertid << "\t "
             << endl;

      double areaFracA = get_area_fraction<0>(cb.m_corrs_sets[i],cb.m_lmsA);
      double areaFracB = get_area_fraction<1>(cb.m_corrs_sets[i],cb.m_lmsB);

      os << " Area Frac A = " << areaFracA<< endl;
      os << " Area Frac B = " << areaFracB<< endl;

      if(ormsd)
      {
//        if(!surfA_pd) surfA_pd = read_off_to_vtk_pd(surfA);
//        if(!surfB_pd) surfB_pd = read_off_to_vtk_pd(surfB);

        os << " RMS Distance AB = " << get_rms_distance(surfA_pd,surfB_pd,cb.m_corrs_sets_xfms[i])
           << endl;
      }

      os<<"------------------------------------------------------------"<<endl;
    }

    os.close();
  }

  /// \brief Write the surface with the alignment data augmented to it
  void write_output_surface(const correspondence_builder_t &cb, int surfnum) const
  {
    ENSURES(surfnum == 0 || surfnum == 1) << "Incorrect surfnum !!!" << SVAR(surfnum);

    BOOST_AUTO(pd,                    ((surfnum == 0)?(surfA_pd):(surfB_pd)));
    BOOST_AUTO(const &lms,            ((surfnum == 0)?(cb.m_lmsA):(cb.m_lmsB)));
    BOOST_AUTO(fn,                    ((surfnum == 0)?(opsurfAtB_fn):(opsurfBfA_fn)));
    BOOST_AUTO(curvf,                 ((surfnum == 0)?(siA->curvDataFile):(siB->curvDataFile)));

    for(int i = 0; i < cb.m_corrs_sets.size(); ++i)
    {
      vtkSmartPointer<vtkDoubleArray> rt =(surfnum == 0)?
            (get_corrs_region_tags<0>(pd,lms,cb.m_corrs_sets[i])):
            (get_corrs_region_tags<1>(pd,lms,cb.m_corrs_sets[i]));

      rt->SetName(("RegionTag"+utl::to_string(i)).c_str());
      pd->GetPointData()->AddArray(rt);


      //      vtkSmartPointer<vtkDoubleArray> clp =
      //          compute_closest_point_dist(pA,pB);
      //      clp->SetName("ClosestPointAtoB"+utl::to_string(i));
      //      pA->GetPointData()->AddArray(clp);


    }

    if(surfnum == 0)
      tag_landmark_points(pd,cb.m_lmsA);
    else
      tag_landmark_points(pd,cb.m_lmsB);


    std::vector<double> meanCurvature;
    read_mean_curvature(meanCurvature,curvf);
    pd->GetPointData()->AddArray(vector_to_vtk_array(meanCurvature,"meanCurvature"));


    vtkSmartPointer<vtkXMLPolyDataWriter> writer =
        vtkSmartPointer<vtkXMLPolyDataWriter>::New();

    writer->SetFileName(fn.c_str());
    writer->SetInputData(pd);
    writer->SetCompressorTypeToZLib();
    writer->Write();


  }



};

void compute_alignment(surface_pair_info_t & spi)
{

  std::stringstream log_ss;

  log_ss << "Aligning "<< spi.aif << endl;

  // These almost never need to be touched
  int    fast_nbest       = max(10,spi.nbest*2); // retains twice the best n corrs
  double fast_dist_factor = 3; // keep corrs with scores < dist_factor * min_score
  int    fast_ndup        = 5; // keep coors that intersect along atmost ndup

  int    prec_nbest       = spi.nbest;
  double prec_dist_factor = 1.5;
  int    prec_ndup        = 3;

  /*---------------- build corresspondences ---------------------------------*/

  correspondence_builder_t cb;

  cb.read_lm_info(spi.siA->fpDataFile,spi.siB->fpDataFile);

  cb.build_correspondences(spi.Tms,5);
  log_ss << " # Fp A = " << cb.m_lmsA.size() << endl;
  log_ss << " # Fp B = " << cb.m_lmsB.size() << endl;

  int ne = cb.build_correspondence_sets(spi.Tmrd);
  log_ss << " # Corrs = " << cb.m_corrs.size() <<endl;
  log_ss << " # Edges = " << ne <<endl;
  log_ss << " # Correspondances " << cb.m_corrs_sets.size() << endl;

  /*---------------- Evaluate the correspondances Quickly -------------------*/

  log_ss << "Evaluating correspondances : Fast" <<endl;
  cb.build_transforms();
  cb.compute_alignment_distances_fast();

  BOOST_AUTO(&transforms,cb.m_corrs_sets_xfms);
  BOOST_AUTO(&tfm_dists,cb.m_corrs_sets_xfms_dist);
  BOOST_AUTO(&corrs_sets,cb.m_corrs_sets);
  BOOST_AUTO(&corrs,cb.m_corrs);
  BOOST_AUTO(&lmsA,cb.m_lmsA);
  BOOST_AUTO(&lmsB,cb.m_lmsB);


//  sort_corrs_by_dist(tfm_dists,transforms,corrs_list);
//  filter_corrs_by_unique(tfm_dists,transforms,corrs_list,fast_ndup);
//  filter_corrs_by_topN(tfm_dists,transforms,corrs_list,fast_nbest,fast_dist_factor);

//  filter_corrs_by_normal_angle(tfm_dists,transforms,corrs_list,lmsA,lmsB,M_PI/2.0);
//  filter_corrs_by_distance(tfm_dists,transforms,corrs_list,2);


  sort_corrs_by_dist_over_area_fraction(tfm_dists,transforms,corrs_sets,lmsA,lmsB);

  filter_corrs_by_unique(tfm_dists,transforms,corrs_sets,fast_ndup);
  log_ss << "Filtered unique " << SVAR(corrs_sets.size()) << endl;

  filter_corrs_by_firstN(tfm_dists,transforms,corrs_sets,fast_nbest);
  log_ss << "Filtered fast Nbest " << SVAR(corrs_sets.size()) << endl;

  log_ss << "Evaluating correspondances : Precise" <<endl;

//  sort_corrs_by_dist(tfm_dists,transforms,corrs_list);
//  filter_corrs_by_unique(tfm_dists,transforms,corrs_list,prec_ndup);
//  filter_corrs_by_topN(tfm_dists,transforms,corrs_list,prec_nbest,prec_dist_factor);

//  filter_corrs_by_distance(tfm_dists,transforms,corrs_list,1);
  sort_corrs_by_dist_over_area_fraction(tfm_dists,transforms,corrs_sets,lmsA,lmsB);

  filter_corrs_by_unique(tfm_dists,transforms,corrs_sets,prec_ndup);
  log_ss << "Filtered unique " << SVAR(corrs_sets.size()) << endl;

  filter_corrs_by_firstN(tfm_dists,transforms,corrs_sets,prec_nbest);
  log_ss << "Filtered prec Nbest " << SVAR(corrs_sets.size()) << endl;

  log_ss << "Done Evaluating correspondances" <<endl;


  br::copy(cb.m_corrs_sets_xfms,std::back_inserter(spi.transforms));
  br::copy(cb.m_corrs_sets,std::back_inserter(spi.corrs_sets));

  /*------------- 4. Output Score and transform -----------------------------*/

  LOG_MACRO(info) << log_ss.str();

  spi.write_alignment_info_file(cb);

  if(spi.opsurfAtB_fn.size() !=0 ) spi.write_output_surface(cb,0);
  if(spi.opsurfBfA_fn.size() !=0 ) spi.write_output_surface(cb,1);

  if(!spi.opFeatureCorrs_fn.empty())
    save_feature_point_correspondence_vtk(spi.opFeatureCorrs_fn,lmsA,lmsB,corrs,corrs_sets);
}


/*---------------------------------------------------------------------------*/

/** \brief main function                                                    **/
int main(int argc, char **argv)
{

#if MS3ALIGN_MEMORY_LIMIT > 0
  {
    struct rlimit rl;

    long lim = 1024*1024;
    lim *= MS3ALIGN_MEMORY_LIMIT;

    rl.rlim_cur = lim;
    rl.rlim_max = lim;
    setrlimit (RLIMIT_AS, &rl);
  }
#endif


  std::vector<std::string> surfFiles;
  std::string tempFileDir;
  std::string opFileDir;
  std::string opJsonFile;
  bool   firstToAll;

  double Rcurv;

  double Ts;
  bool   opMsComplex;
  bool   opVtkFpCorrs; // output the feature Point correspondance in a .vtu file

  double MsCurvInner,MsCurvOuter;
  int MsCurvNumScales;

  bool   opsurfAtB;
  bool   opsurfB;
  bool   opcorrs;
  double Tms;
  double Tmrd;
  int    Nicp = 0;
  bool   ormsd;
  int    nbest;
  bool   deleteTempFileDir = false;
//  int    cachedMode;

  bpo::positional_options_description p;
  p.add("surfFiles", -1);

  // List of single letter option shortcuts used
  // a,b,c,d,e,f,g,h,n,o,r,s,t

  bpo::options_description desc("Allowed options");
  desc.add_options()
      ("help,h", "produce help message")
      
      // Data
      ("surfFiles",bpo::value(&surfFiles)->required(),"List of Surface Files")      
      
      
      // Parameters
      ("rcurv,r",bpo::value(&Rcurv)->default_value(1.2),
       "Radius of nbd for curvature computation")
      ("ts",bpo::value(&Ts)->default_value(0.1),
       "Simplification threshold given as a fraction of the "\
       "average of the maximum positive curvatures of each surface ")
      ("tms",bpo::value(&Tms)->default_value(-1),
       "Threshold for l-infinity norm to consider multiscale curvatures same"\
       "-1 indicates that Tms = Ts")
      ("tmrd",bpo::value(&Tmrd)->default_value(-1),
       "Maximum relative distance between corrsponding points"\
       "-1 indcates default (tmrd=rcurv")

      // Options
      ("firstToAll,f",bpo::value(&firstToAll)->default_value(false),
       "Enables firstToAll mode where only the first surface is aligned with "\
       "all others. The default implicitly is all to all.")
      ("NumAligns,n",bpo::value(&nbest)->default_value(true),
       "Output best n alignments")
      
      // Output/Temp locations
      ("tempFileDir,t",bpo::value(&tempFileDir)->default_value(""),
       "Directory where temp results are cached")
      ("opFileDir,o",bpo::value(&opFileDir)->default_value("./"),
       "Directory to put output files into")
//      ("cachedMode",bpo::value(&cachedMode)->default_value(-1),
//       "Uses tempFileDir to cache computed curvatures, MS complexes, &"\
//       "Landmark points \n"\
//       "-1 indicates that it will be used for aligning >5 files else not")

      // Advanced Parameters
      ("MsCurvInner",bpo::value(&MsCurvInner)->default_value(-1),
       "Multiscale curv radius inner (-1 indicates Rcurv)")
      ("MsCurvOuter",bpo::value(&MsCurvOuter)->default_value(-1),
       "Multiscale curv radius outer (-1 indicates 2*MsCurvInner)")
      ("MsCurvNumScales",bpo::value(&MsCurvNumScales)->default_value(15),
       "Number of Scales for multi-scale Curvature computation")
      
      // Advanced Output options
      ("OutputVtkAtoBSurf,a",bpo::value(&opsurfAtB)->default_value(true),
       "Enable output of surfA alignment Data(in vtp Format)")
      ("OutputVtkBSurf,b",bpo::value(&opsurfB)->default_value(true),
       "Enable output of surfB alignment Data(in vtp Format)")
      ("OutputVtkFeaturePointCorrs,c",bpo::value(&opVtkFpCorrs)->default_value(true),
       "Ouptut the inital list of corresponences")
      ("OutputVtkMsComplex,m",bpo::value(&opMsComplex)->default_value(true),
       "Output decomposition given by mscomplex")
      ("OutputJsonFile,j",bpo::value(&opJsonFile)->default_value("output.json"),
       "Output a Json File with meta data for this execution")

      ("RmsScore,s",bpo::value(&ormsd)->default_value(false),
       "Compute and output rms scores of alignments")

      ;

  try
  {
    bpo::variables_map vm;
    bpo::store(bpo::command_line_parser(argc, argv).
               options(desc).positional(p).run(), vm);

    bpo::notify(vm);

    if(MsCurvInner == -1)
      MsCurvInner = Rcurv;

    if(MsCurvOuter == -1)
      MsCurvOuter = 2*MsCurvInner;

    if(Tms == -1)
      Tms =Ts;

    if(Tmrd == -1)
      Tmrd = Rcurv;

    if (vm.count("help"))
      throw std::runtime_error("Help Message");

    ensure_ia(surfFiles.size() >= 2,
              "Too Few Surfaces");
    ensure_ia(is_in_range(Ts,0,1),
              "Ts should be Normalized in [0,1]");
    ensure_ia(is_in_range(Tms,0,1),
              "Tms should be Normalized in [0,1]");
    ensure_ia(Tmrd > 0,
              "Tmrd Should be a positive value");
    ensure_ia(MsCurvInner <  MsCurvOuter && MsCurvInner > 0,
              "Invalid MultiScale curvature given");
    ensure_ia(MsCurvNumScales == 15,
              "MsCurvNumScales has to be 15 ( For now )");

    if(tempFileDir.empty())
    {
      tempFileDir = bfs::unique_path(".ms3align-%%%%-%%%%-%%%%-%%%%").string();
      deleteTempFileDir = true;
    }

    if(bfs::path(tempFileDir).is_relative())
      tempFileDir = (bfs::current_path()/tempFileDir).string();

    if(bfs::path(opFileDir).is_relative())
      opFileDir   = (bfs::current_path()/opFileDir).string();

    if(!bfs::exists(tempFileDir))
      bfs::create_directories(tempFileDir);

    ensure(bfs::is_directory(tempFileDir),"Could not create tempfile Dir!!");

    if(!bfs::exists(opFileDir))
      bfs::create_directories(opFileDir);

    ensure(bfs::is_directory(opFileDir),"Could not create op Dir!!");
  }
  catch(std::exception &e)
  {
    cerr << e.what() << endl;
    cerr << desc << endl;
    return 0;
  }

  utl::timer tim;

  double aveFmax = 0;

  using boost::format;
  using boost::str;

  std::vector<surface_info_t> surfacesInfo(surfFiles.size());

  for(int i = 0 ; i < surfFiles.size(); ++i)
  {
    std::string &surfFile = surfFiles[i];

    std::string tmpNameRoot = (bfs::path(tempFileDir)/bfs::path(surfFile).stem()).string();
    std::string opNameRoot  = (bfs::path(opFileDir)/bfs::path(surfFile).stem()).string();

    surface_info_t &si = surfacesInfo[i];
    si.surfaceFile   = surfFiles[i];
    si.curvDataFile  = str(format("%s_Rc%.2f_meancurv.bin")        %tmpNameRoot%Rcurv);
    si.fpDataFile    = str(format("%s_Rc%.2f_Ts%.2f_fpoints.bin")  %tmpNameRoot%Rcurv%Ts);
    si.mscDataFile   = str(format("%s_Rc%.2f_Ts%.2f_mscomplex.bin")%tmpNameRoot%Rcurv%Ts);
    si.mscVtkFile    = str(format("%s_Rc%.2f_Ts%.2f_mscomplex.vtp") %opNameRoot%Rcurv%Ts);;


    // First compute the curvature
    std::vector<double> meanCurvature;

    tri_cc_geom_ptr_t tcc;

    double fmax = 0;

    if( !bfs::exists(si.curvDataFile))
    {
      if(!tcc)
        tcc = read_off(si.surfaceFile);

      fmax =

      compute_and_save_mean_curvature(si.curvDataFile,meanCurvature,tcc,Rcurv).second;
    }
    else
    {
      fmax = read_mean_curvature_min_max(si.curvDataFile).second;
    }

    aveFmax += fmax;
  }

  LOG_MACRO(info) << "CurvatureTime = " <<tim.elapsed()<< endl;tim.restart();

  aveFmax /= surfFiles.size();

  double Ts_  = Ts*aveFmax;
  double Tms_ = Tms*aveFmax;

//  Ts  *= 1.0 / Rcurv;
//  Tms *= 1.0 / Rcurv;

  LOG_MACRO(info)
      << "      aveFmax = " << aveFmax << endl
      << "Ts  * aveFmax = " << Ts_ << endl
      << "Tms * aveFmax = " << Tms_ << endl;

  #pragma omp parallel for
  for(int i = 0 ; i < surfacesInfo.size(); ++i)
  {
    surface_info_t &si = surfacesInfo[i];

    // Second, compute the MsComplex

    std::vector<double> meanCurvature;

    tri_cc_geom_ptr_t tcc;

    mscomplex_ptr_t msc;

    correspondence_builder_t::lms_t fps;

    if( !bfs::exists(si.mscDataFile))
    {
      if(!tcc)
        tcc = read_off(si.surfaceFile);

      if(meanCurvature.size() == 0)
        read_mean_curvature(meanCurvature,si.curvDataFile);

      msc = compute_mscomplex(si.mscDataFile,tcc->get_tri_cc(),meanCurvature,Ts_);
    }

    // Third, compute feature points
    if( !bfs::exists(si.fpDataFile))
    {
      if(!tcc)
        tcc = read_off(si.surfaceFile);

      if(!msc)
        msc.reset(new mscomplex_t(si.mscDataFile));

      compute_feature_points
          (si.fpDataFile,fps,msc,tcc,Ts_,MsCurvInner,MsCurvOuter,
           MsCurvNumScales);

      if(opMsComplex)
        si.save_mscomplex_vtk(msc,tcc);
    }
  }

  LOG_MACRO(info) << "FeaturePointTime = " <<tim.elapsed()<< endl;tim.restart();

  int_pair_list_t alist;

  for(int i = 0 ; i < ((firstToAll)?(1):(surfFiles.size())); ++i)
    for(int j = i+1 ; j < surfFiles.size(); ++j)
      alist.push_back(int_pair_t(i,j));

  std::vector<surface_pair_info_t> surfacePairsInfo(alist.size());

  #pragma omp parallel for
  for(int i = 0 ; i < alist.size() ; ++i)
  {
    surface_pair_info_t &api = surfacePairsInfo[i];

    api.siA = &surfacesInfo[alist[i].first];
    api.siB = &surfacesInfo[alist[i].second];

    std::string aStem = bfs::path(api.siA->surfaceFile).stem().string();
    std::string bStem = bfs::path(api.siB->surfaceFile).stem().string();

    std::string opNameRootA = (bfs::path(opFileDir)/ str(format("%s_to_%s")%aStem%bStem)).string();
    std::string opNameRootB = (bfs::path(opFileDir)/ str(format("%s_from_%s")%bStem%aStem)).string();
    std::string paramTail   = str(format("Rc%.2f_Ts%.2f_Tms%.2f_Tmrd%.2f")%Rcurv%Ts%Tms%Tmrd);

                      api.aif               = opNameRootA+"_" + paramTail+".txt";
    if( opsurfAtB)    api.opsurfAtB_fn      = opNameRootA+"_" + paramTail+".vtp";
    if( opsurfB)      api.opsurfBfA_fn      = opNameRootB+"_" + paramTail+".vtp";
    if( opcorrs)      api.opcorrs_fn        = opNameRootA+"_" + paramTail+"_corrs.vtu";
    if( opVtkFpCorrs) api.opFeatureCorrs_fn = opNameRootA+"_" + paramTail+"_fpCorrs.vtu";

    api.Tms   = Tms_;
    api.Tmrd  = Tmrd;
    api.Nicp  = Nicp;
    api.ormsd = ormsd;
    api.nbest = nbest;

    api.surfA_pd =  read_off_to_vtk_pd(api.siA->surfaceFile);
    api.surfB_pd =  read_off_to_vtk_pd(api.siB->surfaceFile);

    compute_alignment(api);
  }

  LOG_MACRO(info) << "AlignmentTime = " <<tim.elapsed()<< endl;tim.restart();

  if(deleteTempFileDir)
    bfs::remove_all(tempFileDir);

  if(!opJsonFile.empty())
  {
    bpt::ptree pt,p_pt,s_pt,sp_pt;

    p_pt.put("Rc"  ,str(format("%0.4f")%Rcurv));
    p_pt.put("Ts"  ,str(format("%0.4f")%Ts));
    p_pt.put("Tms" ,str(format("%0.4f")%Tms));
    p_pt.put("Tmrd",str(format("%0.4f")%Tmrd));

    BOOST_FOREACH(surface_info_t &si, surfacesInfo)
      s_pt.push_back(std::make_pair("",si.get_ptree()));

    BOOST_FOREACH(surface_pair_info_t &spi,surfacePairsInfo)
      sp_pt.push_back(std::make_pair("",spi.get_ptree()));

    pt.add_child("Parameters",p_pt);
    pt.add_child("Surfaces",s_pt);
    pt.add_child("SurfacePairs",sp_pt);

    bpt::write_json(opJsonFile.c_str(),pt);
  }
}
/*****************************************************************************/
