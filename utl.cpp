#include "utl.h"

#include <sstream>
#include <fstream>

#include <vector>

#include <tr1/functional>


using namespace std;

namespace utl
{

void trim(std::string &s)
{
  s.erase(s.begin(), std::find_if
          (s.begin(), s.end(),
           std::not1(std::ptr_fun<int, int>(std::isspace))));
  s.erase(std::find_if
          (s.rbegin(), s.rend(),
           std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

file_line_iterator::file_line_iterator(const char * f,char c_char):
  is(new std::ifstream(f)),c_char(c_char)
{
  ENSUREV(is->is_open(),"cannot read the file",f);
  increment();
}

void file_line_iterator::increment()
{
  value.clear();

  while(is && value.size() == 0)
  {
    if(is->eof())
    {
      is.reset();
      value.clear();
      break;
    }

    std::getline(*is,value);

    int p = value.find(c_char);

    if ( p < value.size() )
      value = value.substr(0,p);

    trim(value);
  }
}

bool file_line_iterator::equal(file_line_iterator const& other) const
{
  if(!is && !other.is)
    return true;

  if(!is || !other.is)
    return false;

  ENSURE(is == other.is, "cannot compare distinct istream iters");

  return is->tellg() == other.is->tellg();
}

const std::string & file_line_iterator::dereference() const
{
  ENSURE(is,"dereferencing past end of line stream");
  return value;
}

boost::mutex logger::s_mutex;
logger       logger::s_logger;

void read_off(const std::string &fname,
	      std::vector<double> &verts, 
	      std::vector<int> &tris)
{
  file_line_iterator lgen(fname.c_str()),lend;

  ENSUREV(*lgen++ == "OFF","Doesnt appear to be an OFF file",fname);

  int i = 0, j = 0 , nv = 0 , nt = 0 , np = 0;

  from_string(*lgen++,nv,nt);
  
  verts.resize(nv*3);
  tris.resize(nt*3);

  for(i = 0 ; i < nv && lgen != lend ; ++i)
    from_string(*lgen++,verts[3*i],verts[3*i+1],verts[3*i+2]);

  ENSUREV(i == nv,"Vertices ran out prematurely",fname);  

  for(j = 0 ; j < nt && lgen != lend ; ++j)
  {
    from_string(*lgen++,np,tris[3*j],tris[3*j+1],tris[3*j+2]);

    ENSUREV(np == 3,"can read triangles only",np);
  }

  ENSUREV(j == nt,"Tris ran out prematurely",fname);
}


void write_off(const std::string &fname,
              const std::vector<double> &verts,
              const std::vector<int> &tris)
{
  std::fstream offFile ( fname.c_str(), std::ios::out );

  ENSURE(offFile.is_open(),"cannot open the file for write");

  offFile << "OFF" <<std::endl;

  offFile <<verts.size()/3 <<" " << tris.size()/3 << " 0"<<std::endl;

  for(int i = 0 ; i < verts.size()/3; ++i)
  {
    offFile << verts[3*i]   << " "
            << verts[3*i+1] << " "
            << verts[3*i+2] << std::endl;
  }

  for(int i = 0 ; i < tris.size()/3 ; ++i)
  {
    offFile <<"3"           << " "
            << tris[3*i]   << " "
            << tris[3*i+1] << " "
            << tris[3*i+2] << std::endl;
  }

}

template <> double from_string<double>(std::string s) {return atof(s.c_str());}
template <> int    from_string<int>(std::string s) {return atoi(s.c_str());}
template <> std::string from_string<std::string>(std::string s) {return s;}

bool extract_opt(std::string opt,std::vector<std::string> &args)
{
  std::vector<std::string>::iterator it;

  it = find(args.begin(),args.end(),opt);

  if(it != args.end())
  {
    args.erase(it);
    return true;
  }
  else
    return false;
}

const static char encodeLookup[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const static char padCharacter = '=';

void write_b64(std::ostream &os, const unsigned char * cursor, int N)
{
  unsigned long int temp;

  int i=0;

  for(; i < N/3; ++i)
  {
    temp  = (*cursor++) << 16; //Convert to big endian
    temp += (*cursor++) << 8;
    temp += (*cursor++);

    os.put(encodeLookup[(temp & 0x00FC0000) >> 18]);
    os.put(encodeLookup[(temp & 0x0003F000) >> 12]);
    os.put(encodeLookup[(temp & 0x00000FC0) >> 6]);
    os.put(encodeLookup[(temp & 0x0000003F)]);

    if(i%19 == 18)
      os.put('\n');
  }

  if(N%3)
  {
    if(i%19 == 0 && i!=0)
      os.put('\n');

    switch(N%3)
    {
    case 2:
      temp  = (*cursor++) << 16;
      temp += (*cursor++) << 8;

      os.put(encodeLookup[(temp & 0x00FC0000) >> 18]);
      os.put(encodeLookup[(temp & 0x0003F000) >> 12]);
      os.put(encodeLookup[(temp & 0x00000FC0) >> 6]);
      os.put(padCharacter);
      break;
    case 1:
      temp  = (*cursor++) << 16;
      os.put(encodeLookup[(temp & 0x00FC0000) >> 18]);
      os.put(encodeLookup[(temp & 0x0003F000) >> 12]);
      os.put(padCharacter);
      os.put(padCharacter);
      break;
    }
  }
}


char get_next(std::istream &is)
{
  char c;

  do
  {
    c = is.get();

    if(is.eof())
      throw std::runtime_error("Stream Empty !!!");
  }
  while(c == '\n' || c == '\r' || c == '\t' || c == ' ');

  return c;
}


unsigned char decode_next(std::istream &is)
{
  char c = get_next(is);

  unsigned char ret = 0;

  if       (c >= 0x41 && c <= 0x5A) // This area will need tweaking if
    ret = c - 0x41;                 // you are using an alternate alphabet
  else if  (c >= 0x61 && c <= 0x7A)
    ret = c - 0x47;
  else if  (c >= 0x30 && c <= 0x39)
    ret = c + 0x04;
  else if  (c == 0x2B)
    ret = 0x3E; //change to 0x2D for URL alphabet
  else if  (c == 0x2F)
    ret = 0x3F; //change to 0x5F for URL alphabet
  else if  (c == padCharacter) //pad
    throw std::runtime_error("Misplaced padding char!");
  else
    throw std::runtime_error("Non-Valid Character in Base 64!");

  return ret;
}

void read_b64(std::istream &is, unsigned char * cursor, int N)
{
  unsigned long int temp;

  for(int i = 0 ; i < N/3 ; ++i)
  {
    temp  = decode_next(is); temp <<= 6;
    temp |= decode_next(is); temp <<= 6;
    temp |= decode_next(is); temp <<= 6;
    temp |= decode_next(is);

    *cursor++ = (temp >> 16) & 0x000000FF;
    *cursor++ = (temp >> 8 ) & 0x000000FF;
    *cursor++ = (temp      ) & 0x000000FF;
  }

  switch(N%3)
  {
  case 2:
    temp  = decode_next(is); temp <<= 6;
    temp |= decode_next(is); temp <<= 6;
    temp |= decode_next(is);

    if(get_next(is) != padCharacter)
      throw std::runtime_error("Missing Pad character");

    *cursor++ = (temp >> 10) & 0x000000FF;
    *cursor++ = (temp >> 2 ) & 0x000000FF;
    break;
  case 1:
    temp  = decode_next(is); temp <<= 6;
    temp |= decode_next(is);

    if(get_next(is) != padCharacter)
      throw std::runtime_error("Missing Pad character");

    if(get_next(is) != padCharacter)
      throw std::runtime_error("Missing Pad character");

    *cursor++ = (temp >> 4 ) & 0x000000FF;
    break;
  }
}

//---------------------------------------------------------------------------//

struct atom_line_t
{
  char at_nm[8];
  char res_nm[8];
  char chn_nm;
  int          res_no;
  std::string  res_str;
  double x,y,z;
  double q,r;
  std::string  pos_str;

  bool operator == (const atom_line_t & rhs) const;
  void operator = (const atom_line_t & rhs);
};

//---------------------------------------------------------------------------//

ostream & operator << (ostream & os, const atom_line_t & l)
{
  os <<"AtNm    :"<< l.at_nm  << endl;
  os <<"ResNm   :"<< l.res_nm << endl;
  os <<"ChnNm   :"<< l.chn_nm << endl;
//  os <<"ResNo   :"<< l.res_no << endl;
  os <<"ResStr  :"<< l.res_str<< endl;
  os <<"Coords  :"<< l.x << "," << l.y << "," << l.z << endl;
  os <<"Charge  :"<< l.q << endl;
  os <<"Radius  :"<< l.r << endl;
  os <<"Pos_str :"<< l.pos_str << endl;

  return os;
}

//---------------------------------------------------------------------------//

bool atom_line_t::operator == (const atom_line_t & rhs) const
{
  const double eq_t = 0.1; // Angstroms

//  bool at_eq =
//      (strcmp(at_nm,rhs.at_nm) == 0)
//      && (strcmp(res_nm,rhs.res_nm) == 0)
//      && chn_nm == rhs.chn_nm
//      && res_no == rhs.res_no;

  bool pos_eq =
      std::abs(x - rhs.x) < eq_t &&
      std::abs(y - rhs.y) < eq_t &&
      std::abs(z - rhs.z) < eq_t;

//  if(at_eq != pos_eq)
//  {
//    cerr << "Position and atom fields disagree !!!"<<endl;
//    cerr << *this <<endl << rhs <<endl;

//    throw std::runtime_error("atom line error");
//  }

  return pos_eq;
}

//---------------------------------------------------------------------------//

void atom_line_t::operator = (const atom_line_t & rhs)
{
  strcpy(at_nm, rhs.at_nm);
  strcpy(res_nm, rhs.res_nm);
  chn_nm = rhs.chn_nm;
  res_no = rhs.res_no;
  res_str= rhs.res_str;
  pos_str= rhs.pos_str;
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;
  q = rhs.q;
  r = rhs.r;
}

//---------------------------------------------------------------------------//

template<bool is_pqr,bool is_het>
bool get_next_line(std::istream &is, atom_line_t & pln)
{
  std::string ln;

  while(true)
  {
    getline(is,ln);

    if(is.eof())
      return false;

    if(!is_het && ln.substr(0,4) != "ATOM")
      continue;

    if(is_het && ln.substr(0,6) != "HETATM")
      continue;

//    pln.at_no = from_string<int>(ln.substr(6,6));
    strncpy(pln.at_nm ,ln.c_str()+12,4);pln.at_nm [4] = '\0';
    strncpy(pln.res_nm,ln.c_str()+17,3);pln.res_nm[3] = '\0';
    pln.chn_nm = ln[21];
    pln.res_str=                    (ln.substr(22,5));
    pln.res_no = from_string<int>   (ln.substr(22,4));
    pln.x      = from_string<double>(ln.substr(30,8));
    pln.y      = from_string<double>(ln.substr(38,8));
    pln.z      = from_string<double>(ln.substr(46,8));
    pln.pos_str=                    (ln.substr(30,24));

    if(is_pqr)
    {
      pln.q      = from_string<double>(ln.substr(54,8));
      pln.r      = from_string<double>(ln.substr(62,8));
    }
    else
    {
      pln.q      = -1;
      pln.r      = -1;
    }
    return true;
  }

  throw std::runtime_error("How did I get here !!!");
  return false;
}

//---------------------------------------------------------------------------//

void check_next_atom_line(const atom_line_t &pln, const atom_line_t &lpln)
{
  try{
//    if( pln.chn_nm < lpln.chn_nm)
//    {
//      cerr << " Chain numbers cannot be decreasing !!!" <<endl;
//      throw std::runtime_error("Atom line error");
//    }

    if( pln.chn_nm != lpln.chn_nm)
    {
      return;
    }

//    if ( pln.res_no < lpln.res_no)
//    {
//      cerr << " consective residues no.s cannot be decreasing !!!" << endl;
//      throw std::runtime_error("Atom line error");
//    }
  }
  catch(std::exception){
    cerr << "Last Line : " << endl << lpln << endl;
    cerr << "Cur  Line : " << endl <<  pln << endl;
    throw;
  }
}

//---------------------------------------------------------------------------//

template<bool is_het,typename iter_t>
void read_all_atom_lines(string fn,iter_t iter)
{
  try {
  using std::tr1::placeholders::_1;
  using namespace std::tr1;

  fstream fs(fn.c_str(),ios::in);

  assert(fs.is_open() && "Cant open file !!!");

  function<bool(atom_line_t &) > get_next_ln;

  if(fn.substr(fn.size()-4,4) == ".pdb")
    get_next_ln=bind(get_next_line<is_het,false>,ref(fs),_1);
  else if(fn.substr(fn.size()-4,4) == ".pqr")
    get_next_ln=bind(get_next_line<is_het,false>,ref(fs),_1);

  atom_line_t ln,lln;

  get_next_ln(ln);

  while(true)
  {
    *iter++ = ln;

    lln = ln;

    if (!get_next_ln(ln))
      break;

    check_next_atom_line(ln,lln);
  }
  }catch (std::exception)  {
    cerr << "Exception when trying to read " << fn << endl;
    throw;
  }
}
}

#include <iterator>

#include <boost/range/algorithm.hpp>

namespace br = boost::range;

namespace utl
{

void get_subset_atoms(string ffn,string sfn,set<int> &subset_atoms)
{

  std::vector<atom_line_t> flns;
  std::vector<atom_line_t> slns;

  read_all_atom_lines<false>(ffn,std::back_inserter(flns));
  read_all_atom_lines<false>(sfn,std::back_inserter(slns));

  // use a one based counter
  int fresctr = 1;

  std::set<int> res_set;

  std::vector<int> resnum(flns.size());

  std::map<string,int> fposstr_to_idx;

  for(int i = 0 ; i < flns.size(); ++i)
  {
    if ( i!= 0 && flns[i].res_str != flns[i-1].res_str)
      fresctr++;

    resnum[i] = fresctr;

    fposstr_to_idx[flns[i].pos_str] = i;
  }

  std::vector<int> subset_missing;

  for(int i = 0 ; i < slns.size(); ++i)
  {
    if(fposstr_to_idx.count(slns[i].pos_str) == 0 )
    {
      subset_missing.push_back(i);
      continue;
    }

    int j = fposstr_to_idx[slns[i].pos_str];

    res_set.insert(resnum[j]);
  }

  for( int i = 0 ; i < resnum.size(); ++i)
    if(res_set.count(resnum[i]) != 0)
      subset_atoms.insert(i);

  if(subset_missing.size() != 0 )
    cerr<<"Warning!!! Some atoms in subset were not found in full set."<<endl;
  for(int i = 0 ; i < subset_missing.size(); ++i)
    cerr << slns[subset_missing[i]] << endl;

}

//---------------------------------------------------------------------------//

void read_protein
(string fn,vector<double> &_coords,vector<double> &_radius)
{
  try {

  atom_line_t fln,lfln;

  fstream f_fs(fn.c_str(),ios::in);

  assert(f_fs.is_open() && "Cant open file !!!");

  // has to be from pqr only
  get_next_line<true,false>(f_fs,fln);

  while(true)
  {
    _coords.push_back(fln.x);
    _coords.push_back(fln.y);
    _coords.push_back(fln.z);
    _radius.push_back(fln.r);

    lfln = fln;

    if(!get_next_line<true,false>(f_fs,fln))
      break;

    check_next_atom_line(fln,lfln);
  }
  }catch (std::exception)  {
    cerr << "Exception when trying to read " << fn << endl;
    throw;
  }
}

//---------------------------------------------------------------------------//

void read_ligand
(string fn,string lig_name,vector<double> &_coords,vector<double> &_radius)
{
  atom_line_t fln,lfln;

  fstream f_fs(fn.c_str(),ios::in);

  assert(f_fs.is_open() && "Cant open file !!!");

  // has to be from pqr only
  get_next_line<true,true>(f_fs,fln);

  while(true)
  {
    if(lig_name ==  fln.res_nm)
    {
      _coords.push_back(fln.x);
      _coords.push_back(fln.y);
      _coords.push_back(fln.z);
      _radius.push_back(fln.r);
    }

    lfln = fln;

    if(!get_next_line<true,true>(f_fs,fln))
      break;

    check_next_atom_line(fln,lfln);
  }
}

//---------------------------------------------------------------------------//

}
