/** \brief  Feature point alignment tool main header file.                  **/

#ifndef __FPALIGN_H_INCLUDED__
#define __FPALIGN_H_INCLUDED__

#include <vector>
#include <string>
#include <iostream>
#include <set>

#include <vtkSmartPointer.h>

class vtkDoubleArray;
class vtkPolyData;
class vtkTransform;
class vtkDataSet;
class vtkUnstructuredGrid;

/*****************************************************************************/

class correspondence_builder_t
{

public:

  /** \brief correspondance pair type                                         **/
  typedef std::pair  <int,int>         corrs_t;

  /** \brief a list of correspondances                                        **/
  typedef std::vector< corrs_t >  corrs_list_t;

  /** \brief a set  of correspondances                                        **/
  typedef std::set   < corrs_t >  corrs_set_t;

  /** \brief Data structure to hold landmark info                             **/
  struct lm_t
  {
    int              vertid;
    int              index;
    double           pos[3];
    //  double           upos[3];
    //  double           posUncert;
    double           ms_curv[15];
    double           area;
    double           normal[3];


    std::vector<int> mfold;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
      ar& vertid;
      ar& index;
      for(int i = 0 ; i < 3 ; ++i)
      {
        ar & pos[i];
        ar & normal[i];
      }
      for(int i = 0 ; i < 15; ++ i)
        ar & ms_curv[i];

      ar & area;
      ar & mfold;
    }
  };

  /// \brief A collection of landmarks with some other semantics
  struct lms_t : public std::vector<lm_t> {
    typedef std::vector<lm_t> base_t;

    void read(std::string fnA);
    void save(std::string fnA);
  };


  /**  \brief Read landmark point info from files  **/
  void read_lm_info(std::string fnA,std::string fnB);

  /**
  \brief build the set of correspondences between feature points of A and B

  \note a correspondence between a feature point in A and one
         in B is present only if the L infinity distance between the multiscale
         curvatures is less than a threshold.

  \note In case Tms is too small, the best K correspondences are picked, where
         K = minSizeTimes*min(|lmsA|,|lmsB|).

  \param[in]   Tms               Multi-scale curvature L-infinity threshold.

  **/
  void build_correspondences (double Tms,double minSizeTimes);


  /**
  \brief Build a list of potential correspondance sets such that the MRD score is
         less than a given threshold.

  \param[in]  TMRD              Maximum Relative Distance Threshold.
  \param[in]  Tafrac            Minimum Area fraction threshold
  **/
  int build_correspondence_sets (double TMRD);


  /** \brief Align A to B using only location of lms in each npt alignment.  **/
  void build_transforms();

  /** \brief Compute coarse alignment distances  **/
  void compute_alignment_distances_fast();

  /** \brief Compute precise alignment distances  **/
  void compute_alignment_distances_prec
    (vtkSmartPointer<vtkPolyData> pA, vtkSmartPointer<vtkPolyData> pB);




public:

  lms_t                          m_lmsA,m_lmsB;
  corrs_list_t                   m_corrs;
  std::vector<corrs_list_t>      m_corrs_sets;
  std::vector<vtkSmartPointer<vtkTransform> >
                                 m_corrs_sets_xfms;
  std::vector<double>            m_corrs_sets_xfms_dist;


};

/*****************************************************************************/



// /*---------------------------------------------------------------------------*/

// /**
//\brief Build a list of potential 3pt correspondance sets such that the dRMS
//       is less than a given threshold.

//\param[out] corrs_sets        List of correspondence sets.
//\param[in]  corrs             List of corresponence
//\param[in]  Afp_mesh_idx      Index of feature point of A in a vertex list.
//\param[in]  Bfp_mesh_idx      Index of feature point of B in a vertex list.
//\param[in]  pA                Surface patch A
//\param[in]  pB                Surface patch B
//\param[in]  Tdrms             drms threshold.
//**/

//void get_3pt_correspondances_drms
//(std::vector<corrs_list_t> &corrs_sets,
// const corrs_list_t & corrs,
// const std::vector<int> &Afp_mesh_idx,
// const std::vector<int> &Bfp_mesh_idx,
// vtkSmartPointer<vtkPolyData> pA,
// vtkSmartPointer<vtkPolyData> pB,
// double Tdrms);

// /*---------------------------------------------------------------------------*/

// /**
//\brief Build a list of potential 3pt correspondance sets such that the dRMS
//       is within a given fraction of all possible dRMSs

//\param[out] corrs_sets        List of correspondence sets.
//\param[in]  corrs             List of corresponence
//\param[in]  lmsA              Landmark info of A
//\param[in]  lmsB              Landmark info of B
//\param[in]  Fdrms             drms fraction.
//**/

//void get_3pt_correspondances_frac
//(std::vector<corrs_list_t> &corrs_sets,
// const corrs_list_t & corrs,
// const std::vector<lm_t> &lmsA,
// const std::vector<lm_t> &lmsB,
// double Fdrms);

/*---------------------------------------------------------------------------*/


// /**
//\brief Improve given transformation using ICP on asc/des mfolds of lm pts

//\note Alignment is from A to B.

//**/

//vtkSmartPointer<vtkTransform> do_icp
//(vtkSmartPointer<vtkDataSet> pA,
// vtkSmartPointer<vtkDataSet> pB,
// int Nicp,
// vtkSmartPointer<vtkTransform> tfmAtoB=vtkSmartPointer<vtkTransform>());




/*---------------------------------------------------------------------------*/

/**
\brief Compute the pervertex smallest L2 distance of points in A to B.

\param[in]  pA                Surface patch A
\param[in]  pB                Surface patch B

\returns The pervertex smallest squared L2 distance of points in B to A.

**/

vtkSmartPointer<vtkDoubleArray> compute_closest_point_dist
(vtkSmartPointer<vtkPolyData> pA,
 vtkSmartPointer<vtkPolyData> pB);

/*---------------------------------------------------------------------------*/

/**
\brief Get the rms of distances of points from a in A to its closest point b
in B and vise versa.
**/

double get_rms_distance
(vtkSmartPointer<vtkDataSet> ptsA,
 vtkSmartPointer<vtkDataSet> ptsB,
 vtkSmartPointer<vtkTransform> tfmAtoB=vtkSmartPointer<vtkTransform>());

/*---------------------------------------------------------------------------*/

/**
\brief Get the area fraction represnted by the given landmark points

\note The area fraction is the sum of areas of asc/des
      manifolds of aligned landmarks divided by area of all landmarks.

**/

template<int cind> double get_area_fraction
(const correspondence_builder_t::corrs_list_t &corrs,
 const std::vector<correspondence_builder_t::lm_t> &lms);

/*---------------------------------------------------------------------------*/

/** \brief Tag regions that are part of the corrs with the corrs num.       **/

template<int cind> vtkSmartPointer<vtkDoubleArray> get_corrs_region_tags
(vtkSmartPointer<vtkPolyData> pS,
 const std::vector<correspondence_builder_t::lm_t> & lms,
 const correspondence_builder_t::corrs_list_t & corrs);

/*---------------------------------------------------------------------------*/

/** \brief Tag the landmark points with their Morse  index (Regular is -1)  **/

void tag_landmark_points
(vtkSmartPointer<vtkPolyData> pS,
 const std::vector<correspondence_builder_t::lm_t> & lms);


/*****************************************************************************/




#include <vtkPolyData.h>

/*****************************************************************************/

/** \brief L infinity distance between two arbitary sized vectors **/

template<int N> inline double linf_dist(const double *a,const double *b)
{
  double ret = 0 ;

  for(int i = 0 ; i < N; ++i)
    ret = std::max(ret,std::abs(a[i]-b[i]));

  return ret;
}

/*---------------------------------------------------------------------------*/

/** \brief Square of L2 distance between two arbitary sized vectors **/

inline double l2_dist_sq(const std::vector<double> &a,
                         const std::vector<double> &b)
{
  double ret = 0 ;

  for(int i = 0 ; i < a.size(); ++i)
    ret += (a[i]-b[i])*(a[i]-b[i]);

  return ret;
}

/*---------------------------------------------------------------------------*/

/** \brief L2 distance between two points in a vtkPolydata's points **/

inline double l2_dist(vtkPolyData * pd , vtkIdType a, vtkIdType b)
{
  double Va[3] , Vb[3];

  pd->GetPoint(a,Va);
  pd->GetPoint(b,Vb);

  double d =
      (Va[0] - Vb[0])*(Va[0] - Vb[0]) +
      (Va[1] - Vb[1])*(Va[1] - Vb[1]) +
      (Va[2] - Vb[2])*(Va[2] - Vb[2]);

  return std::sqrt(d);
}

/*---------------------------------------------------------------------------*/

/** \brief Squared L2 distance between two points **/

inline double l2_dist_sq(const double *Va ,const double *Vb)
{
  return
      (Va[0] - Vb[0])*(Va[0] - Vb[0]) +
      (Va[1] - Vb[1])*(Va[1] - Vb[1]) +
      (Va[2] - Vb[2])*(Va[2] - Vb[2]);
}

/*****************************************************************************/
#endif // __FPALIGN_H_INCLUDED__
