#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <stack>
#include <set>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/HalfedgeDS_vector.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>

#include <utl.h>

struct MeshItems : public CGAL::Polyhedron_items_3
{
  template <class Refs, class Traits> struct Vertex_wrapper
  {
    typedef typename Traits::Point_3 P;
    typedef typename CGAL::Tag_true  T;

    struct Vertex:public CGAL::HalfedgeDS_vertex_base<Refs,T,P>
    {
      Vertex():CGAL::HalfedgeDS_vertex_base<Refs,T,P>(){}
      Vertex(const P &p):CGAL::HalfedgeDS_vertex_base<Refs,T,P>(p){}
      int index;
    };
  };

  template <class	Refs,	class	Traits>  struct Face_wrapper
  {
    typedef typename CGAL::Tag_true    T;

    struct	Face : public CGAL::HalfedgeDS_face_base<Refs, T>
    {
      Face():CGAL::HalfedgeDS_face_base<Refs, T>(){}
      int index;
    };
  };

};

typedef CGAL::Simple_cartesian<double>           kernel;
typedef kernel::Point_3                          Point;
typedef CGAL::Polyhedron_3
 <kernel,MeshItems,CGAL::HalfedgeDS_vector>      Mesh;

typedef Mesh::Vertex_handle                      Vertex_handle;
typedef Mesh::Halfedge_handle                    Halfedge_handle;
typedef Mesh::Halfedge_iterator                  Halfedge_iterator;
typedef Mesh::Facet_iterator                     Facet_iterator;
typedef Mesh::Vertex_iterator                    Vertex_iterator;
typedef Mesh::Halfedge_around_vertex_circulator  HV_circulator;
typedef Mesh::Halfedge_around_facet_circulator   HF_circulator;
typedef Mesh::HalfedgeDS                         HalfedgeDS;

using namespace std;


/*****************************************************************************/
/* usage                                                                     */

void print_usage(std::ostream &os)
{
  os<< "Usage: molpatch <mesh>.off  --plane a b c d |                  "<<endl;
  os<< "       --proximity pd full subset |--skin-simplex full subset  "<<endl;
  os<< "      [-o patch][ -r dil-shrink-radius][-sf small-frac]        "<<endl;
  os<< "                                                               "<<endl;
  os<< "---------------Brief description-------------------------------"<<endl;
  os<< "                                                               "<<endl;
  os<< "   A tool to extract patches from surface of a molecule using  "<<endl;
  os<< "   different methods                                           "<<endl;
  os<< "                                                               "<<endl;
  os<< "    <mesh>.off:input mesh file in .off format                  "<<endl;
  os<< "                                                               "<<endl;
  os<< "---------------Extraction Methods -----------------------------"<<endl;
  os<< "                                                               "<<endl;
  os<< " --plane <a> <b> <c> <d>: extract patch by slicing using plane "<<endl;
  os<< "            Points with  ax + by + cz + d  >= 0                "<<endl;
  os<< "            are extracted                                      "<<endl;
  os<< "                                                               "<<endl;
  os<< " --proximity <pd> <full>.pqr <subset>.[pdb|pqr]:               "<<endl;
  os<< "            extract patch using proximity to van-der-waals     "<<endl;
  os<< "            spheres.                                           "<<endl;
  os<< "                                                               "<<endl;
  os<< "            Points that are within r+<pd> L2 distance to       "<<endl;
  os<< "            any atom (r is the atomic radius) in the subset of "<<endl;
  os<< "            atoms of <full>.pqr extracted                      "<<endl;
  os<< "                                                               "<<endl;
  os<< "    Note a):<full>.pqr        :full molecule in pqr format     "<<endl;
  os<< "            <subset>.[pdb|pqr]:subset of <full> in pdb/pqr fmt."<<endl;
  os<< "                                                               "<<endl;
  os<< "    Note b):All atoms in <subset> are identified in <full> and "<<endl;
  os<< "            atoms of all residues containing atoms in <subset> "<<endl;
  os<< "            are considered as the subset of atoms of <full>.   "<<endl;
  os<< "            This is to accomodate additional atoms, such as H, "<<endl;
  os<< "            that may have been added to <full> and not <subset>"<<endl;
  os<< "                                                               "<<endl;
  os<< "    Note c):Patches extracted using proximity based method     "<<endl;
  os<< "            often has small holes and spurious components .    "<<endl;
  os<< "            This is can be repaired by dialation/shrink and    "<<endl;
  os<< "            small component elimination.                       "<<endl;
  os<< "            If no values are specified, dil-radius is set to   "<<endl;
  os<< "            maximum atomic radius in molecule and small-frac   "<<endl;
  os<< "            is set to 0.1 .                                    "<<endl;
  os<< "            See relevant option to modify/disable.             "<<endl;
  os<< "                                                               "<<endl;
  os<< " --skin-simplex <full>.pqr <subset>.[pdb|pqr]:                 "<<endl;
  os<< "            extract patch using the skin surface simplex tag.  "<<endl;
  os<< "                                                               "<<endl;
  os<< "            Extract points that are are formed by any simplex  "<<endl;
  os<< "            in the mixed complex of the skin surface containing"<<endl;
  os<< "            an atom from subset of <full> in its vertex set    "<<endl;
  os<< "                                                               "<<endl;
  os<< "     Note a): See Note a) above                                "<<endl;
  os<< "     Note b): See Note b) above                                "<<endl;
  os<< "     Note c): <mesh>.off is assumed to have 4 additional int   "<<endl;
  os<< "              fields in its point information. This specifies  "<<endl;
  os<< "              the mixed complex simplex. For non tetrahedron   "<<endl;
  os<< "              simplices, -1 is entered for extra fields.       "<<endl;
  os<< "                                                               "<<endl;
  os<< "---------------Other Options ----------------------------------"<<endl;
  os<< "                                                               "<<endl;
  os<< " -o <patch>.off:output (repaired) mesh file name               "<<endl;
  os<< "                        (default <mesh>_patch.off)             "<<endl;
  os<< "                                                               "<<endl;
  os<< " -df <decimate-frac>: Target reduction desired by Quadric mesh "<<endl;
  os<< "                 decimation (default 0: no decimation )        "<<endl;
  os<< "                                                               "<<endl;
  os<< " -sf <small-frac>: Components with less than <small-frac> of   "<<endl;
  os<< "                 vertices will be removed (default 0)          "<<endl;
  os<< "                 0 indicates that no components to be removed  "<<endl;
  os<< " -r <dil-shrink-radius>: Radius to use in dialation and        "<<endl;
  os<< "                         shrink process (default 0)            "<<endl;
  os<< "                         0 indicates no dil/shrink             "<<endl;
  os<< "                                                               "<<endl;
  os<< "---------------------------------------------------------------"<<endl;
//  os<< " -df <decimate-frac>: Target reduction desired by Quadric mesh "<<endl;
//  os<< "                 decimation (default 0: no decimation )        "<<endl;
//  os<< "                                                               "<<endl;
}

const int IN       = 1;
const int INOROUT  = 0;
const int OUT      = -1;


inline bool is_tri_in(const vector<char> &vflgs, Halfedge_handle he)
{
  Halfedge_handle hec = he;

  int u = he->vertex()->index; he = he->next();
  int v = he->vertex()->index; he = he->next();
  int w = he->vertex()->index; he = he->next();

  assert(hec == he);

  return vflgs[u] == IN && vflgs[v] == IN && vflgs[w] == IN;
}


inline bool is_border_edge(const vector<char> &vflgs, Halfedge_handle he)
{
  if(he->is_border_edge())
    return true;

  bool a_in = is_tri_in(vflgs,he);
  bool b_in = is_tri_in(vflgs,he->opposite());

  return a_in != b_in;
}

inline void print_tri(const vector<char> &vflgs, Halfedge_handle he,std::ostream &os = cout)
{
  Halfedge_handle hec = he;

  int u = he->vertex()->index; he = he->next();
  int v = he->vertex()->index; he = he->next();
  int w = he->vertex()->index; he = he->next();

  os <<"(";
  os << u <<":"<< int(vflgs[u])<<",";
  os << v <<":"<< int(vflgs[v])<<",";
  os << w <<":"<< int(vflgs[w])<<",";
  os << ")";

  os.flush();

  assert(hec == he);
}

void extract_mesh_subset
(const std::vector<char> &vflgs,
 Mesh & mesh,
 std::vector<double> &verts,
 std::vector<int> &tris)
{

  int nv = mesh.vertices_end() - mesh.vertices_begin();
  int nt = mesh.facets_end() - mesh.facets_begin();

  std::vector<char> tflgs(nt,OUT);

  assert(IN != OUT);

  for(Vertex_iterator vit = mesh.vertices_begin(); vit != mesh.vertices_end(); ++vit)
  {
    HV_circulator hc  = vit->vertex_begin();
    HV_circulator hce = vit->vertex_begin();

    CGAL_For_all_backwards(hce,hc)
      if (is_border_edge(vflgs,hc))
        break;

    hce = hc;

    int nflips = 0;

    CGAL_For_all(hc,hce)
    {
      if(is_border_edge(vflgs,hc) )
        nflips++;

      if(is_tri_in(vflgs,hc))
        tflgs[hc->facet()->index] = IN;
    }

    assert(nflips%2 == 0);

    if (nflips <= 2 )
      continue;

    cerr << "Repairing non manifold vert .. idx = " << vit->index << endl;

    assert(is_border_edge(vflgs,hc));

    std::list< std::list<Halfedge_handle> > bnd_tris;

    std::list< std::list<Halfedge_handle> >::iterator ll_it;
    int llsize = 0;

    CGAL_For_all(hc,hce)
    {
      if(is_border_edge(vflgs,hc) )
        bnd_tris.push_back(std::list<Halfedge_handle>());

      if(!is_tri_in(vflgs,hc))
      {
        bnd_tris.back().push_back(hc);

        if(llsize < bnd_tris.back().size() )
        {
          ll_it = --bnd_tris.end();
          llsize = bnd_tris.back().size();
        }
      }
    }

    bnd_tris.erase(ll_it);

    for(std::list< std::list<Halfedge_handle> >::iterator it=bnd_tris.begin();
        it != bnd_tris.end(); ++it)
      for(std::list<Halfedge_handle>::iterator jt = it->begin();
          jt != it->end(); ++jt)
        tflgs[(*jt)->facet()->index] = IN;
  }

  int nvo=0,nto =0 ;

  std::vector<int> svidx(nv,OUT),stidx(nt,OUT);

  for(Facet_iterator fit = mesh.facets_begin(); fit != mesh.facets_end();++fit)
  {
    assert(fit->is_triangle());

    if(tflgs[fit->index] == OUT)
      continue;

    Halfedge_handle he = fit->halfedge();

    for(int i = 0 ; i < 3; ++ i)
    {
      int u = he->vertex()->index;

      if(svidx[u] == OUT)
        svidx[u] = nvo++;

      he = he->next();
    }

    stidx[fit->index] = nto++;
  }

  verts.resize(nvo*3);

  for(Vertex_iterator vit = mesh.vertices_begin(); vit != mesh.vertices_end(); ++vit)
  {
    int j = svidx[vit->index];

    if( j == OUT)
      continue;

    Point pt = vit->point();

    verts[3*j +0] = pt.x();
    verts[3*j +1] = pt.y();
    verts[3*j +2] = pt.z();
  }

  tris.resize(nto*3);

  for(Facet_iterator fit = mesh.facets_begin(); fit != mesh.facets_end();++fit)
  {
    int j = stidx[fit->index];

    if(j == OUT)
      continue;

    Halfedge_handle he = fit->halfedge();

    tris[3*j + 0] = svidx[he->vertex()->index]; he = he->next();
    tris[3*j + 1] = svidx[he->vertex()->index]; he = he->next();
    tris[3*j + 2] = svidx[he->vertex()->index]; he = he->next();
  }
}


/*****************************************************************************/
/* Subroutine to get vertices within an r neighborhood of a vertex           */

void get_rrad_verts(Vertex_handle  start_hVertex,
                    std::vector<Vertex_handle> &v_rrad,
                    double radius)
{
  typedef typename std::map<int,Vertex_handle> seen_verts_t;

  std::stack<Vertex_handle>     front_verts;
  seen_verts_t                  seen_verts;

  Point st_pt = start_hVertex->point();

  front_verts.push(start_hVertex);

  while(front_verts.size() != 0)
  {
    Vertex_handle hVertex = front_verts.top();
    front_verts.pop();

    HV_circulator hHalfedge = hVertex->vertex_begin();
    HV_circulator begin = hHalfedge;

    CGAL_For_all(hHalfedge,begin)
    {
      Vertex_handle hAdjVertex  = hHalfedge->opposite()->vertex();

      if((st_pt - hAdjVertex->point()).squared_length() > radius*radius)
        continue;

      if(seen_verts.count(hAdjVertex->index) != 0)
        continue;

      seen_verts[hAdjVertex->index] = hAdjVertex;

      front_verts.push(hAdjVertex);
    }
  }

  for(seen_verts_t::iterator it = seen_verts.begin();
      it!=seen_verts.end(); ++it)
    v_rrad.push_back(it->second);
}

/*****************************************************************************/
/* Subroutine to build the mesh from vert and tri lists                      */

template <class HDS>
struct Mesh_Builder : public CGAL::Modifier_base<HDS>
{

  const std::vector<double> & verts;
  const std::vector<int>    & tris;

  Mesh_Builder(const std::vector<double> & v,const std::vector<int> & t)
    :verts(v),tris(t){}
  void operator()( HDS& hds)
  {
    typedef typename HDS::Vertex   Vertex;
    typedef typename Vertex::Point Point;

    CGAL::Polyhedron_incremental_builder_3<HDS> B( hds, true);

    B.begin_surface( verts.size()/3, tris.size()/3);

    for(int i = 0 ; i < verts.size()/3; ++i)
      B.add_vertex( Point( verts[3*i], verts[3*i+1], verts[3*i+2]))->index=i;

    for(int i = 0 ; i < tris.size()/3; i++)
      B.add_facet(tris.data()+i*3,tris.data()+i*3+3)->facet()->index = i;

    B.end_surface();
  }
};

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkIdTypeArray.h>
#include <vtkQuadricDecimation.h>
#include <vtkDataObject.h>
#include <vtkAlgorithmOutput.h>

void decimate_and_remove_small_comps
(const std::vector<double> &verts,
 const std::vector<int> &tris,
 double deci_frac,
 double small_frac,
 std::vector<double> &verts_o,
 std::vector<int> &tris_o)
{
  vtkSmartPointer<vtkPolyData>    pd  = vtkSmartPointer<vtkPolyData>::New();
  vtkSmartPointer<vtkPoints>     pts  = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();

  for(int i = 0 ;i < verts.size()/3; ++i)
    pts->InsertNextPoint(&verts[3*i]);

  for(int i = 0 ; i < tris.size(); i+=3)
  {
    vtkIdType tri[]= {tris[i],tris[i+1],tris[i+2]};
    cells->InsertNextCell(3,tri);

    for(int j = 0 ; j < 3; ++j)
    {
      ENSUREV3(0 <= tri[j] &&
               tri[j] < verts.size()/3,"input ind out of range", i,j,tri[j]);
    }

  }

  pd->SetPoints(pts);
  pd->SetPolys(cells);
  pd->Update();

  vtkSmartPointer<vtkAlgorithmOutput> prev_output = pd->GetProducerPort();

  std::vector<vtkSmartPointer<vtkPolyDataAlgorithm> > filters;

  {
    vtkSmartPointer<vtkCleanPolyData> cpdf =
        vtkSmartPointer<vtkCleanPolyData>::New();

    cpdf->SetInputConnection(prev_output);
    cpdf->Update();

    prev_output = cpdf->GetOutputPort();

    filters.push_back(cpdf);
  }

  if( deci_frac > 0 && deci_frac < 1)
  {
    vtkSmartPointer<vtkQuadricDecimation> deci =
        vtkSmartPointer<vtkQuadricDecimation>::New();

    deci->SetInputConnection(prev_output);
    deci->SetTargetReduction(deci_frac);
    deci->Update();

    filters.push_back(deci);

    prev_output = deci->GetOutputPort();
  }

  if(small_frac > 0 && small_frac < 1)
  {
    int npts = vtkPolyData::SafeDownCast
        (prev_output->GetProducer()->GetOutputDataObject(0))
        ->GetNumberOfPoints();

    int nsmall = std::floor(small_frac*npts);

    vtkSmartPointer<vtkPolyDataConnectivityFilter> cf =
        vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();

    cf->SetInputConnection(prev_output);
    cf->SetExtractionModeToAllRegions();
    cf->InitializeSpecifiedRegionList();
    cf->Update();

    vtkSmartPointer<vtkIdTypeArray> rs = cf->GetRegionSizes();

    for(int i = 0 ; i < rs->GetNumberOfTuples(); ++i)
      if(rs->GetValue(i) > nsmall)
        cf->AddSpecifiedRegion(i);

    cf->SetExtractionModeToSpecifiedRegions();
    cf->Update();

    prev_output = cf->GetOutputPort();

    filters.push_back(cf);

  }

  {
    vtkSmartPointer<vtkCleanPolyData> cpdf =
        vtkSmartPointer<vtkCleanPolyData>::New();

    cpdf->SetInputConnection(prev_output);
    cpdf->Update();

    prev_output = cpdf->GetOutputPort();
    filters.push_back(cpdf);
  }

  vtkSmartPointer<vtkPolyData> pdo = vtkSmartPointer<vtkPolyData>::New();

  pdo->DeepCopy(vtkPolyDataAlgorithm::SafeDownCast
                (prev_output->GetProducer())->GetOutput());

  verts_o.resize(pdo->GetNumberOfPoints()*3);
  tris_o.resize(pdo->GetNumberOfCells()*3);

  for(int i = 0 ; i < pdo->GetNumberOfPoints(); ++i)
    pdo->GetPoint(i,&verts_o[3*i]);

  for(int i = 0 ; i < pdo->GetNumberOfCells(); ++i)
  {
    vtkCell * cell = pdo->GetCell(i);
    assert(cell->GetNumberOfPoints()==3);

    for(int j = 0 ; j < 3; ++j)
    {
      ENSUREV3(0 <= cell->GetPointId(j) &&
             cell->GetPointId(j) < pdo->GetNumberOfPoints(),
               "ind out of range",i,j,cell->GetPointId(j));
      tris_o[3*i+j] = cell->GetPointId(j);
    }
  }

  filters.clear();
}

void check_tri_inds(const std::vector<int> & tris,int n)
{
  cout << "Checking tris "  << endl;

  for(int i = 0 ; i < tris.size(); ++i)
  {
    ENSUREV3(0 <= tris[i] && tris[i] < n,"triIdx out of range" ,i/3,i%3,tris[i]);
  }
}


/*****************************************************************************/
/* main                                                                      */

int main(int argc, char **argv)
{
  
  /*=========================================================================*/
  /*----------- 1. Parse Arguments-------------------------------------------*/

  std::string i_file;
  std::string f_file;
  std::string p_file;
  std::string opoff_file;

  // extract patch by slicing using a plane
  double pln[4]  = {0,0,0,0};
  bool   use_pln = false;

  // extract patch by proximity of mesh verts to van-Der-Walls balls of ppdb
  double prox_dist = 0;
  bool   use_pd    = false;

  // extract patch using skin surface simplices
  bool   use_ss    = false;

  // dialation radius
  bool        use_dr     = false;
  double      dil_radius = 0;

  // clip small components
  double      small_frac = 0;
  bool        use_sf      = false;

  // decimate small components
  double      deci_frac = 0;
  bool        use_df    = false;


  {
    if (argc == 1)
    {
      print_usage(cout);
      return 0;
    }

    std::vector<std::string> args(argv+1,argv+argc);

    utl::extract_opt_value("-o",opoff_file,args);
    use_dr  = utl::extract_opt_value("-r",dil_radius,args);
    use_sf  = utl::extract_opt_value("-sf",small_frac,args);
    use_df  = utl::extract_opt_value("-df",deci_frac,args);
    use_pln = utl::extract_opt_value("--plane",pln[0],pln[1],pln[2],pln[3],args);
    use_pd  = utl::extract_opt_value("--proximity",prox_dist,f_file,p_file,args);
    use_ss  = utl::extract_opt_value("--skin-simplex",f_file,p_file,args);

    if( args.size() != 1)
    {
      cerr<<"Incorrect arguments !!! "<<endl;
      print_usage(cerr);
      return 1;
    }

    i_file = args[0];

    if(opoff_file.size() == 0)
      opoff_file = i_file.substr(0,i_file.size()-4) + "_patch.off";
  }
  
  
  /*-------------------------------------------------------------------------*/
  /*=========================================================================*/



  /*=========================================================================*/
  /*----------- 2. Read meshes and molecules --------------------------------*/
  
  std::vector<char>   vertex_tag;
  Mesh                mesh;

  {
    std::vector<double> verts;
    std::vector<int>    tris;

    if( use_pln)
    {
      utl::read_off(i_file,verts,tris);

      vertex_tag.resize(verts.size()/3);

      for(int i = 0 ; i < vertex_tag.size(); ++i)
        vertex_tag[i] = ((pln[0]*verts[3*i+0] +
                          pln[1]*verts[3*i+1] +
                          pln[2]*verts[3*i+2] +
                          pln[3]) >=0)?(IN):(OUT);

      if(!use_sf)
      {
        cout<<"Warning !!! Discarding small components is recommended when \n"\
              "cutting molsurfaces using planes \n"\
              "Setting small-frac to 0.1 "<<endl;
        use_sf = true;
        small_frac = 0.1;
      }
    }
    else if (use_pd)
    {
      std::vector<int>    subset;
      std::vector<double> at_pos;
      std::vector<double> at_rad;

      utl::read_off(i_file,verts,tris);

      vertex_tag.resize(verts.size()/3,OUT);

      utl::get_subset_atoms(f_file,p_file,subset);

      utl::read_protein(f_file,at_pos,at_rad);

      for(int i = 0 ; i < vertex_tag.size(); ++i)
      {
        for(int j = 0 ; j < subset.size(); ++j)
        {
          double dx = at_pos[subset[j]*3+0] - verts[i*3+0];
          double dy = at_pos[subset[j]*3+1] - verts[i*3+1];
          double dz = at_pos[subset[j]*3+2] - verts[i*3+2];

          double dist2 = dx*dx + dy*dy + dz*dz;

          double atDist = at_rad[subset[j]]  + prox_dist;

          if (dist2 < atDist*atDist )
          {
            vertex_tag[i] = IN;
            break;
          }
        }
      }

      if(!use_dr)
      {
        cout<<"Warning !!! Repair with dialation is recommended for \n"\
              "proximity based patch extraction. \n"\
              "Setting dil-radius to maximum atomic radius in molecule"<<endl;
        use_dr = true;
        dil_radius = *max_element(at_rad.begin(),at_rad.end());
      }

      if(!use_sf)
      {
        cout<<"Warning !!! Discarding small components is recommended for \n"\
              "proximity based patch extraction. \n"\
              "Setting small-frac to 0.1 "<<endl;
        use_sf = true;
        small_frac = 0.1;
      }
    }
    else if(use_ss)
    {
      std::vector<int>    vSimp;
      std::set<int>       subset;

      utl::read_off<int,4>(i_file,verts,tris,vSimp);
      utl::get_subset_atoms(f_file,p_file,subset);

      vertex_tag.resize(verts.size()/3);

      for(int i = 0 ; i < vertex_tag.size(); ++i)
        vertex_tag[i] =
            (subset.count(vSimp[i*4 + 0]) || subset.count(vSimp[i*4 + 1])||
             subset.count(vSimp[i*4 + 2]) || subset.count(vSimp[i*4 + 3]))?
              (IN):(OUT);
    }
    else
    {
      cout << "Full mesh selected .. only repairs will be perfomed" << endl;

      ENSURE(!use_dr,"Cannot dialate with full mesh");
      ENSURE(use_sf || use_df,"No modification operation selected");

      utl::read_off(i_file,verts,tris);

      vertex_tag.resize(verts.size()/3,IN);
    }

    Mesh_Builder<HalfedgeDS> builder(verts,tris);
    mesh.delegate(builder);
  }
  
  /*-------------------------------------------------------------------------*/
  /*=========================================================================*/




  /*=========================================================================*/
  /*------------- 3. Do Dialation and Shrinkage -----------------------------*/

  if(dil_radius > 0)
  {

    clog << "Repairing with Dialation/Shrinkage Radius=" << dil_radius << endl;

    /*----------- 3.a. Perform dialation ------------------------------------*/

    std::vector<Vertex_handle> new_dil_verts;

    for(Vertex_iterator vit = mesh.vertices_begin();
        vit != mesh.vertices_end(); ++vit)
    {
      if(vertex_tag[vit->index] == IN)
      {
        std::vector<Vertex_handle> v_rrad;

        get_rrad_verts(vit,v_rrad,dil_radius);

        for(int j = 0 ;  j < v_rrad.size(); ++j)
          if(vertex_tag[v_rrad[j]->index] == OUT)
          {
            new_dil_verts.push_back(v_rrad[j]);
            vertex_tag[v_rrad[j]->index] = INOROUT;
          }
      }
    }

    for(int i = 0 ; i < new_dil_verts.size(); ++i)
      vertex_tag[new_dil_verts[i]->index] = IN;

    /*----------- 3.b. Perform Shrinkage ------------------------------------*/

    std::vector<Vertex_handle> rejected_dil_verts;

    for(int i = 0 ; i < new_dil_verts.size(); ++i)
    {
      std::vector<Vertex_handle> v_rrad;

      get_rrad_verts(new_dil_verts[i],v_rrad,dil_radius);

      for(int j = 0 ; j < v_rrad.size(); ++j)
        if(vertex_tag[v_rrad[j]->index] == OUT)
          {rejected_dil_verts.push_back(new_dil_verts[i]);break;}
    }

    for(int i = 0 ; i < rejected_dil_verts.size(); ++i)
      vertex_tag[rejected_dil_verts[i]->index] = OUT;
  }
  
  /*-------------------------------------------------------------------------*/
  /*=========================================================================*/


  /*=========================================================================*/
  /*----------- 4. Extract mesh subset --------------------------------------*/

  std::vector<double> verts;
  std::vector<int>    tris;

  extract_mesh_subset(vertex_tag,mesh,verts,tris);

  check_tri_inds(tris,verts.size()/3);

  if(use_sf||deci_frac)
  {
    clog << "Expunging components with less than " << small_frac*100.0
         << "% of the vertices "
         << endl;

    decimate_and_remove_small_comps(verts,tris,deci_frac,small_frac,verts,tris);

    check_tri_inds(tris,verts.size()/3);
  }
  /*-------------------------------------------------------------------------*/
  /*=========================================================================*/


 /*=========================================================================*/
  /*----------- 6. Save patch -----------------------------------------------*/


  utl::write_off(opoff_file,verts,tris);
  
  /*-------------------------------------------------------------------------*/
  /*=========================================================================*/

}

