#ifndef __UTL_H_INCLUDED__
#define __UTL_H_INCLUDED__

#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <set>


#include <boost/iterator/iterator_facade.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>


namespace utl
{

/*===========================================================================*/

/**
  \brief Reads a triangle mesh in OFF format

  \param[in]   fname filename of the off file
  \param[out]  verts list of verts as x1,y1,z1,x2,y2,z2 ... xn,yn,zn
  \param[out]  tris  list of triangles as u1,v1,w1,u2,v2,w2 ... un,vn,wn

  **/

void read_off(const std::string &fname,
              std::vector<double> &verts,
              std::vector<int> &tris);

/*---------------------------------------------------------------------------*/

/**
  \brief Reads a triangle mesh with a scalar in OFF format

  \tparam[in]  T       type of the scalar
  \tparam[in]  compno  column number of the scalar in the file (0 based).

  \param[in]   fname   filename of the off file
  \param[out]  verts   list of verts as x1,y1,z1,x2,y2,z2 ... xn,yn,zn
  \param[out]  tris    list of triangles as u1,v1,w1,u2,v2,w2 ... un,vn,wn
  \param[out]  scalar  list of scalar values for each vert

  **/

template <typename T,int compno>
void read_off(const std::string &fname,
              std::vector<double> &verts,
              std::vector<int> &tris,
              std::vector<T> &scalar);

/*---------------------------------------------------------------------------*/

/**
  \brief Writes a triangle mesh in OFF format

  \param[in]  fname  filename of the off file
  \param[in]  verts  list of verts as x1,y1,z1,x2,y2,z2 ... xn,yn,zn
  \param[in]  tris   list of triangles as u1,v1,w1,u2,v2,w2 ... un,vn,wn

  **/

void write_off(const std::string &fname,
              const std::vector<double> &verts,
              const std::vector<int> &tris);

/*---------------------------------------------------------------------------*/

/**
  \brief Writes a triangle mesh with a scalar in OFF format

  \tparam[in]  T     type of the scalar

  \param[in]  fname  filename of the off file
  \param[in]  verts  list of verts as x1,y1,z1,x2,y2,z2 ... xn,yn,zn
  \param[in]  tris   list of triangles as u1,v1,w1,u2,v2,w2 ... un,vn,wn
  \param[in]  scalar list of scalar values for each vert

  **/

template <typename T>
void write_off(const std::string &fname,
              const std::vector<double> &verts,
              const std::vector<int> &tris,
              const std::vector<T> &scalar);


/*---------------------------------------------------------------------------*/

/**
  \brief Extracts a sub mesh from a triangle mesh represented as lists of
         verts and tris.

  \note  A flag variable list is used that indicates whether a
         vertex is in or out.

  \note  The given vert list, and tri list is modified in
         place.

  \note  The flag variable is also modified to remove isolated vertices
         (vertices with no triangle incident on it).

  \tparam[in]  in    value of the flag variable when vertex is to be included
  \tparam[in]  out   value of the flag variable when vertex is to be excluded

  \param[in,out]  vflgs    the per vertex flag
  \param[in,out]  verts    list of verts as x1,y1,z1,x2,y2,z2 ... xn,yn,zn
  \param[in,out]  tris     list of triangles as u1,v1,w1,u2,v2,w2 ... un,vn,wn
  \param[out]     stof_map the index of each sub-mesh vertex in the full mesh.


  **/

template <char in, char out>
void extract_mesh_subset
( std::vector<char> &vflgs,
  std::vector<double> &verts,
  std::vector<int> &tris,
  std::vector<int> &stof_map);

/*---------------------------------------------------------------------------*/

/**
  \brief Given a full pdb/pqr and subset pdb/pqr, get the indices of subset
         atoms in the full pdb/pqr

  \param[in]  ffn           filename of the full pdb/pqr
  \param[in]  sfn           filename of the subset pdb/pqr
  \param[out] subset_atoms  output set of subset atom indices
  **/

void get_subset_atoms
(std::string ffn,
 std::string sfn,
 std::set<int> &subset_atoms);

/*---------------------------------------------------------------------------*/

/** \brief variant of get_subset_atoms()                                    **/
inline void get_subset_atoms
(std::string ffn,
 std::string sfn,
 std::vector<int> &subset_atoms)
{
  std::set<int> subset;
  get_subset_atoms(ffn,sfn,subset);
  subset_atoms.resize(subset.size());
  std::copy(subset.begin(),subset.end(),subset_atoms.begin());
}

/*---------------------------------------------------------------------------*/

/**
  \brief Read coordinates and radii from pqr

  \param[in]  fn       Filename of the pdb/pqr
  \param[out] coords   Array of coordinates .. x1,y1,z1,x2,y2,z2,...,xn,yn,zn.
  \param[out] radius   Array of radii       ..    r1   ,  r2    ,...,   rn.
  **/


void read_protein
(std::string fn,
 std::vector<double> &coords,
 std::vector<double> &radius);


/*---------------------------------------------------------------------------*/

/**
  \brief Checks and removes an option from a list of arguments

  \returns true if option was found , false if not.

  \param[in]      opt   option to check for
  \param[in,out]  args  list of arguments

  \note  the first occurance of opt is removed from args.
  **/

bool extract_opt(std::string opt,std::vector<std::string> &args);

/*---------------------------------------------------------------------------*/

/**
  \brief Checks and extracts an option and its value from list of arguments

  \returns true if option was found , false if not.

  \tparam         T     the option's value type

  \param[in]      opt   option to check for
  \param[out]     val   value of the option given
  \param[in,out]  args  list of arguments

  \note  the first occurance of opt and its corressponding value is removed
         from args.
  \note  This uses the from_string method (below) to convert from string
         representation to T type.

  **/
template <class T> bool extract_opt_value
  (std::string opt, T &val,std::vector<std::string> &args);

/*---------------------------------------------------------------------------*/

/**
  \brief Checks and extracts an option and 2 values from list of arguments

  \returns true if option was found , false if not.

  \tparam         T1     the option's first value type
  \tparam         T2     the option's second value type

  \param[in]      opt   option to check for
  \param[out]     val1  first value of the option given
  \param[out]     val2  second value of the option given
  \param[in,out]  args  list of arguments

  \note  the first occurance of opt and its corressponding value is removed
         from args.
  \note  This uses the from_string method (below) to convert from string
         representation to T type.

  **/
template <class T1,class T2> bool extract_opt_value
  (std::string opt, T1 &val1,T2 &val2,std::vector<std::string> &args);


/*---------------------------------------------------------------------------*/

/**
  \brief Checks and extracts an option and 3 values from list of arguments

  \returns true if option was found , false if not.

  \tparam         T1     the option's first value type
  \tparam         T2     the option's second value type
  \tparam         T3     the option's third value type

  \param[in]      opt   option to check for
  \param[out]     val1  first value of the option given
  \param[out]     val2  second value of the option given
  \param[out]     val3  third value of the option given
  \param[in,out]  args  list of arguments

  \note  the first occurance of opt and its corressponding value is removed
         from args.
  \note  This uses the from_string method (below) to convert from string
         representation to T type.

  **/
template <class T1,class T2,class T3> bool extract_opt_value
  (std::string opt, T1 &val1,T2 &val2,T3 &val3,std::vector<std::string> &args);

/*---------------------------------------------------------------------------*/

/**
  \brief Checks and extracts an option and 4 values from list of arguments

  \returns true if option was found , false if not.

  \tparam         T1     the option's first value type
  \tparam         T2     the option's second value type
  \tparam         T3     the option's third value type
  \tparam         T4     the option's third value type


  \param[in]      opt   option to check for
  \param[out]     val1  first value of the option given
  \param[out]     val2  second value of the option given
  \param[out]     val3  third value of the option given
  \param[out]     val4  fourth value of the option given
  \param[in,out]  args  list of arguments

  \note  the first occurance of opt and its corressponding value is removed
         from args.
  \note  This uses the from_string method (below) to convert from string
         representation to T type.

  **/
template <class T1,class T2,class T3,class T4> bool extract_opt_value
  (std::string opt, T1 &val1,T2 &val2,T3 &val3,T4 &val4,std::vector<std::string> &args);

/*---------------------------------------------------------------------------*/

/**
  \brief Converts a given string to the template type

  \tparam    T  the value type
  \param[in] s  Input string

  \returns converted value from string representation.

  \note Template specailizations of only basic types are implemented.
**/

template <class T> T from_string(std::string s);

/*---------------------------------------------------------------------------*/

/**
  \brief Converts a given string to the template types

  \tparam    T1 value type
  \tparam    T2 value type
  \tparam    T3 value type
  \tparam    T4 value type

  \param[in] s  Input string

  \note will work only if type has >> operator implemented.
**/

template <typename T1,typename T2,typename T3,typename T4>
void from_string(std::string s,T1& t1,T2& t2,T3& t3,T4& t4);

/*---------------------------------------------------------------------------*/

/**
  \brief Converts a given string to the template types

  \tparam    T1 value type
  \tparam    T2 value type
  \tparam    T3 value type

  \param[in] s  Input string

  \note will work only if type has >> operator implemented.
**/


template <typename T1,typename T2,typename T3>
void from_string(std::string s,T1& t1,T2& t2,T3& t3);

/*---------------------------------------------------------------------------*/

/**
  \brief Converts a given string to the template types

  \tparam    T1 value type
  \tparam    T2 value type

  \param[in] s  Input string

  \note will work only if type has >> operator implemented.
**/


template <typename T1,typename T2>
void from_string(std::string s,T1& t1,T2& t2);

/*---------------------------------------------------------------------------*/

/**
  \brief Encodes given data into base 64 and saves it in the given stream

  \tparam     T  the value type of the vector
  \param[out] os Output stream
  \param[in]  c  Data pointer
  \param[in]  N  num data bytes

**/

void write_b64(std::ostream &os, const unsigned char * c, int N);

/*---------------------------------------------------------------------------*/

/**
  \brief Encodes a given vector into base 64 and saves it in the given stream

  \tparam     T  the value type of the vector
  \param[out] os Output stream
  \param[in]  v  Data vector

**/

template <typename T>
inline void write_b64 (std::ostream &os, const std::vector<T>& v)
{write_b64(os,(unsigned char*)(void*)v.data(),v.size()*sizeof(T));}

/*---------------------------------------------------------------------------*/

/**
  \brief Encodes a given set into base 64 and saves it in the given stream

  \tparam     T  the value type of the vector
  \param[out] os Output stream
  \param[in]  v  Data vector

**/

template <typename T>
inline void write_b64 (std::ostream &os, const std::set<T>& s)
{std::vector<T> v(s.begin(),s.end());write_b64(os,v);}

/*---------------------------------------------------------------------------*/

/**
  \brief Decodes the given base 64 stream into the given data buffer

  \tparam     T  the value type of the vector
  \param[in]  is Input stream
  \param[out] c  Data pointer
  \param[in]  N  num data bytes

**/

void read_b64(std::istream &is, unsigned char * c, int N);

/*---------------------------------------------------------------------------*/

/**
  \brief Decodes the given base 64 stream into the given vector

  \tparam     T  the value type of the vector
  \param[in]  is Input stream
  \param[out] v  Data vector

**/

template <typename T>
inline void read_b64 (std::istream &is, std::vector<T>& v)
{read_b64(is,(unsigned char*)(void*)v.data(),v.size()*sizeof(T));}

/*---------------------------------------------------------------------------*/

/**
  \brief A generic to string impl that uses the << operator of type T

  \tparam     T  the value type of the vector
  \param[in]  v  Value

  \returns string
**/

template <typename T>
inline std::string to_string (const T& v)
{std::stringstream ss; ss << v ; return ss.str();}

/*---------------------------------------------------------------------------*/

template <class iter_t>
void argsort(iter_t b, iter_t e, std::vector<size_t>& idxs);

/*---------------------------------------------------------------------------*/

template <class iter_t>
void rearrange(iter_t b, iter_t e,const std::vector<size_t>& idxs);

/*---------------------------------------------------------------------------*/

/// \brief strip front and back whitespaces in given string
void trim(std::string &s);

/*---------------------------------------------------------------------------*/
/**
  \brief A class to iterate over non-comment lines of a file

  \note All parts of a line that succeed the comment char ('#' default) are
  assumed to be a comment and are stripped

  \note Sample usage
  for(file_line_iterator lgen("file.txt"),lend; lgen != lend; )
    cout << *lgen++;

**/

class file_line_iterator
    : public boost::iterator_facade<
    file_line_iterator
    , std::string const
    , boost::forward_traversal_tag
    >
{
public:
  file_line_iterator(const char * f,char c_char='#');
  file_line_iterator(){}

private:
  friend class boost::iterator_core_access;
  void increment();
  bool equal(file_line_iterator const& other) const;
  const std::string &dereference() const;

private:
  boost::shared_ptr<std::ifstream> is;
  std::string                      value;
  char                             c_char;
};

/*---------------------------------------------------------------------------*/

/** \brief Simple stopwatch timer (to measure wall clock time NOT cpu time) **/
class timer
{
 public:
  timer()
  {restart();}

  inline void   restart()
  { _start_time = boost::posix_time::microsec_clock::local_time(); }

  inline double elapsed() const
  {
    boost::posix_time::time_duration td =
        boost::posix_time::microsec_clock::local_time() - _start_time;

    return double(td.total_milliseconds())/1000;
  }

 private:
  boost::posix_time::ptime _start_time;
}; // timer

/*---------------------------------------------------------------------------*/

/** \brief A simple multi-threaded logger                                   **/
class logger
{
 public:

  enum severity_level {trace,debug,info,warning,error,fatal};

  inline bool isOpen(severity_level severity)
  {return severity >= info;}

  inline void push(const std::string & log)
  {
    std::string tstr = boost::posix_time::to_simple_string
        (boost::posix_time::second_clock::local_time());

    boost::mutex::scoped_lock  lock(s_mutex);

    std::clog << " [" <<tstr <<"]"
              << " [" <<boost::this_thread::get_id()<<"]"
              << std::endl
              << log
              << std::endl;
  }

  static inline logger& get() {return s_logger;}

private:

  static boost::mutex s_mutex;
  static logger       s_logger;
}; // logger

namespace detail
{
/** \brief a small extension of std::pair where only first is initialized   **/
template<class _T1, class _T2> struct pair:public std::pair<_T1,_T2>
{pair(const _T1 &t1){std::pair<_T1,_T2>::first = t1;}};
}//namespace detail

/*===========================================================================*/
}// namespace utl


/*===========================================================================*/
/* Some useful macros                                                        */

#define LOG_MACRO(lev)  \
  if(utl::logger::get().isOpen(utl::logger::lev))\
  for(utl::detail::pair<bool,std::stringstream> __utl_lm_v__(true); \
      __utl_lm_v__.first ;__utl_lm_v__.first=false,\
  utl::logger::get().push(__utl_lm_v__.second.str())) __utl_lm_v__.second

#define STRMVAR(VAR) #VAR << " = "<< (VAR)

#ifndef ASSERT
#ifndef NDEBUG
#define ASSERT(cond) if (!(cond))\
{ std::stringstream ss; \
  ss << "Runtime error at (" << __FILE__ <<  "," << __func__ << "," \
     << __LINE__<<") \n " << #cond ; throw std::runtime_error(ss.str());}
#else  //ifndef NDEBUG
#define ASSERT(cond)
#endif // ifndef NDEBUG
#endif // ifndef ASSERT


#define ENSURE(cond,mesg) if (!(cond))\
{ std::stringstream ss; \
  ss<<"Failed to ensure condition " << #cond <<"\n" \
    <<"at ("<<__FILE__<<","<<__func__<<","<<__LINE__<<") \n "\
    <<"Message : " << #mesg << "\n"\
  ;throw std::runtime_error(ss.str());}

#define ENSUREV(cond,mesg,var1) if (!(cond))\
{ std::stringstream ss; \
  ss<<"Failed to ensure condition " << #cond <<"\n" \
    <<"at ("<<__FILE__<<","<<__func__<<","<<__LINE__<<") \n "\
    <<"Message : " << #mesg << "\n"\
    << #var1 <<" = "<< (var1) << "\n"\
  ;throw std::runtime_error(ss.str());}

#define ENSUREV2(cond,mesg,var1,var2) if (!(cond))\
{ std::stringstream ss; \
  ss<<"Failed to ensure condition " << #cond <<"\n" \
    <<"at ("<<__FILE__<<","<<__func__<<","<<__LINE__<<") \n "\
    <<"Message : " << #mesg << "\n"\
    << #var1 <<" = "<< (var1) << "\n"\
    << #var2 <<" = "<< (var2) << "\n";\
  ;throw std::runtime_error(ss.str());}

#define ENSUREV3(cond,mesg,var1,var2,var3) if (!(cond))\
{ std::stringstream ss; \
  ss<<"Failed to ensure condition " << #cond <<"\n" \
    <<"at ("<<__FILE__<<","<<__func__<<","<<__LINE__<<") \n "\
    <<"Message : " << #mesg << "\n"\
    << #var1 <<" = "<< (var1) << "\n"\
    << #var2 <<" = "<< (var2) << "\n"\
    << #var3 <<" = "<< (var3) << "\n";\
  ;throw std::runtime_error(ss.str());}

#define ENSURES(cond) \
  if(!(cond)) \
  for(std::stringstream ss ; true ; throw std::runtime_error(ss.str())) \
  ss<<"Failed to ensure condition " << #cond <<"\n" \
    <<"at ("<<__FILE__<<","<<__func__<<","<<__LINE__<<") \n "


/*===========================================================================*/



/*===========================================================================*/
/* Template Implementations                                                  */
#include "utl_inl.h"
/*===========================================================================*/

#endif //__UTL_H_INCLUDED__
