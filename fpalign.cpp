#include <fstream>
#include <sstream>
#include <limits>
#include <cmath>
#include <set>
#include <list>
#include <iterator>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkLandmarkTransform.h>
#include <vtkIterativeClosestPointTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCellLocator.h>
#include <vtkDoubleArray.h>
#include <vtkTriangle.h>
#include <vtkTransform.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTransformFilter.h>
#include <vtkPointData.h>

#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>


#include "fpalign.h"
#include "utl.h"

using namespace std;

namespace br = boost::range;
namespace badpt = boost::adaptors;

const int num_curvature_scales = 15;

typedef correspondence_builder_t::lm_t         lm_t;
typedef correspondence_builder_t::corrs_t      corrs_t;
typedef correspondence_builder_t::corrs_set_t  corrs_set_t;
typedef correspondence_builder_t::corrs_list_t corrs_list_t;

int get_corrs_first(const corrs_t &p) {return p.first;}
int get_corrs_second(const corrs_t &p) {return p.second;}


/*****************************************************************************/

void read_lm_info
(std::string fname, std::vector<correspondence_builder_t::lm_t> &lms)
{
  std::ifstream fi(fname.c_str(),ios::in);

  ENSUREV(fi.is_open(),"cant open file ",fname);

  {std::string s;getline(fi,s);}

  int Nlm;

  fi >> Nlm;

  lms.resize(Nlm);

  for(int i = 0 ; i < Nlm; ++i)
  {
    int Npts;

    fi >> lms[i].index
       >> lms[i].vertid
       >> Npts
       >> lms[i].pos[0]
       >> lms[i].pos[1]
       >> lms[i].pos[2]
       >> lms[i].area
//       >> lms[i].posUncert
//       >> lms[i].upos[0]
//       >> lms[i].upos[1]
//       >> lms[i].upos[2]
       >> lms[i].normal[0]
       >> lms[i].normal[1]
       >> lms[i].normal[2];




//    cout << lms[i].index << "  "
//         << lms[i].vertid<< "  "
//         << Npts<< "  "
//         << lms[i].pos[0]<< "  "
//         << lms[i].pos[1]<< "  "
//         << lms[i].pos[2]<< "  "
//         << lms[i].posUncert<< "  "
//         << lms[i].area << endl;

    lms[i].mfold.resize(Npts);
  }

  for(int i = 0; i < Nlm; ++i)
  {
    for(int j = 0; j < num_curvature_scales; ++j)
    {
      fi >> lms[i].ms_curv[j];
    }
  }

  for(int i = 0 ; i < Nlm; ++i)
    utl::read_b64(fi,lms[i].mfold);

  ENSUREV(fi.eof() == false,"read past end of file !!",fname);
}

/*---------------------------------------------------------------------------*/

namespace boost { namespace serialization { template<class Archive>
inline void serialize(Archive & ar,correspondence_builder_t::lms_t & t,const unsigned int file_version
){ar & boost::serialization::base_object<correspondence_builder_t::lms_t::base_t>(t);}}}


/*---------------------------------------------------------------------------*/
void correspondence_builder_t::lms_t::read(std::string fn)
{
  std::ifstream fs(fn.c_str());
  boost::archive::binary_iarchive ia(fs);
  ia >> *this;
}

/*---------------------------------------------------------------------------*/
void correspondence_builder_t::lms_t::save(std::string fn)
{
  std::ofstream fs(fn.c_str());
  boost::archive::binary_oarchive oa(fs);
  oa << *this;
}

/*---------------------------------------------------------------------------*/

void correspondence_builder_t::read_lm_info(string fnA, string fnB)
{
  m_lmsA.read(fnA);
  m_lmsB.read(fnB);
}

/*---------------------------------------------------------------------------*/

void correspondence_builder_t::build_correspondences(double Tms, double minSizeTimes)
{
  std::vector<std::pair<double,corrs_t> > dists(m_lmsA.size() * m_lmsB.size());


  for(int i = 0,k=0 ; i < m_lmsA.size(); ++i)
    for(int j = 0 ; j < m_lmsB.size(); ++j,++k)
    {
      double d = linf_dist<15>(m_lmsA[i].ms_curv,m_lmsB[j].ms_curv);
      dists[k] = make_pair(d,corrs_t(i,j));
    }


  br::sort(dists);

  size_t ncorrs = std::distance(boost::begin(dists|badpt::map_keys),
                                br::upper_bound(dists|badpt::map_keys,Tms));

  ncorrs = max<size_t>(ncorrs,ceil(minSizeTimes*min(m_lmsA.size(),m_lmsB.size())));

  m_corrs.resize(ncorrs);

  br::copy(dists|badpt::map_values|badpt::sliced(0,ncorrs),m_corrs.begin());
}


/*---------------------------------------------------------------------------*/

// /** \brief A class to aid maintaining unique sets of 3pt corresspondances   **/
//struct corrs3pt_t
//{
//  int p,q,r,u,v,w;
//  double dRMS;

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  corrs3pt_t(int p,int q,int u,int v,double dRMS)
//    :p(p),q(q),r(-1),u(u),v(v),w(-1),dRMS(dRMS)
//  {order();}

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  corrs3pt_t(int p,int q,int r, int u,int v,int w,double dRMS)
//    :p(p),q(q),r(r),u(u),v(v),w(w),dRMS(dRMS)
//  {order();}

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  corrs3pt_t()
//    :p(-1),q(-1),r(-1),u(-1),v(-1),w(-1),dRMS(-1)
//  {}

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  static bool dRMS_cmp (const corrs3pt_t &a,const corrs3pt_t &b)
//  {return a.dRMS < b.dRMS;}

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  bool operator <(const corrs3pt_t & o) const
//  {
//    int a[] = {p,u,q,v,r,w,o.p,o.u,o.q,o.v,o.r,o.w};
//    return lexicographical_compare(a,a+6,a+6,a+12);
//  }

//  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

//  void order()
//  {
//    if (p < q) {swap(p,q); swap(u,v);}
//    if (p < r) {swap(p,r); swap(u,w);}
//    if (q < r) {swap(q,r); swap(v,w);}
//  }
//};

/*---------------------------------------------------------------------------*/

//void get_3pt_correspondances_drms
//( std::vector<corrs_list_t> &corrs,
// const graph_t & graph,
// const vector<int> &Afp_mesh_idx,
// const vector<int> &Bfp_mesh_idx,
// vtkSmartPointer<vtkPolyData> pA,
// vtkSmartPointer<vtkPolyData> pB,
// double Tdrms)
//{

//  ENSUREV(Tdrms > 0 ,"Invalid Argument",Tdrms);

//  std::vector<corrs3pt_t> twopt_corrs;
  
//  int num_Afp = Afp_mesh_idx.size();

//  for(int i = 0 ; i < num_Afp; ++i)
//    for(int j = i+1 ; j < num_Afp; ++j)
//    {
//      int p = i,q = j;

//      double pq = l2_dist(pA,Afp_mesh_idx[p],Afp_mesh_idx[q]);

//      for(int k = 0 ; k < graph[i].size(); ++k)
//        for(int l = 0 ; l < graph[j].size(); ++l)
//        {
//          int u = graph[i][k],v = graph[j][l];

//          if(u == v)
//            continue;

//          double uv = l2_dist(pB,Bfp_mesh_idx[u],Bfp_mesh_idx[v]);

//          // dRMS is (i=1..n j=1..n |pi-pj| - |qi-qj|)/(n^2)
//          double dRMS = abs(pq-uv)/2.0;

//          if(dRMS < Tdrms)
//            twopt_corrs.push_back(corrs3pt_t(p,q,u,v,dRMS));
//        }
//    }

//  std::set<corrs3pt_t> threept_corrs;
  
//  for(int i  = 0 ; i < twopt_corrs.size(); i++)
//  {
//    int p = twopt_corrs[i].p;
//    int q = twopt_corrs[i].q;
//    int u = twopt_corrs[i].u;
//    int v = twopt_corrs[i].v;

//    double pq_uv = twopt_corrs[i].dRMS * 2.0;

//    for(int j = 0 ; j < num_Afp; ++j)
//    {
//      if(j == p || j == q)
//        continue;

//      int r = j;

//      double pr = l2_dist(pA,Afp_mesh_idx[p],Afp_mesh_idx[r]);
//      double qr = l2_dist(pA,Afp_mesh_idx[q],Afp_mesh_idx[r]);

//      for(int k = 0 ; k < graph[j].size(); ++k)
//      {
//        int w = graph[j][k];

//        if(w == u || w == v)
//          continue;

//        double uw = l2_dist(pB,Bfp_mesh_idx[u],Bfp_mesh_idx[w]);
//        double vw = l2_dist(pB,Bfp_mesh_idx[v],Bfp_mesh_idx[w]);


//        // dRMS is (i=1..n j=1..n |pi-pj| - |qi-qj|)/(n^2)
//        double dRMS = (pq_uv + abs(pr-uw) + abs(qr-vw))*2.0/9.0;

//        if(dRMS < Tdrms)
//          threept_corrs.insert(corrs3pt_t(p,q,r,u,v,w,dRMS));
//      }
//    }
//  }

//  corrs.resize(threept_corrs.size(),corrs_list_t(3));

//  int i = 0;

//  for(set<corrs3pt_t>::iterator it = threept_corrs.begin();
//      it != threept_corrs.end(); ++it,++i)
//  {
//    corrs[i][0] = corrs_t(it->p,it->u);
//    corrs[i][1] = corrs_t(it->q,it->v);
//    corrs[i][2] = corrs_t(it->r,it->w);
//  }
//}

/*---------------------------------------------------------------------------*/

//void get_3pt_correspondances_frac
//( std::vector<corrs_list_t> &corrs,
// const graph_t & graph,
// const std::vector<lm_t> &lmsA,
// const std::vector<lm_t> &lmsB,
// double Fdrms)
//{
//  ENSUREV(0 < Fdrms && Fdrms< 1,"Invalid Argument",Fdrms);

//  std::vector<corrs3pt_t> twopt_corrs;

//  int num_Afp = graph.size();

//  for(int i = 0 ; i < num_Afp; ++i)
//    for(int j = i+1 ; j < num_Afp; ++j)
//    {
//      int p = i,q = j;

//      double pq = std::sqrt(l2_dist_sq(lmsA[p].ms_curv,lmsA[q].ms_curv));

//      for(int k = 0 ; k < graph[i].size(); ++k)
//        for(int l = 0 ; l < graph[j].size(); ++l)
//        {
//          int u = graph[i][k],v = graph[j][l];

//          if(u == v)
//            continue;

//          double uv = std::sqrt(l2_dist_sq(lmsB[u].ms_curv,lmsB[v].ms_curv));

//          // dRMS is (i=1..n j=1..n |pi-pj| - |qi-qj|)/(n^2)
//          double dRMS = abs(pq-uv)/2.0;

//          twopt_corrs.push_back(corrs3pt_t(p,q,u,v,dRMS));
//        }
//    }

//  std::sort(twopt_corrs.begin(),twopt_corrs.end(),corrs3pt_t::dRMS_cmp);

//  twopt_corrs.resize(size_t(std::ceil(double(twopt_corrs.size())*Fdrms)));

//  std::set<corrs3pt_t> threept_corrs;

//  for(int i  = 0 ; i < twopt_corrs.size(); i++)
//  {
//    int p = twopt_corrs[i].p;
//    int q = twopt_corrs[i].q;
//    int u = twopt_corrs[i].u;
//    int v = twopt_corrs[i].v;

//    double pq_uv = twopt_corrs[i].dRMS * 2.0;

//    for(int j = 0 ; j < num_Afp; ++j)
//    {
//      if(j == p || j == q)
//        continue;

//      int r = j;

//      double pr = std::sqrt(l2_dist_sq(lmsA[p].ms_curv,lmsA[r].ms_curv));
//      double qr = std::sqrt(l2_dist_sq(lmsA[q].ms_curv,lmsA[r].ms_curv));

//      for(int k = 0 ; k < graph[j].size(); ++k)
//      {
//        int w = graph[j][k];

//        if(w == u || w == v)
//          continue;

//        double uw = std::sqrt(l2_dist_sq(lmsB[u].ms_curv,lmsB[w].ms_curv));
//        double vw = std::sqrt(l2_dist_sq(lmsB[v].ms_curv,lmsB[w].ms_curv));

//        double dRMS = (pq_uv + abs(pr-uw) + abs(qr-vw))*2.0/9.0;

//        threept_corrs.insert(corrs3pt_t(p,q,r,u,v,w,dRMS));
//      }
//    }
//  }

//  std::vector<corrs3pt_t> clist(threept_corrs.begin(),threept_corrs.end());

//  std::sort(clist.begin(),clist.end(),corrs3pt_t::dRMS_cmp);

//  clist.resize(size_t(std::ceil(double(clist.size())*Fdrms)));

//  corrs.resize(clist.size(),corrs_list_t(3));

//  for(int i = 0 ; i < clist.size() ; ++i)
//  {
//    corrs[i][0] = (corrs_t(clist[i].p,clist[i].u));
//    corrs[i][1] = (corrs_t(clist[i].q,clist[i].v));
//    corrs[i][2] = (corrs_t(clist[i].r,clist[i].w));
//  }
//}

/*---------------------------------------------------------------------------*/

/** \brief helper functor for get_npt_correspondances_mrd                   **/
struct corrs_enumerator_t
{
  /** \brief Ctor                                                           **/
  corrs_enumerator_t
  ( const corrs_list_t &corrs,
    const std::vector<lm_t> &lmsA,
    const std::vector<lm_t> &lmsB):
    edge_list(corrs),lmsA(lmsA),lmsB(lmsB),areaA(0),areaB(0)
  {
    for(int i = 0 ; i < lmsA.size(); ++i) areaA += lmsA[i].area;
    for(int i = 0 ; i < lmsB.size(); ++i) areaB += lmsB[i].area;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief functor for metric info                                        **/
  struct metric_info_t
  {
    const vector<lm_t> &lms;

    metric_info_t(const vector<lm_t> &lms):lms(lms){}

    double distance(int a, int b)
    {return std::sqrt(l2_dist_sq(lms[a].pos,lms[b].pos));}

    double normals_inner_prod(int a, int b)
    {return l2_dist_sq(lms[a].normal,lms[b].normal);}
  };


  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief Build the corrs adjacency graph using an MRD threshold         **/
  int build_corrs_adj(double TMRD,double Tang)
  {
    corrs_adj.resize(edge_list.size());

    metric_info_t amet(lmsA),bmet(lmsB);

    int ne = 0;

    for(int i =  0 ; i < edge_list.size(); ++i)
      for(int j =  i+1 ; j < edge_list.size(); ++j)
      {
        int p = edge_list[i].first;
        int u = edge_list[i].second;

        int q = edge_list[j].first;
        int v = edge_list[j].second;

        if(p == q || u == v)
          continue;

        double mrd = abs(amet.distance(p,q) -
                         bmet.distance(u,v));

        if ( mrd >= TMRD )
        {
//          cout<<"MRD REJECT:"
//             << "(" << p << "," << q <<"),"
//             << "(" << u << "," << v <<")" << endl;
          continue;
        }

//        ENSURE(0<TMRD && TMRD < 1, "Using a relative distance");

//        double d1 = amet.distance(p,q);
//        double d2 = bmet.distance(u,v);

//        if (d1 > d2) std::swap(d1,d2);

//        if( (1 - (d1/d2) ) >= TMRD)
//          continue;


        double a1 = std::acos(amet.normals_inner_prod(p,q));
        double a2 = std::acos(bmet.normals_inner_prod(u,v));

        double ndiff = abs(a1-a2);

//        const double M_PI = 3.14159265359;

//        if(ndiff >= Tang)
//        {
////          cout<<"ANG REJECT:"
////             << "(" << p << "," << q <<"),"
////             << "(" << u << "," << v <<")" << endl;

//          continue;
//        }

        corrs_adj[i].insert(j);
        corrs_adj[j].insert(i);

//        cout<<"ACCEPT:"
//           << "(" << p << "," << q <<"),"
//           << "(" << u << "," << v <<")" << endl;

        ++ne;
      }

//    for(int i = 0 ; i < corrs_adj.size(); ++i )
//    {
//      BOOST_FOREACH(int j ,  corrs_adj[i])
//      {
//        cout << "(" << edge_list[i].first << "," << edge_list[j].first << ")"
//             << " <----> "
//             << "(" << edge_list[i].second << "," << edge_list[j].second << ")"
//             << endl;
//      }
//    }

    return ne;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief Build the corrs adjacency graph using lm pos uncertainity      **/
  template <class adist_t,class bdist_t>
  int build_corrs_adj_uncert
  (adist_t   adist,  bdist_t   bdist)
  {
    corrs_adj.resize(edge_list.size());

    int ne = 0;

    for(int i =  0 ; i < edge_list.size(); ++i)
      for(int j =  i ; j < edge_list.size(); ++j)
      {
        int p = edge_list[i].first;
        int u = edge_list[i].second;

        int q = edge_list[j].first;
        int v = edge_list[j].second;

        if(p == q || u == v)
          continue;

        double mrd = abs(adist(p,q) - bdist(u,v));

        double ua = adist(p)+adist(q);
        double ub = bdist(u)+bdist(v);

        if ( mrd >= min(ua,ub))
          continue;

        corrs_adj[i].insert(j);
        corrs_adj[j].insert(i);

        ++ne;
      }

    return ne;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief Greedy Pivot variant of the Bron-Kerbosch maximal clique       *
    *        enumeration algorithm                                          **/
  template<class iftor_t>
  void bk_gp(std::set<int> &R,std::set<int> &P,std::set<int> &X,iftor_t iftor)
  {
    if(P.size() == 0 && X.size() == 0)
    {
      iftor(R);
    }
    else
    {
      int max_deg = -1;
      int pivot   = -1;

      for(std::set<int>::iterator it = P.begin(); it != P.end(); ++it)
      {
        if(int(corrs_adj[*it].size()) > max_deg)
        {
          max_deg = corrs_adj[*it].size();
          pivot   = *it;
        }
      }

      for(std::set<int>::iterator it = X.begin(); it != X.end(); ++it)
      {
        if(int(corrs_adj[*it].size()) > max_deg)
        {
          max_deg = corrs_adj[*it].size();
          pivot   = *it;
        }
      }

      for(std::set<int>::iterator it = P.begin(); it != P.end();)
      {
        int u = *it++;

        if(corrs_adj[pivot].count(u))
          continue;

        P.erase(u);

        std::set<int> Rnew,Pnew,Xnew;

        Rnew.insert(R.begin(),R.end());
        Rnew.insert(u);

        br::set_intersection(P,corrs_adj[u],inserter(Pnew,Pnew.begin()));
        br::set_intersection(X,corrs_adj[u],inserter(Xnew,Xnew.begin()));

        bk_gp(Rnew,Pnew,Xnew,iftor);

        X.insert(u);
      }
    }
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief Evaluates and retains corrs set if relevant                    **/
  void add_corrs(const std::set<int> &R,int minSize,double minAreaFrac)
  {
    if(R.size() < minSize)
      return;

    double aA = 0,aB = 0;

    BOOST_FOREACH(int i, R)
    {
      aA += lmsA[edge_list[i].first].area;
      aB += lmsB[edge_list[i].second].area;
    }

    aA /= areaA;
    aB /= areaB;

    if(max(aA,aB) < minAreaFrac)
      return;

    ENSURE(corrs_sets.size() < 0x00ffffff,"Out of Mem");

    corrs_sets.resize(corrs_sets.size() + 1);
    corrs_sets.back().resize(R.size());
    br::copy(R,corrs_sets.back().begin());

  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  /** \brief Does the the corrs building                                    **/
  void enumerate(int minSize,double minAfrac)
  {
    std::set<int> R,P,X;

    for(int i = 0 ; i < edge_list.size(); ++i)
      P.insert(i);

    bk_gp(R,P,X,bind(&corrs_enumerator_t::add_corrs,this,_1,minSize,minAfrac));
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/  

  /** \brief Does the the corrs building                                    **/
  void save_gml(std::string fn)
  {
    ofstream os(fn.c_str());
    ENSUREV(os.is_open(),"Cant open file !!",fn);

    os << "graph [" <<endl;
    os << "directed 0" <<endl;

    for(int i = 0 ; i < corrs_adj.size(); ++ i)
      os << " node [ id " << i << " ] " << endl;


    for(int i = 0 ; i < corrs_adj.size(); ++ i)
      for(std::set<int>::iterator it = corrs_adj[i].begin(); it != corrs_adj[i].end(); ++it)
        os <<" edge [ source "<< i << " target " << *it <<" ]" << endl;

    os << "]" << endl;
  }

  /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

  inline const std::list<std::vector<int> > & get_corrs_sets()
  {return corrs_sets;}


private:

  // Input data
  const corrs_list_t  &edge_list;  // The list of Correspondences
  const std::vector<lm_t>  &lmsA,&lmsB; // Landmark point information

  // Internal data
  vector<set<int> >       corrs_adj;    // Graph with nodes as corrs edges
  double                areaA,areaB;    // Total area of both

  // Output data
  std::list<std::vector<int> >    corrs_sets; // set of maximal corrs in corrs_adj graph

};

/*---------------------------------------------------------------------------*/

int correspondence_builder_t::build_correspondence_sets (double TMRD)
{
  corrs_enumerator_t en(m_corrs,m_lmsA,m_lmsB);

  int ne = en.build_corrs_adj(TMRD,M_PI/2),i=0;

  LOG_MACRO(trace) << " Corrs graph built " << endl;

  en.enumerate(6,0.15);

  LOG_MACRO(trace) << " Max cliques Enumerated " << endl;

  m_corrs_sets.resize(en.get_corrs_sets().size());

  BOOST_FOREACH(const std::vector<int> & cset,en.get_corrs_sets())
  {
    m_corrs_sets[i].resize(cset.size());

    for(int j = 0 ; j < cset.size(); ++j)
      m_corrs_sets[i][j] = m_corrs[cset[j]];

    i++;
  }

//  builder.save_gml("test.gml");

  return ne;
}

/*---------------------------------------------------------------------------*/

void correspondence_builder_t::build_transforms()
{
  m_corrs_sets_xfms.resize(m_corrs_sets.size());

  for(int cNum = 0 ; cNum < m_corrs_sets.size(); ++cNum)
  {
    corrs_list_t & corrs = m_corrs_sets[cNum];

    vtkSmartPointer<vtkTransform> transform =
        vtkSmartPointer<vtkTransform>::New();

    vtkSmartPointer<vtkPoints> lmPtsA =vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkPoints> lmPtsB =vtkSmartPointer<vtkPoints>::New();

    for(int i=0; i< corrs.size() ;i++)
    {
      lmPtsA->InsertNextPoint(m_lmsA[corrs[i].first].pos);
      lmPtsB->InsertNextPoint(m_lmsB[corrs[i].second].pos);
    }

    vtkSmartPointer<vtkLandmarkTransform> landmarkTransform =
            vtkSmartPointer<vtkLandmarkTransform>::New();
    landmarkTransform->SetSourceLandmarks(lmPtsA);
    landmarkTransform->SetTargetLandmarks(lmPtsB);
    landmarkTransform->SetModeToRigidBody();
    landmarkTransform->Update();

    transform->SetMatrix(landmarkTransform->GetMatrix());

    m_corrs_sets_xfms[cNum] = transform;
  }
}

/*---------------------------------------------------------------------------*/

//vtkSmartPointer<vtkTransform> do_icp
//(vtkSmartPointer<vtkDataSet> pA,
// vtkSmartPointer<vtkDataSet> pB,
// int Nicp,
// vtkSmartPointer<vtkTransform> tfmAtoB)
//{
//  if(tfmAtoB)
//  {
//    vtkSmartPointer<vtkTransformFilter> tfmFilter;
//    tfmFilter = vtkSmartPointer<vtkTransformFilter>::New();
//    tfmFilter->SetInput(pA);
//    tfmFilter->SetTransform(tfmAtoB);
//    tfmFilter->Update();

//    pA = vtkDataSet::SafeDownCast(tfmFilter->GetOutputDataObject(0));
//  }

//  vtkSmartPointer<vtkIterativeClosestPointTransform> icp =
//    vtkSmartPointer<vtkIterativeClosestPointTransform>::New();
//  icp->SetSource(pA);
//  icp->SetTarget(pB);

//  icp->SetMaximumNumberOfLandmarks(pA->GetNumberOfPoints());
//  icp->GetLandmarkTransform()->SetModeToRigidBody();
//  icp->GetLandmarkTransform()->Update();
//  icp->CheckMeanDistanceOn();
//  icp->SetMaximumMeanDistance(0.1);

//  icp->SetMaximumNumberOfIterations(Nicp);
//  icp->Modified();
//  icp->Update();

// //  clog << " Num Icp iters = " << icp->GetNumberOfIterations() << endl;

//  vtkSmartPointer<vtkTransform> transform =
//      vtkSmartPointer<vtkTransform>::New();

//  transform->SetMatrix(icp->GetMatrix());

//  if(tfmAtoB)
//  {
//    transform->Concatenate(tfmAtoB);
//    transform->Update();
//  }

//  return transform;
//}

/*---------------------------------------------------------------------------*/

template<typename ftor_t>
vtkSmartPointer<vtkUnstructuredGrid> __get_corrs_surface
(const corrs_list_t &corrs,
 const std::vector<lm_t> &lms,
 vtkSmartPointer<vtkPolyData> pd,
 vtkSmartPointer<vtkTransform> tfm,
 ftor_t get_corrs)
{
  vtkSmartPointer<vtkUnstructuredGrid> usg=
      vtkSmartPointer<vtkUnstructuredGrid>::New();

  vtkSmartPointer<vtkPoints>    pts  =vtkSmartPointer<vtkPoints>::New();
  usg->SetPoints(pts);

  for(int i=0; i< corrs.size() ;i++)
  {
    int a = get_corrs(corrs[i]);

    for(int j=0; j< lms[a].mfold.size() ;j++)
    {
      double pt[3];
      pd->GetPoint(lms[a].mfold[j],pt);
      vtkIdType id = usg->GetPoints()->InsertNextPoint(pt);
      usg->InsertNextCell(VTK_VERTEX,1,&id);
    }
  }  

  if(tfm)
  {
    vtkSmartPointer<vtkTransformFilter> tfmFilter;
    tfmFilter = vtkSmartPointer<vtkTransformFilter>::New();
    tfmFilter->SetInputData(usg);
    tfmFilter->SetTransform(tfm);
    tfmFilter->Update();

    usg->DeepCopy(tfmFilter->GetOutputDataObject(0));
  }

  return usg;
}

template<int CNUM> vtkSmartPointer<vtkUnstructuredGrid> get_corrs_surface
(const corrs_list_t &corrs, const std::vector<lm_t> &lms,
 vtkSmartPointer<vtkPolyData> pd,
 vtkSmartPointer<vtkTransform> tfm=vtkSmartPointer<vtkTransform>())
{return __get_corrs_surface(corrs,lms,pd,tfm,get_corrs_first);}


template<> vtkSmartPointer<vtkUnstructuredGrid> get_corrs_surface<0>
(const corrs_list_t &corrs, const std::vector<lm_t> &lms,
 vtkSmartPointer<vtkPolyData> pd, vtkSmartPointer<vtkTransform> tfm)
{return __get_corrs_surface(corrs,lms,pd,tfm,get_corrs_first);}

template<> vtkSmartPointer<vtkUnstructuredGrid> get_corrs_surface<1>
(const corrs_list_t &corrs, const std::vector<lm_t> &lms,
 vtkSmartPointer<vtkPolyData> pd, vtkSmartPointer<vtkTransform> tfm)
{return __get_corrs_surface(corrs,lms,pd,tfm,get_corrs_second);}

/*---------------------------------------------------------------------------*/

/**
\note The alignment distance is the area weighted average distance
      b/w the the landmarks.

                         [     Sum(i=1..n) A(pi) d(pi,qi)^2     ]
                    sqrt | ------------------------------------ |
                         [     Sum(i=1..n) A(pi)                ]
      D'(C,P,Q) =--------------------------------------------------
                              Sum(i=1..n) Afrac(pi)

       D(C,P,Q) = min {D'(C,P,Q), D'(C,Q,P)}

**/

double get_alignment_distance_fast
(const corrs_list_t &corrs,
 const std::vector<lm_t> &lmsA,
 const std::vector<lm_t> &lmsB,
 vtkSmartPointer<vtkTransform> transform)
{
  double AreaA = 0, AreaB = 0;

  for(int i = 0 ; i < lmsA.size(); ++i)
    AreaA += lmsA[i].area;

  for(int i = 0 ; i < lmsB.size(); ++i)
    AreaB += lmsB[i].area;

  double errAB = 0, errBA = 0;
  double PareaA = 0, PareaB = 0;

  std::vector<double> apos(corrs.size()*3);

  for(int i = 0 ; i < corrs.size(); ++i)
    for(int j=0; j < 3; ++j)
      apos[i*3 + j] = lmsA[corrs[i].first].pos[j];

  if(transform)
    for(int i = 0 ; i < corrs.size(); i++)
      transform->TransformPoint(&apos[i*3],&apos[i*3]);

  for(int i = 0 ; i < corrs.size(); ++i)
  {
    int a = corrs[i].first;
    int b = corrs[i].second;
    double d = vtkMath::Distance2BetweenPoints(&apos[i*3],lmsB[b].pos);
//    d = std::sqrt(d);

    errAB += d*lmsA[a].area;
    errBA += d*lmsB[b].area;

    PareaA += lmsA[a].area;
    PareaB += lmsB[b].area;
  }

//  errAB = std::sqrt(errAB/PareaA);
//  errBA = std::sqrt(errBA/PareaB);

//  errAB /= (PareaA/AreaA);
//  errBA /= (PareaB/AreaB);

//  return min(errAB,errBA);

// coarse RMS estimate
  return std::sqrt((errAB + errBA)/(PareaA + PareaB));

// coarse averge volume gap between surfaces after alignment
//  return (errAB/PareaA + errBA/PareaB)/2;
}

void correspondence_builder_t::compute_alignment_distances_fast()
{
  m_corrs_sets_xfms_dist.resize(m_corrs_sets.size());

  for(int i = 0 ; i < m_corrs_sets_xfms.size(); ++i)
  {
    m_corrs_sets_xfms_dist[i] =
        get_alignment_distance_fast
        (m_corrs_sets[i],m_lmsA,m_lmsB,m_corrs_sets_xfms[i]);
  }
}

/*---------------------------------------------------------------------------*/

vtkSmartPointer<vtkDoubleArray> compute_closest_point_dist
(vtkSmartPointer<vtkPolyData> pA,
 vtkSmartPointer<vtkPolyData> pB)
{
  vtkSmartPointer<vtkCellLocator> locB= vtkSmartPointer<vtkCellLocator>::New();

  locB->SetDataSet(pB);
  locB->SetNumberOfCellsPerBucket(1);
  locB->BuildLocator();

  vtkSmartPointer<vtkDoubleArray> pAdistB =
      vtkSmartPointer<vtkDoubleArray>::New();
  pAdistB->SetName("DistaceToOther");
  pAdistB->SetNumberOfComponents(1);
  pAdistB->SetNumberOfTuples(pA->GetNumberOfPoints());

  for(int i = 0; i < pA->GetNumberOfPoints(); i++)
  {
    vtkIdType cell_id;
    int sub_id;
    double dist2;
    double outPoint[3];

    locB->FindClosestPoint(pA->GetPoint(i),outPoint,cell_id,sub_id,dist2);
    pAdistB->SetValue(i,std::sqrt(dist2));
  }

  return pAdistB;
}
/*---------------------------------------------------------------------------*/

template<bool is_rms>
std::pair<double,double> get_sum_distances
(vtkSmartPointer<vtkDataSet> ptsA,
 vtkSmartPointer<vtkDataSet> ptsB,
 vtkSmartPointer<vtkTransform> tfmAtoB)
{
  double sds[2] = {0,0};  

  vtkSmartPointer<vtkTransformFilter> tfmFilter;

  if (tfmAtoB)
  {
    tfmFilter = vtkSmartPointer<vtkTransformFilter>::New();
    tfmFilter->SetInputData(ptsA);
    tfmFilter->SetTransform(tfmAtoB);
    tfmFilter->Update();

    ptsA = vtkDataSet::SafeDownCast(tfmFilter->GetOutputDataObject(0));
  }

  for(int i = 0 ; i < 2; ++i)
  {
    vtkSmartPointer<vtkCellLocator> loc=vtkSmartPointer<vtkCellLocator>::New();

    loc->SetDataSet(ptsB);
    loc->SetNumberOfCellsPerBucket(1);
    loc->BuildLocator();

    for(int j = 0; j < ptsA->GetNumberOfPoints(); j++)
    {
      vtkIdType cell_id;
      int sub_id;
      double dist2;      
      double outPoint[3];
      loc->FindClosestPoint(ptsA->GetPoint(j),outPoint,cell_id,sub_id,dist2);
      if(!is_rms) dist2 = std::sqrt(dist2);
      sds[i] += dist2;
    }

    std::swap(ptsA,ptsB);
  }

  return make_pair(sds[0],sds[1]);
}

/*---------------------------------------------------------------------------*/

double get_rms_distance
(vtkSmartPointer<vtkDataSet> ptsA,
 vtkSmartPointer<vtkDataSet> ptsB,
 vtkSmartPointer<vtkTransform> tfmAtoB)
{
  std::pair<double,double> sds = get_sum_distances<true>(ptsA,ptsB,tfmAtoB);
  double N =ptsA->GetNumberOfPoints() + ptsB->GetNumberOfPoints();
  return sqrt((sds.first + sds.second )/ N);
}

/*---------------------------------------------------------------------------*/

template<typename getpitem_t> double __get_area_fraction
(const corrs_list_t &corrs,const std::vector<lm_t> &lms,getpitem_t gpi)
{
  double area = 0,totarea = 0;

  for(int i = 0 ; i < corrs.size(); ++i)
    area += lms[gpi(corrs[i])].area;

  for(int i = 0 ; i < lms.size() ; ++i)
    totarea += lms[i].area;

  return area/totarea;
}

template<> double get_area_fraction<0>
(const corrs_list_t &corrs,const std::vector<lm_t> &lms)
{  return __get_area_fraction(corrs,lms,get_corrs_first);}

template<> double get_area_fraction<1>
(const corrs_list_t &corrs,const std::vector<lm_t> &lms)
{  return __get_area_fraction(corrs,lms,get_corrs_second);}

/*---------------------------------------------------------------------------*/

template<typename getpitem_t>
vtkSmartPointer<vtkDoubleArray> __get_corrs_region_tags
(vtkSmartPointer<vtkPolyData> pS,
 const std::vector<lm_t> & lms,
 const corrs_list_t & corrs,
 getpitem_t gpi)
{
  vtkSmartPointer<vtkDoubleArray> rt = vtkSmartPointer<vtkDoubleArray>::New();

  rt->SetNumberOfComponents(1);
  rt->SetNumberOfTuples(pS->GetNumberOfPoints());
  rt->SetName("CorrsRegionTag");

  for(int i = 0 ; i < rt->GetNumberOfTuples(); ++i)
  {
    rt->SetValue(i,-1);
  }

  for(int i = 0 ; i < corrs.size(); ++i)
  {
    int lm = gpi(corrs[i]);

    for(int j = 0 ; j < lms[lm].mfold.size(); ++j)
      rt->SetValue(lms[lm].mfold[j],i);
  }

  return rt;
}

template<> vtkSmartPointer<vtkDoubleArray> get_corrs_region_tags<0>
(vtkSmartPointer<vtkPolyData> pS,
 const std::vector<lm_t> & lms,
 const corrs_list_t & corrs)
{return __get_corrs_region_tags(pS,lms,corrs,get_corrs_first);}

template<> vtkSmartPointer<vtkDoubleArray> get_corrs_region_tags<1>
(vtkSmartPointer<vtkPolyData> pS,
 const std::vector<lm_t> & lms,
 const corrs_list_t & corrs)
{return __get_corrs_region_tags(pS,lms,corrs,get_corrs_second);}

/*---------------------------------------------------------------------------*/

void tag_landmark_points(vtkSmartPointer<vtkPolyData> pS, const std::vector<lm_t> & lms)
{
  vtkSmartPointer<vtkIntArray> arr = vtkSmartPointer<vtkIntArray>::New();

  arr->SetNumberOfComponents(1);
  arr->SetNumberOfTuples(pS->GetNumberOfPoints());
  arr->SetName("CriticalPointTag");

  for(int i = 0 ; i < arr->GetNumberOfTuples(); ++i)
  {
    arr->SetValue(i,-1);
  }

  vtkSmartPointer<vtkCellArray> vrts = vtkSmartPointer<vtkCellArray>::New();

  for(int i = 0 ; i < lms.size(); ++i)
  {
    const lm_t &lm = lms[i];

    arr->SetValue(lm.vertid,lm.index);
    vtkIdType id = lm.vertid;
    vrts->InsertNextCell(1,&id);
  }

  pS->SetVerts(vrts);
  pS->GetPointData()->AddArray(arr);
}

/*---------------------------------------------------------------------------*/

/**
\note The alignment distance is the rmsd from the corresponding surface of
       A to B normalized by the area fraction.

                          RMSD(A to B)
      D'(C,P,Q) =-------------------------
                   Sum(i=1..n) Afrac(pi)

       D(C,P,Q) = min {D'(C,P,Q), D'(C,Q,P)}

**/

void correspondence_builder_t::compute_alignment_distances_prec
(vtkSmartPointer<vtkPolyData> pA, vtkSmartPointer<vtkPolyData> pB)
{
  m_corrs_sets_xfms_dist.resize(m_corrs_sets.size());

  for(int i = 0 ; i < m_corrs_sets.size(); i++)
  {
    vtkSmartPointer<vtkUnstructuredGrid> csfA = get_corrs_surface<0>
        (m_corrs_sets[i],m_lmsA,pA,m_corrs_sets_xfms[i]);

    vtkSmartPointer<vtkUnstructuredGrid> csfB = get_corrs_surface<1>
        (m_corrs_sets[i],m_lmsB,pB);

//    if(Nicp > 0)
//    {
//      vtkSmartPointer<vtkTransform> icp = do_icp(csfA,csfB,Nicp);
//      icp->Concatenate(transforms[i]);
//      icp->Update();
//      transforms[i]= icp;
//    }

     std::pair<double,double> sds = get_sum_distances<false>
         (csfA,csfB,vtkSmartPointer<vtkTransform>());

    m_corrs_sets_xfms_dist[i]  = (sds.first + sds.second)/2;
  }
}


/*****************************************************************************/
