#ifndef __UTL_INL_H_INCLUDED__
#define __UTL_INL_H_INCLUDED__

#ifndef __UTL_H_INCLUDED__
#error "Not meant to be directly included "
#error "Include utl.h only and this will be included by it"
#endif

#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <fstream>

namespace utl
{
template <char in, char out>
void extract_mesh_subset
( std::vector<char> &vflgs,
  std::vector<double> &verts,
  std::vector<int> &tris,
  std::vector<int> &stof_map)
{
  int vno = 0,tno = 0;

  {
    std::vector<char> has_tri_inc(verts.size()/3,-1);

    for(int i = 0 ; i < tris.size()/3; ++i)
    {
      int v0 = tris[3*i];
      int v1 = tris[3*i+1];
      int v2 = tris[3*i+2];

      if(vflgs[v0] == in && vflgs[v1] == in && vflgs[v2] == in)
      {
        has_tri_inc[v0] = 1;
        has_tri_inc[v1] = 1;
        has_tri_inc[v2] = 1;
      }
    }

    for(int i =0 ; i < verts.size()/3; ++i)
      if(has_tri_inc[i] == -1)
        vflgs[i] = out;
  }

  std::vector<int> ftos_map(verts.size()/3,-1);

  for(int i =0 ; i < verts.size()/3; ++i)
  {
    if(vflgs[i] == in)
    {
      ftos_map[i] = vno/3;

      stof_map.push_back(i);

      verts[vno++] = verts[3*i];
      verts[vno++] = verts[3*i+1];
      verts[vno++] = verts[3*i+2];
    }
  }

  for(int i = 0 ; i < tris.size()/3; ++i)
  {
    int v0 = ftos_map[tris[3*i]];
    int v1 = ftos_map[tris[3*i+1]];
    int v2 = ftos_map[tris[3*i+2]];

    if( v0 != -1 && v1 != -1  &&v2 != -1)
    {
      tris[tno++] = v0;
      tris[tno++] = v1;
      tris[tno++] = v2;
    }
  }

  verts.resize(vno);
  tris.resize(tno);
}

template <class T> bool extract_opt_value
  (std::string opt, T &v,std::vector<std::string> &args)
{
  std::vector<std::string>::iterator it;

  it = std::find(args.begin(),args.end(),opt);

  if(it != args.end())
  {
    it++;

    ENSURE(it != args.end(),"Args ran out before extracting value field");

    if(it == args.end())
      return false;

    v = from_string<T>(*it); it--;
    args.erase(it,it+2);

    return true;
  }
  else
    return false;
}

template <class T1,class T2> bool extract_opt_value
  (std::string opt, T1 &val1,T2 &val2,std::vector<std::string> &args)
{

  std::vector<std::string>::iterator it;

  it = std::find(args.begin(),args.end(),opt);

  if(it != args.end())
  {
    it++;
    it++;

    ENSURE(it != args.end(),"Args ran out before extracting value field");

    val2 = from_string<T2>(*it); it--;
    val1 = from_string<T1>(*it); it--;
    args.erase(it,it+3);

    return true;
  }
  else
    return false;

}

template <class T1,class T2,class T3> bool extract_opt_value
  (std::string opt, T1 &val1,T2 &val2,T3 &val3,std::vector<std::string> &args)
{

  std::vector<std::string>::iterator it;

  it = std::find(args.begin(),args.end(),opt);

  if(it != args.end())
  {
    it++;
    it++;
    it++;

    ENSURE(it != args.end(),"Args ran out before extracting value field");

    val3 = from_string<T3>(*it); it--;
    val2 = from_string<T2>(*it); it--;
    val1 = from_string<T1>(*it); it--;
    args.erase(it,it+4);

    return true;
  }
  else
    return false;

}

template <class T1,class T2,class T3,class T4> bool extract_opt_value
(std::string opt, T1 &v1, T2 &v2, T3 &v3, T4 &v4,
 std::vector<std::string> &args)
{

  std::vector<std::string>::iterator it;

  it = std::find(args.begin(),args.end(),opt);

  if(it != args.end())
  {
    it++;
    it++;
    it++;
    it++;

    ENSURE(it != args.end(),"Args ran out before extracting value field");

    if(it == args.end())
      return false;

    v4 = from_string<T4>(*it); it--;
    v3 = from_string<T3>(*it); it--;
    v2 = from_string<T2>(*it); it--;
    v1 = from_string<T1>(*it); it--;
    args.erase(it,it+5);

    return true;
  }
  else
    return false;
}

template <typename T,int scalar_compno> void read_off
(const std::string &fname, std::vector<double> &verts,std::vector<int> &tris,
 std::vector<T> &scalars)
{
  std::fstream offFile ( fname.c_str(), std::ios::in );

  ENSURE(offFile.is_open(),"cannot read the file");

  std::string line,tmp;

  getline ( offFile, line );

  ENSURE(line == "OFF","Doesnt appear to be an OFF file");

  getline ( offFile, line );

  std::stringstream linestream (line);

  uint num_verts,num_tris;

  linestream >> num_verts >> num_tris;

  verts.resize(num_verts*3);
  scalars.resize(num_verts);

  for(int i = 0 ; i < num_verts && !offFile.eof() ; ++i)
  {
    getline ( offFile, line );

    std::stringstream linestream (line);

    linestream >> verts[3*i] >>verts[3*i +1] >> verts[3*i +2];

    for(int j = 3; j < scalar_compno; ++j)
      linestream >> tmp;

    linestream >> scalars[i];

    ENSURE(!linestream.eof(),"Not enough fields to read scalar");
  }

  ENSURE(!offFile.eof(),"Vertices ran out prematurely");

  tris.resize(num_tris*3);

  for(int i = 0 ; i < num_tris && !offFile.eof() ; ++i)
  {
    getline ( offFile, line );

    uint num_points;

    std::stringstream linestream ( line );

    linestream >> num_points;

    ENSURE(num_points == 3,"can read triangles only");

    linestream >> tris[3*i] >> tris[3*i+1] >> tris[3*i+2];
  }

  ENSURE(!offFile.eof(),"Triangles ran out prematurely");
}

template <typename T>
void write_off(const std::string &fname,
              const std::vector<double> &verts,
              const std::vector<int> &tris,
              const std::vector<T> &scalar)
{
  std::fstream offFile ( fname.c_str(), std::ios::out );

  ENSURE(offFile.is_open(),"cannot open the file for write");

  offFile << "OFF" <<std::endl;

  offFile <<verts.size()/3 <<" " << tris.size()/3 << " 0"<<std::endl;

  for(int i = 0 ; i < verts.size()/3; ++i)
  {
    offFile << verts[3*i]   << " "
            << verts[3*i+1] << " "
            << verts[3*i+2] << " "
            << scalar[i]    << std::endl;
  }

  for(int i = 0 ; i < tris.size()/3 ; ++i)
  {
    offFile <<"3"           << " "
            << tris[3*i]   << " "
            << tris[3*i+1] << " "
            << tris[3*i+2] << std::endl;
  }

}

template <typename iter_t> inline bool __argsort_pair_compare
(const std::pair<size_t,iter_t>& a,const std::pair<size_t,iter_t>& b)
{return *a.second < *b.second;}

template <typename iter_t> inline size_t __argsort_pair_first
(const std::pair<size_t,iter_t>& a) {return a.first;}


template <class iter_t>
void argsort(iter_t b, iter_t e, std::vector<size_t>& idxs)
{
  std::vector< std::pair<size_t,iter_t> > pv ;
  pv.reserve(e - b) ;

  for (int k = 0; b != e ; b++, k++)
    pv.push_back( std::pair<int,iter_t>(k,b));

  std::sort(pv.begin(), pv.end(),__argsort_pair_compare<iter_t>);

  idxs.resize(pv.size());

  std::transform(pv.begin(),pv.end(),idxs.begin(),
                 __argsort_pair_first<iter_t>);
}

template <class iter_t>
void rearrange(iter_t b, iter_t e,const std::vector<size_t>& idxs)
{
  typedef typename std::iterator_traits<iter_t>::value_type vt;

  std::vector<vt> tarr(b,e) ;
  size_t d = e - b;
  size_t i = 0;

  for(; b != e;)
    if(idxs[i] < d)
      *b++ = tarr[idxs[i++]];
    else
      throw std::out_of_range("error in rearrange");
}

template <typename T1,typename T2,typename T3,typename T4>
void from_string(std::string s,T1& t1,T2& t2,T3& t3,T4& t4)
{
  std::stringstream ss(s);

  ss >> t1 >> t2 >> t3 >> t4;  
}

template <typename T1,typename T2,typename T3>
void from_string(std::string s,T1& t1,T2& t2,T3& t3)
{
  std::stringstream ss(s);

  ss >> t1 >> t2 >> t3;
}

template <typename T1,typename T2>
void from_string(std::string s,T1& t1,T2& t2)
{
  std::stringstream ss(s);

  ss >> t1 >> t2;
}
}
#endif //__UTL_INL_H_INCLUDED__
