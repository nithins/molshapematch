#include <stdexcept>
#include <iostream>
#include <exception>
#include <string>
#include <iomanip>

#include <trimesh_dataset.h>
#include <trimesh_mscomplex.h>
#include <trimesh_mscomplex_simp.h>

#include "common/utl.h"
#include "curvature.h"

using namespace std;

using namespace trimesh;

namespace br = boost::range;
namespace ba = boost::adaptors;

typedef tri_cc_geom_t::vertex_t      vertex_t;
typedef tri_cc_geom_t::vertex_list_t vertex_list_t;

template <typename T>
void read_off_file(const string & fname, vertex_list_t &vlist,
                   tri_idx_list_t &tlist,std::vector<T> &fns,int compno)
{
  std::fstream offFile ( fname.c_str(), std::ios::in );
  ENSURE(offFile.is_open(),"cannot read the file");

  std::string line,tmp;
  getline ( offFile, line );
  ENSURE(line == "OFF","Doesnt appear to be an OFF file");

  ENSURE(compno >= 3, "Cannot read coordianate info column as scalar ");

  getline ( offFile, line );
  std::stringstream linestream (line);
  uint num_verts,num_tris;
  linestream >> num_verts >> num_tris;

  vlist.resize(num_verts);
  fns.resize(num_verts);
  tlist.resize(num_tris);

  for ( uint i = 0; i < num_verts && !offFile.eof(); ++i )
  {
    getline ( offFile, line );
    std::stringstream linestream (line);

    linestream >> vlist[i][0] >> vlist[i][1] >> vlist[i][2];

    for(int j = 3; j < compno; ++j)
      linestream >> tmp;

    ENSURE(!linestream.eof(),"Not enough fields to read scalar: line=\n"+line);

    linestream >> fns[i];
  }

  ENSURE(!offFile.eof(),"Vertices ran out prematurely");

  for ( uint i = 0; i < num_tris && !offFile.eof(); i++ )
  {
    getline ( offFile, line );
    std::stringstream linestream (line);

    int ntv;
    linestream >> ntv;

    ensure(ntv == 3,"Mesh contains non-triangle polys");

    linestream >> tlist[i][0] >> tlist[i][1] >>tlist[i][2] ;

    ensure(is_in_range(tlist[i][0],0,num_verts),"invalid index in file");
    ensure(is_in_range(tlist[i][1],0,num_verts),"invalid index in file");
    ensure(is_in_range(tlist[i][2],0,num_verts),"invalid index in file");
  }

  ENSURE(!offFile.eof(),"Triangles ran out prematurely");
}

int print_usage(std::ostream &os, std::string err_mesg="")
{
  if(err_mesg.size() !=0 )
  {
    os<<"Command Line parsing error: "<<endl;
    os<<err_mesg                      <<endl;
  }

  os<< "Usage: morsesmale-fp-tool <input>.off [-c <scalar-compno>]   " << endl;
  os<< "        [-o <output>.txt] [-s <simp-thresh>]                 " << endl;
  os<< "        [-r <bnd-radius] [-f <fp-type>]                      " << endl;
  os<< "        [-m <nbd-beg> <nbd-end> <nbd-num>]                   " << endl;
  os<< "                                                             " << endl;
  os<< "                                                             " << endl;
  os<< "Brief Description:                                           " << endl;
  os<< "   Extract feature points by computing the Morse-Smale       " << endl;
  os<< "   complex, followed by a persistence simplification. Feature" << endl;
  os<< "   points are critical points that survive simplification.   " << endl;
  os<< "   Some feature points may be rejected if they are too close " << endl;
  os<< "   to the boundary. The user may also specify which type of  " << endl;
  os<< "   critical points he is interested in as feature points     " << endl;
  os<< "   e.g. only maxima,only minima, only saddles, all , etc     " << endl;
  os<< "                                                             " << endl;
  os<< "                                                             " << endl;
  os<< "  <input>.off: Input triangle mesh in off format with scalar " << endl;
  os<< "         function as a per vertex data field                 " << endl;
  os<< "                                                             " << endl;
  os<< "-c <scalar-compno>: column number of the scalar field in off " << endl;
  os<< "         file (default 3)                                    " << endl;
  os<< "                                                             " << endl;
  os<< "-o <output>.txt: file to put the list of selected feature    " << endl;
  os<< "         points' data                                        " << endl;
  os<< "         (default <input>_fp.txt)                            " << endl;
  os<< "                                                             " << endl;
  os<< "-s <simp-thresh>: Persistence Simplification threshold for   " << endl;
  os<< "         mscomplex. (default 0.0)                            " << endl;
  os<< "                                                             " << endl;
  os<< "-r <bnd-radius>: feature points may be optionally rejected if" << endl;
  os<< "         they are within euclidian distance <bnd-radius> from" << endl;
  os<< "         any boundary (default 0.0)                          " << endl;
  os<< "                                                             " << endl;
  os<< "-f <fp-type>: The type of critical points that are to be     " << endl;
  os<< "         extracted as feature points.                        " << endl;
  os<< "         0: all (default)                                    " << endl;
  os<< "         1: maxima                                           " << endl;
  os<< "         2: minima                                           " << endl;
  os<< "         3: saddle                                           " << endl;
  os<< "         4: maxima and minima                                " << endl;
  os<< "                                                             " << endl;
  os<< "-m <nbd-beg> <nbd-end> <nbd-num>: Compute multi scale        " << endl;
  os<< "         curvatures for each of the selected landmark points." << endl;
  os<< "         the scales are given by [<nbd-beg>,<nbd-end>] with  " << endl;
  os<< "         <nbd-num> samples beg and end inclusive.            " << endl;
  os<< "                                                             " << endl;

  return (err_mesg.size() !=0);
}

bool fp_type_filter(mscomplex_ptr_t msc, int fp_type, int cp)
{
  switch(fp_type)
  {
  case 0: return true;
  case 1: return msc->index(cp) == 2;
  case 2: return msc->index(cp) == 0;
  case 3: return msc->index(cp) == 1;
  case 4: return msc->index(cp) != 1;
  default: return false;
  }
}

bool is_bnd_outside_rrad
(tri_cc_ptr_t tcc,const vertex_list_t &vlist,double radius,cellid_t s)
{
  if(radius == 0)
    return true;

  std::stack<int>     front_verts;
  std::set<int>       seen_verts;

  vertex_t st_pt = vlist[s];

  front_verts.push(s);

  double r2 = radius*radius;

  while(front_verts.size() != 0)
  {
    cellid_t t = front_verts.top();

    front_verts.pop();

    if(tcc->is_cell_boundry(t))
      return false;

    cellid_t nverts[40];

    int ct = tcc->get_vert_link_verts(t,nverts);

    for(int i = 0 ; i < ct; ++i)
    {
      cellid_t n = nverts[i];

      if((st_pt - vlist[n]).squaredNorm() > r2)
        continue;

      if(seen_verts.count(n) != 0)
        continue;

      seen_verts.insert(n);

      front_verts.push(n);
    }
  }

  return true;
}

void compute_lm_mfold_area
(dataset_ptr_t ds,
 mscomplex_ptr_t msc,
 const vertex_list_t  &vlist,
 const std::vector<int> &lmcps,
 std::vector<double> &lmMfoldArea)
{
  int tbeg = ds->m_tcc->get_num_cells_max_dim(1);

  std::vector<double> varea(ds->m_tcc->get_num_cells_dim(0),0);
  std::vector<double> tarea(ds->m_tcc->get_num_cells_dim(2),0);

  lmMfoldArea.resize(lmcps.size());

  for(int i = 0 ; i < ds->m_tcc->get_num_cells_dim(2); ++i)
  {
    cellid_t pts[3];
    ds->m_tcc->get_cell_points(i+tbeg,pts);

    vertex_t u = vlist[pts[0]] - vlist[pts[1]];
    vertex_t v = vlist[pts[0]] - vlist[pts[2]];

    double a = u.cross(v).norm()/2.0;

    varea[pts[0]] += a/3;
    varea[pts[1]] += a/3;
    varea[pts[2]] += a/3;

    tarea[i] = a;
  }

  for(int i = 0 ; i < lmcps.size(); ++i)
  {
    switch(msc->index(lmcps[i]))
    {

    case 0:
    {
      mfold_t &mfold = msc->mfold<ASC>(lmcps[i]);
      for(int j = 0 ; j < mfold.size(); ++j)
        lmMfoldArea[i] += varea[mfold[j]];
      break;
    }

    case 2:
    {
      mfold_t &mfold = msc->mfold<DES>(lmcps[i]);
      for(int j = 0 ; j < mfold.size(); ++j)
        lmMfoldArea[i] += tarea[mfold[j]-tbeg];
      break;
    }
    }
  }
}

void compute_lm_mfold_verts
(dataset_ptr_t ds,
 mscomplex_ptr_t msc,
 const std::vector<int> &lmcps,
 std::vector<std::vector<int> > &lmMfoldPts)
{
  lmMfoldPts.resize(lmcps.size());

  for(int i = 0 ; i < lmcps.size(); ++i)
  {
    switch(msc->index(lmcps[i]))
    {
    case 0:
    {
      mfold_t &mfold = msc->mfold<ASC>(lmcps[i]);
      lmMfoldPts[i].resize(mfold.size());
      br::copy(mfold,lmMfoldPts[i].begin());
      break;
    }
    case 2:
    {
      std::set<int> mfold_set;
      mfold_t &mfold = msc->mfold<DES>(lmcps[i]);
      for(int j = 0 ; j < mfold.size(); ++j)
      {
        cellid_t pts[3];
        ds->m_tcc->get_cell_points(mfold[j],pts);
        mfold_set.insert(pts[0]);
        mfold_set.insert(pts[1]);
        mfold_set.insert(pts[2]);
      }

      lmMfoldPts[i].resize(mfold_set.size());
      br::copy(mfold_set,lmMfoldPts[i].begin());
      break;
    }
    }
  }
}

void compute_lm_pos_uncertainty
(double lm_fn,
 double simp_thresh,
 const fn_list_t &fns,
 const vertex_list_t  &vlist,
 const std::vector<int> &lmMfoldPts,
 vertex_t &lmPos,
 double &lmUncert)
{
  lmPos = la::make_vec<double>(0,0,0);

  std::vector<int> cp_mf_vids;

  for(int j = 0 ; j < lmMfoldPts.size(); ++j )
    if(std::abs(fns[lmMfoldPts[j]] - lm_fn) < simp_thresh)
    {
      cp_mf_vids.push_back(lmMfoldPts[j]);
      lmPos  += vlist[lmMfoldPts[j]];
    }

  lmPos /= cp_mf_vids.size();

  lmUncert = 0;

  for(int k = 0 ; k < cp_mf_vids.size(); ++k )
    lmUncert = max(lmUncert,(lmPos-vlist[cp_mf_vids[k]]).squaredNorm());

  lmUncert = std::sqrt(lmUncert);
}

void compute_lm_pos_uncertainty
(double lm_fn,
 double simp_thresh,
 const fn_list_t &fns,
 const vertex_list_t  &vlist,
 dataset_ptr_t ds,
 int lm_vid,
 vertex_t &lmPos,
 double &lmUncert)
{
  std::set<int>   mfold;
  std::stack<int> front_verts;
  cellid_t pts[40];

  front_verts.push(lm_vid);
  lmPos = vlist[lm_vid];
  mfold.insert(lm_vid);

  while(front_verts.size() != 0)
  {
    int vid = front_verts.top();
    front_verts.pop();

    int ct = ds->m_tcc->get_vert_link_verts(vid,pts);
    ENSUREV(ct < 40,"Overflow error!!!",ct);

    for(int i = 0 ; i < ct; ++i)
    {
      int nvid = pts[i];

      if(abs<double>(fns[nvid] - lm_fn) >= simp_thresh)
        continue;

      if (mfold.count(nvid) != 0)
        continue;

      front_verts.push(nvid);
      lmPos += vlist[nvid];
      mfold.insert(nvid);
    }
  }

  lmPos /= mfold.size();

  lmUncert = 0;

  for(std::set<int>::iterator it = mfold.begin(); it != mfold.end(); ++it)
    lmUncert = max(lmUncert,(lmPos-vlist[*it]).squaredNorm());

  lmUncert = std::sqrt(lmUncert);
}

int main(int argc , char **argv)
{
  if (argc == 1)
    return print_usage(cout);

  string input_offfile;
  string output_txtfile;
  int    comp_no = 3;
  double simp_tresh  = 0.1;
  double bnd_radius  = 0;
  int    fp_type = 0;

  double nbd_beg = 1.2;
  double nbd_end = 2.6;
  int    nbd_num = 15;

  std::vector<std::string> args(argv+1,argv+argc);

  utl::extract_opt_value("-c",comp_no,args);
  utl::extract_opt_value("-s",simp_tresh,args);
  utl::extract_opt_value("-o",output_txtfile,args);
  utl::extract_opt_value("-r",bnd_radius,args);
  utl::extract_opt_value("-f",fp_type,args);
  utl::extract_opt_value("-m",nbd_beg,nbd_end,nbd_num,args);

  if(args.size() != 1)
    return print_usage(cerr,"Expected one .off file for <input>.off");

  input_offfile = args[0];

  if(output_txtfile.size() ==0)
    output_txtfile = input_offfile.substr(0,input_offfile.size()-4)+"_lm.txt";

  cout<<"==============================================="<<endl;
  cout<<"         Starting Processing                   "<<endl;
  cout<<"-----------------------------------------------"<<endl;

  vertex_list_t           vlist;
  trimesh::tri_idx_list_t tlist;
  trimesh::fn_list_t      fns;

  cout<<"selected comp = "<<comp_no<<endl;
  cout<<"thresh        = "<<simp_tresh<<endl;
  cout<<"-----------------------------------------------"<<endl;

  read_off_file(input_offfile,vlist,tlist,fns,comp_no);

  cout<<"fmin = "<<*br::min_element(fns)<<"  ";
  cout<<"fmax = "<<*br::max_element(fns)<<endl;
  cout<<"data read -------------------------------------"<<endl;

  trimesh::dataset_ptr_t   ds(new trimesh::dataset_t(fns,tlist));
  trimesh::mscomplex_ptr_t msc(new trimesh::mscomplex_t);
  ds->work(msc);
  cout<<"gradient done ---------------------------------"<<endl;

  msc->simplify(simp_tresh*(msc->m_fmax - msc->m_fmin));

  cout<<"simplification done ---------------------------"<<endl;

  std::vector<int> lmcps;

  br::copy
      (msc->cp_range()|
       ba::filtered(bind(&mscomplex_t::is_not_paired,msc,_1))|
       ba::filtered(bind(fp_type_filter,msc,fp_type,_1))|       
       ba::filtered(bind(is_bnd_outside_rrad,ds->m_tcc,vlist,bnd_radius,
                         bind(&mscomplex_t::vertid,msc,_1))),
       std::back_inserter(lmcps));

  /*----------- Compute Multiscale curvature per landmark -------------------*/

  std::vector<double> ms_curv,cp_normals;

  {
    std::vector<int> lmvids;

    br::copy(lmcps|ba::transformed(bind(&mscomplex_t::vertid,msc,_1)),
             back_inserter(lmvids));

    std::vector<double> vl(vlist.size()*3);
    std::vector<int>    tl(tlist.size()*3);

    for( int i = 0 ; i < vlist.size() ; ++i)
    {
      vl[3*i+0] = vlist[i][0];
      vl[3*i+1] = vlist[i][1];
      vl[3*i+2] = vlist[i][2];
    }

    for( int i = 0 ; i < tlist.size() ; ++i)
    {
      tl[3*i+0] = tlist[i][0];
      tl[3*i+1] = tlist[i][1];
      tl[3*i+2] = tlist[i][2];
    }    

    compute_multi_scale_mean_curvature
        (vl,tl,lmvids,nbd_beg,nbd_end,nbd_num,ms_curv,cp_normals);
  }

  cout<<"Multi-Scale curvature done --------------------"<<endl;

  /*----------- Compute per landmark info -----------------------------------*/

  std::vector < std::vector <int> > lmMfoldPts (lmcps.size());

  msc->collect_mfolds(DES,2,ds);

  std::vector<double>  lmMfoldArea(lmcps.size(),0);

  compute_lm_mfold_area(ds,msc,vlist,lmcps,lmMfoldArea);

  compute_lm_mfold_verts(ds,msc,lmcps,lmMfoldPts);

  std::ofstream os(output_txtfile.c_str(),std::ios::out);

  os << setw(6)  << left << "#Idx"
     << setw(6)  << left << "Vid"
     << setw(6)  << left << "NPts"
     << setw(12) << left << "PosX"
     << setw(12) << left << "PosY"
     << setw(12) << left << "PosZ"
     << setw(12) << left << "Area"
//     << setw(12) << left << "PosUncert"
//     << setw(12) << left << "uPosX"
//     << setw(12) << left << "uPosY"
//     << setw(12) << left << "uPosZ"
     << setw(12) << left << "nX"
     << setw(12) << left << "nY"
     << setw(12) << left << "nZ"
     << endl;

  os << lmcps.size() << endl;

  for(int i = 0 ; i < lmcps.size(); ++i)
  {    
    vertex_t pos = vlist[msc->vertid(lmcps[i])];
    double *nrm = cp_normals.data()+ i*3;


//    compute_lm_pos_uncertainty
//        (msc->fn(lmcps[i]),simp_tresh,fns,vlist,lmMfoldPts[i],pos,posUncert);

//    vertex_t upos;
//    double  posUncert = 0;

//    compute_lm_pos_uncertainty
//        (msc->fn(lmcps[i]),simp_tresh,fns,vlist,ds,msc->vertid(lmcps[i]),
//         upos,posUncert);

    os << int(msc->index(lmcps[i])) <<"\t"
       << msc->vertid(lmcps[i]) <<"\t"
       << lmMfoldPts[i].size() <<"\t"
       << pos[0]<<"\t"
       << pos[1]<<"\t"
       << pos[2]<<"\t"
       << lmMfoldArea[i]<<"\t"
//       << posUncert<<"\t"
//       << upos[0]<<"\t"
//       << upos[1]<<"\t"
//       << upos[2]<<"\t"
       << nrm[0]<<"\t"
       << nrm[1]<<"\t"
       << nrm[2]<<"\t"
       << endl;
  }

  for ( int i = 0,k=0 ; i < lmcps.size(); ++i)
  {
    for(int j = 0; j < nbd_num; ++j,++k)
      os << ms_curv[k] << "\t";
    os << endl;
  }

  for(int i = 0 ; i < lmcps.size(); ++i)
  {
    utl::write_b64(os,lmMfoldPts[i]);
    os << endl << endl;
  }

  cout<<"Landmark info done ----------------------------"<<endl;
  cout<<"-----------------------------------------------"<<endl;
  cout<<"        Finished Processing                    "<<endl;
  cout<<"==============================================="<<endl;
}
