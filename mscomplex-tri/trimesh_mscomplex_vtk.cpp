//#ifndef USING_VTK
//#define USING_VTK
//#endif

#ifdef USING_VTK

#include <trimesh_mscomplex.h>
#include <tri_edge.h>

#include <boost/foreach.hpp>
#include <boost/range/adaptors.hpp>

#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkCellArray.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkIntArray.h>
#include <vtkPointData.h>

namespace badpt = boost::adaptors;

void trimesh::mscomplex_t::save_conn_graph_vtk(const std::string &fn, tri_cc_geom_ptr_t tcc) const
{
  using namespace trimesh;

  vtkSmartPointer<vtkPoints>     nodes = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkCellArray>   arcs = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkIntArray>   nind  = vtkSmartPointer<vtkIntArray>::New();

  std::map<vtkIdType,vtkIdType> node_map;

  BOOST_FOREACH(vtkIdType a, (cp_range()|badpt::filtered(bind(&mscomplex_t::is_not_paired,this,_1))))
  {
    tri_cc_geom_t::vertex_t pt =  tcc->get_cell_position(vertid(a));
    node_map[a] = nodes->InsertNextPoint(pt[0],pt[1],pt[2]);
    nind->InsertNextValue(index(a));
  }

  BOOST_FOREACH(vtkIdType a, (cp_range()|badpt::filtered(bind(&mscomplex_t::is_not_paired,this,_1))))
  {
    BOOST_FOREACH(vtkIdType b, m_des_conn[a])
    {
      vtkIdType arc[]={node_map[a],node_map[b]};
      arcs->InsertNextCell(2,arc);
    }
  }

  vtkSmartPointer<vtkUnstructuredGrid> ug = vtkSmartPointer<vtkUnstructuredGrid>::New();
  ug->SetPoints(nodes);
  ug->SetCells(VTK_LINE,arcs);
  nind->SetName("Index");
  ug->GetPointData()->AddArray(nind);

  vtkSmartPointer<vtkXMLUnstructuredGridWriter> w =
      vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

  w->SetFileName(fn.c_str());
  w->SetInputData(ug);
  w->SetCompressorTypeToZLib();
  w->Write();
}

#endif
