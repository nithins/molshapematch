#ifndef __CURVATURE_H_INCLUDED__
#define __CURVATURE_H_INCLUDED__


#include <vector>

#include <tri_edge.h>
#include <boost/shared_ptr.hpp>


/*****************************************************************************/

void compute_mean_curvature
(tri_cc_geom_ptr_t tcc,
 double nbd_size,
 std::vector<double> &curv);

/*---------------------------------------------------------------------------*/

void compute_multi_scale_mean_curvature
(tri_cc_geom_ptr_t tcc,
 int i,
 double nbd_beg,
 double nbd_end,
 int    nbd_num,
 double *curv,
 double *normal);

/*****************************************************************************/
#endif
